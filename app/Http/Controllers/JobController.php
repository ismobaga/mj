<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JobType;
use App\Models\Category;
use App\Models\Job;
use App\Models\City;

class JobController extends Controller
{
    //
  public function index(Request $request )
  {
        // print_r($request);
        // $jobs =  Job::with(['Employer', 'City', 'Category', 'JobType', 'Company']);
    $jobs = Job::where('is_active', true)->first();
    if ($request->filled('loc')) {
      $loc = $request->get('loc');
      $city = City::where('ascii_name',"like", "%$loc%")->first();
      if ($city!==null) {
        $jobs  = $jobs->where('city_id', $city->id);
      }
    }
    if ($request->filled('type')) {
      $type = $request->get('type');
      $jtype = JobType::where('var_name',"like", "%$type%")->first();
      if ($jtype!==null) {
        $jobs  = $jobs->where('job_type_id', $jtype->id);
      }
    }
     if ($request->filled('cat')) {
      $c = $request->get('cat');
      $cat = Category::where('var_name',"like", "%$c%")->first();
      if ($cat!==null) {
        $jobs  = $jobs->where('category_id', $cat->id);
      }
    }
    	   $data = $jobs->get();//->join('jobtypes');
        return view('jobs.home', compact('data'));
      }
      public function show($id)
      {
       $job = Job::find($id);
        // return $job;
       return view('jobs.show', compact('job'));
     }
   }
