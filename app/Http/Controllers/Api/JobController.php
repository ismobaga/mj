<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    //
        public function index()
    {
    	$data = Job::with(['Employer', 'City'])->get();//->join('jobtypes');
        // return view('jobs.home', compact('data'));
        return $data;
    }
       public function show($id)
    {
    	$job = Job::with(['Employer', 'City', 'Category', 'JobType', 'Company'])->find($id);//->;get();
        return $job;
        //return view('jobs.job', compact('job'));
    }
}
