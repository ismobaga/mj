<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    //
    //
    //
     public function Company()
    {
        return $this->belongsTo('App\Models\Company', 'employer_id', 'id');
    }
      public function Job()
    {
        return $this->belongsTo('App\Models\Job');
    }
}
