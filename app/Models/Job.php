<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    //
     public function JobType()
    {
        return $this->hasOne('App\Models\JobType', 'id', 'job_type_id');
    }
      public function City()
    {
        return $this->hasOne('App\Models\City', 'id', 'city_id');
    }
      public function Category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }
      public function Employer()
    {
        return $this->hasOne('App\Models\Employer', 'id', 'employer_id');
    }
      public function Company()
    {
       return $this->hasOne('App\Models\Company', 'id', 'company_id');
    }
}
