<div class="card bg-dark bg- border border-success mb-20 col-sm-3" >
  {{-- <img class="card-img-top" style="width: 100%;" src="{{$job->Company->logo_path}}" alt="Card image cap"> --}}
  <div class="card-body">
    <h5 class="card-title">{{$job->title}}</h5>
    <p class="card-text">{{$job->description}}</p>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">{{$job->City->name}}</li>
    <li class="list-group-item">{{$job->Company->name}}</li>
    <li class="list-group-item">Type: {{$job->JobType->name}}</li>
    <li class="list-group-item">Cat: {{$job->Category->name}}</li>
    <li class="list-group-item">{{$job->salary}}</li>
    <li class="list-group-item">{{$job->expires}}</li>
  </ul>
  <div class="card-body">
    <a href="#" class="btn btn-primary card-link">Apply</a>
    <a href="{{route('job.show', $job->id)}}" class="btn btn-primary card-link">More</a>
  </div>
</div>