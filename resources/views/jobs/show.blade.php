@extends('layouts.app')

@section('content')

 

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Title -->
          <h1 class="mt-4">{{$job->title}}</h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="#">{{$job->Company->name}}</a>
          </p>

          <hr>

          <!-- Date/Time -->
          <p>Ending on {{$job->expires}}</p>

          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" style="width: 100%;height: 200px; " src="{{$job->Company->logo_path}}" alt="">

          <hr>

          <!-- Post Content -->
          <p class="lead">{{$job->description}}</p>



    {{--       <blockquote class="blockquote">
            <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
            <footer class="blockquote-footer">Someone famous in
              <cite title="Source Title">Source Title</cite>
            </footer>
          </blockquote> --}}

   

          <p>{{$job->apply_desc}}</p>

          <hr>

          <!-- Comments Form -->
          <div class="card my-4 pull-right">
            <div class="card-body">
           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#applicationModal">Apply</button>
     
            </div>
          </div>

   <hr>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <div class="card my-4">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
            <h5 class="card-header">Type</h5>
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">{{$job->JobType->name}}</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <h5>Categories</h5>
                  <ul class="list-unstyled mb-0">
                   <li>
                      <a href="#">{{$job->Category->name}}</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <h5>Location</h5>
                  <ul class="list-unstyled mb-0">
                   <li>
                      <a href="#">{{$job->City->name}}</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!-- Side Widget -->
          <div class="card my-4">
            <img class="card-img-top" style="width: 100%;" src="{{$job->Company->profile_picture}}" alt="Card image cap">
            <h5 class="card-header">{{$job->Company->name}}</h5>
            <div class="card-body">
              {{$job->Company->description}}
            </div>
             <button type="submit" class="btn btn-primary">View</button>
          </div>

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
    @include('jobs.modals.application')
@endsection