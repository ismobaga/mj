@extends('layouts.app')

@section('content')



@include('jobs.partials.detail-modals')

<div class="job-details">
			<div class="container">
				<div class="row top-heading">
					<div class="col-md-12 col-xs-12">
						<a href="{{route('jobs')}}"><button type="button" class="btn btn-back">@lang('general.backbtn')</button></a>
						<h1>{{$job->title}}</h1>
						<h2>@lang('general.go_home')> {{$job->Category->name}} > {$profile.fullname}</h2>
					</div>
				</div>
				<div class="row top-heading">

					<div class="col-md-9 col-xs-12 details">
						<h3>{{$job->title}} @if( '$REMOTE_PORTAL' == 'deactivated' || true)@lang('general.in') {{$job->City->name}}@endif @if(true)<span class="new">@lang('job.new')</span>@endif</h3>
						
						@if( '$FAVORITES_PLUGIN and $FAVORITES_PLUGIN' == 'true' or true)
							@if( '$favourites_job_ids and $job.id|in_array:$favourites_job_ids'=='' or true)
								<span id="desk-favourites-block-{$job.id}" ><a title="{$translations.alljobs.favourites_tooltip_remove}" href="#" onclick="return SimpleJobScript.removeFromFavourites({$job.id}, '{$BASE_URL}_tpl/{$THEME}/img/', 'desk-');"><i class="fa fa-heart fa-lg ml10" aria-hidden="true"></i></a></span>
					 		@else
					 			<span id="desk-favourites-block-{$job.id}" ><a title="{$translations.alljobs.favourites_tooltip_add}" href="#" onclick="return SimpleJobScript.addToFavourites({$job.id}, '{$BASE_URL}_tpl/{$THEME}/img/', 'desk-');"><i class="fa fa-heart-o fa-lg ml10" aria-hidden="true"></i></a></span>
					 		@endif
					 	@endif

						<ul class="top-ul">
							<li><span><i class="fa fa-building" aria-hidden="true"></i></span> {{$job->company}}</li>
							<li><span><i class="fa fa-braille" aria-hidden="true"></i></span> {{$job->JobType->name}}</li>
							<li data-toggle="tooltip" title="@lang('job.email_to')"><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span> <a class="undsc" data-toggle="modal" data-target="#emailModal" href="#" onclick="return false;">@lang('job.email_to')</a></li>
						</ul>
						<ul>
							
							<li><span><i class="fa fa-map-marker" aria-hidden="true"></i></span> {{$job->City->name}}</li>
							
							<li><span><i class="fa fa-calendar" aria-hidden="true"></i></span> {{$job->created_at}}</li>
							<li data-toggle="tooltip" title="@lang('job.report')"><span><i class="fa fa-volume-up" aria-hidden="true"></i></span> <a class="undsc" data-toggle="modal" data-target="#reportModal" href="#" onclick="return false;">@lang('job.report')</a></li>
						</ul>
					
						<div class="border-light"></div>

					

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="detail-font" >
								<p>{{$job->description}}</p>
							</div>
						</div>

							<div class="border-light"></div>
							@if($job->salary)<p class="price-apply">{{$job->salary}}</p>@endif

								@if($job->apply_online == 1 or true)

									@if('$SESSION_APPLICANT '== 'true' )
										@include('jobs.partials.apply-existing-modal')
									@else
									@include('jobs.partials.apply-modal')
									@endif

								@else

								<!-- TODO - needs styling -->
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mlpl0">
									<p class="hta-p">{$job.apply_desc}</p>
								</div>

								@endif

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 jd-ads-m">
								@if('ADSENSE' == 'true' or true)
									{include file="$adsense_detail_rectangle"}
								@endif
								</div>


						</div>
						<div class="col-md-3 col-xs-12 co-name">

							@if($job['public_profile'] == '1' or true)
								<a href="{{$job->Employer->Company}}"><img class="co-logo" src="{{$job->Employer->Company}}" alt="company logo" />
								{{$job->Employer->Company}}</a>
							@else
								<img class="co-logo" src="/{$job.company_logo_path}" alt="company logo" />
							@endif

							<h2 class="co-title">{{$job->company}}</h2>
								{{$job->Employer->Company}}</a>
							<p class="co-summary">
								{$job.company_desc_excerpt}

							@if(true)
							<a href="{$job.company_detail_url}" target="_blank"><button type="button" class="btn btn-more" >{$translations.website_general.more}</button></a>
							@endif

							</p>

						</div>

						<div class="col-md-3 col-xs-12">
						@if( '$smarty.const.BANNER_MANAGER' == 'true')
							{include file="$banners_detail_rectangle"}
						@endif	
						</div>

					</div>

					@if( '$related_jobs'=="")
					<div class="row top-heading">
						<h2 class="heading">{$translations.detail_sidebar.related_jobs_title}</h2>

						

				</div>
				<!-- related jobs -->
				@endif

			</div>
		</div>


<script type="text/javascript">
	$(document).ready(function() {
		SimpleJobScript.I18n = {/literal}{$translationsJson}{literal};
	    SimpleJobScript.initApplyValidation();

		$('#cv').change(function() {
			var fname = $('input[type=file]').val().split('\\').pop();
			if( fname )
				$('#cvLabel').html(fname);
			else
				$('#cvLabel').html($('#cvLabel').html());
        });

        $('.popup').click(function(event) {
		    var width  = 575,
		        height = 400,
		        left   = ($(window).width()  - width)  / 2,
		        top    = ($(window).height() - height) / 2,
		        url    = this.href,
		        opts   = 'status=1' +
		                 ',width='  + width  +
		                 ',height=' + height +
		                 ',top='    + top    +
		                 ',left='   + left;
		    
		    window.open(url, 'twitter', opts);
		    return false;
		  });

	});

</script>

@endsection
