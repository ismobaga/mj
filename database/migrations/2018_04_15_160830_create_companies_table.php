<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 500);
            $table->text('description');
            $table->string('hq', 600);
            $table->string('url', 300);
            $table->string('street', 400);
            $table->string('city_postcode', 300);
            $table->string('profile_picture', 300);
            $table->boolean('public');
            $table->text('logo_path');
            $table->timestamps();
            $table->unsignedInteger('employer_id');
            $table->foreign('employer_id')->references('id')->on('employers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
