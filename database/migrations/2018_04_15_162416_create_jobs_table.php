<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_type_id');
            $table->integer('employer_id');
            $table->integer('category_id');
            $table->integer('company_id');
            $table->string('title', 400);
            $table->text('description');
            $table->timestamp('expires');
            $table->boolean('is_active');
            $table->integer('views_count');
            $table->integer('city_id');
            $table->boolean('apply_online');
            $table->text('apply_desc');
            // $table->string('company', 150);
            $table->string('salary', 150);
            $table->integer('spotlight');
            $table->string('is_tmp');
            $table->timestamps();



            $table->foreign('job_type_id')->references('id')->on('job_types');
            $table->foreign('employer_id')->references('id')->on('employers');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('company_id')->references('id')->on('companies');

            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
