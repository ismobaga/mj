<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 100);
            $table->string('email', 100);
            $table->string('phone', 40);
            $table->string('weblink', 400);
            $table->string('cv_path', 200);
            $table->boolean('public_profile');
            $table->string('password', 100);
            $table->timestamp('last_activity');
            $table->string('location', 300);
            $table->text('skills');
            $table->boolean('comfirmed');
            $table->string('sn_link_1', 400);
            $table->string('sn_link_2', 400);
            $table->string('sn_link_3', 400);
            $table->string('sn_link_4', 400);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
