<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\City;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         City::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            $c = $faker->city;
            City::create([

                'name' => $c,
                'ascii_name' => Str::slug($c, '-'),
            ]);
        }
    }
}
