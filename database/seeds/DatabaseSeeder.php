<?php

use Illuminate\Database\Seeder;
// use \App\Models\Employer;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CandidatesSeeder::class);
        $this->call(EmployersSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(JobTypesSeeder::class);
        $this->call(JobsSeeder::class);
        
      
    }
}


/**
 * ->each(function ($j) {
        $j->posts()->save(factory(App\Models\Employer::class)->make());
    });
 */