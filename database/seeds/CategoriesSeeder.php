<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use \App\Models\Category;
class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Category::truncate();

    	$faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
    	for ($i = 0; $i < 50; $i++) {
            $c =$faker->realText(32);
    		Category::create([
    			'name' => $c,
    			'var_name' => Str::slug($c, ''),
    			'title' => $faker->realText(32),
    			'description' => $faker->realText(100),
    			'keywords' =>  $faker->word(3, true),
    			'category_order' => $i,
    			'created_at' => $faker->dateTime,

    		]);

    	}
    }
}
