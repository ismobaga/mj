<?php

use Illuminate\Database\Seeder;
use App\Models\Employer;
class EmployersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	Employer::truncate();

    	$faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
    	$password = Hash::make('maliba');
    	for ($i = 0; $i < 50; $i++) {
    		Employer::create([
    			'name' => $faker->name,
    			'email' => $faker->safeEmail,
    			'password' => $password,
    			'comfirmed' => 1 ,
    			'created_at' => $faker->dateTime,

    		]);

    	}
    }
}