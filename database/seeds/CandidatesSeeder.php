<?php

use Illuminate\Database\Seeder;
use App\Models\Candidate;
class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
               // Let's truncate our existing records to start from scratch.
        Candidate::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        $password = Hash::make('maliba');
        for ($i = 0; $i < 50; $i++) {
            Candidate::create([
            'fullname' =>$faker->name,
            'email' => $faker->safeEmail,
            'phone' =>  $faker->PhoneNumber,
            'password' => $password,
            'weblink' => $faker->url,
            'cv_path' => '-',
            'public_profile' => 1,
            'last_activity' => $faker->dateTime,
            'comfirmed' =>1,
            'location' => $faker->address,
            'skills' => $faker->word(4, true),
            'sn_link_1' => $faker->url,
            'sn_link_2' => $faker->url,
            'sn_link_3' => $faker->url,
            'sn_link_4' => $faker->url,
        ]);
    }
}
}