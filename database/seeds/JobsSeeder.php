<?php

use Illuminate\Database\Seeder;
use App\Models\Job;
class JobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
         Job::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Job::create([
            'title' => $faker->jobTitle,
            'description' => $faker->realText(100),
            'apply_desc' => $faker->realText(100),
            'spotlight' => $faker->numberBetween(0,1),
            // 'company' => $faker->company,
            'salary' => $faker->numberBetween(10,50)."$/h",
            'is_tmp' => 0,
            'employer_id' => $faker->numberBetween(1,50),
            'company_id' => $faker->numberBetween(1,50),
            'job_type_id' => $faker->numberBetween(1,20),
            'category_id' => $faker->numberBetween(1,10),
            'city_id' => $faker->numberBetween(1,10),
            'apply_online' => 1,
            'is_active' => 1,
            'views_count' => 0,
            'expires' => $faker->dateTime('2018-06-08 06:07:09'),

            ]);
        }
    }
}
