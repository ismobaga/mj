<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\JobType;

class JobTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         JobType::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            $t = $faker->word(3, true);
            JobType::create([
                'name' => $t,
                'var_name' => Str::slug($t, ''),
                'created_at' => $faker->dateTime,

            ]);
    }
}

}
