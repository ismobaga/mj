<?php

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         // Let's truncate our existing records to start from scratch.
        Company::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Company::create([
                'name' => $faker->company,
                'description' => $faker->paragraph,
                'hq' => $faker->text,
                'url' => $faker->url,
                'street' => $faker->streetName,
                'city_postcode' => $faker->city." ".$faker->postcode,
                'profile_picture' => $faker->imageUrl,
                'public' => 1 ,
                'logo_path' => $faker->imageUrl,
                'employer_id' => $faker->numberBetween(1,50),
                'created_at' => $faker->dateTime,

            ]);
        }
    }
}
