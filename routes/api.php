<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    // Controllers Within The "App\Http\Controllers\Api" Namespace
Route::get('jobs', 'JobController@index');
Route::get('jobs/{job}', 'JobController@show');
Route::post('jobs', 'JobController@store');
Route::put('jobs/{id}', 'JobController@update');
Route::delete('jobs/{id}', 'JobController@delete');
});