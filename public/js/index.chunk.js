webpackJsonp(["index"],{

/***/ "./resources/assets/src/app/containers/user/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_navbar__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__playlist_view__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_component__ = __webpack_require__("./resources/assets/src/app/containers/user/user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__playlists__ = __webpack_require__("./resources/assets/src/app/containers/user/playlists/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__user_guard__ = __webpack_require__("./resources/assets/src/app/containers/user/user.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_player_service__ = __webpack_require__("./resources/assets/src/app/containers/user/user-player.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__user_routing__ = __webpack_require__("./resources/assets/src/app/containers/user/user.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// import { PlaylistViewComponent, PlaylistResolver, PlaylistVideosResolver } from '@shared/components/playlist-view';



var UserModule = (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_index__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__app_navbar__["a" /* AppNavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3__playlist_view__["a" /* PlaylistViewModule */],
                __WEBPACK_IMPORTED_MODULE_8__user_routing__["a" /* routing */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__user_component__["a" /* UserComponent */],
                __WEBPACK_IMPORTED_MODULE_5__playlists__["a" /* PlaylistsComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__user_component__["a" /* UserComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__user_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_7__user_player_service__["a" /* UserPlayerService */],
            ]
        })
    ], UserModule);
    return UserModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/user/playlists/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__playlists_component__ = __webpack_require__("./resources/assets/src/app/containers/user/playlists/playlists.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__playlists_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/containers/user/playlists/playlists.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_player_service__ = __webpack_require__("./resources/assets/src/app/containers/user/user-player.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlaylistsComponent = (function () {
    function PlaylistsComponent(store, userPlayerService) {
        this.store = store;
        this.userPlayerService = userPlayerService;
        this.playlists$ = this.store.select(function (state) { return state.user.playlists; });
    }
    PlaylistsComponent.prototype.ngOnInit = function () { };
    PlaylistsComponent.prototype.playSelectedPlaylist = function (playlist) {
        this.userPlayerService.playSelectedPlaylist(playlist);
    };
    PlaylistsComponent.prototype.queueSelectedPlaylist = function (playlist) {
        this.userPlayerService.queuePlaylist(playlist);
    };
    PlaylistsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'playlists',
            template: "\n  <section class=\"videos-list\">\n    <div class=\"list-unstyled ux-maker youtube-items-container clearfix\">\n      <youtube-playlist\n        *ngFor=\"let playlist of playlists$ | async\"\n        [media]=\"playlist\"\n        link=\"/user/\"\n        (play)=\"playSelectedPlaylist(playlist)\"\n        (queue)=\"queueSelectedPlaylist(playlist)\">\n      </youtube-playlist>\n    </div>\n  </section>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_0__user_player_service__["a" /* UserPlayerService */]])
    ], PlaylistsComponent);
    return PlaylistsComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/user/user-player.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPlayerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_store_app_player__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserPlayerService = (function () {
    function UserPlayerService(nowPlaylistService, userProfile, store) {
        this.nowPlaylistService = nowPlaylistService;
        this.userProfile = userProfile;
        this.store = store;
    }
    UserPlayerService.prototype.playSelectedPlaylist = function (playlist) {
        var _this = this;
        this.userProfile
            .fetchPlaylistItems(playlist.id, '')
            .subscribe(function (items) {
            _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__["h" /* QueueVideos */](items));
            _this.nowPlaylistService.updateIndexByMedia(items[0].id);
            _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__core_store_app_player__["c" /* LoadAndPlay */](items[0]));
        });
    };
    UserPlayerService.prototype.queuePlaylist = function (playlist) {
        var _this = this;
        this.userProfile
            .fetchPlaylistItems(playlist.id, '')
            .subscribe(function (items) {
            _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__["h" /* QueueVideos */](items));
            return items;
        });
    };
    UserPlayerService.prototype.queueVideo = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__["g" /* QueueVideo */](media));
    };
    UserPlayerService.prototype.playVideo = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__core_store_app_player__["c" /* LoadAndPlay */](media));
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__["g" /* QueueVideo */](media));
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__["n" /* SelectVideo */](media));
    };
    UserPlayerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__core_services__["d" /* NowPlaylistService */],
            __WEBPACK_IMPORTED_MODULE_2__core_services__["e" /* UserProfile */],
            __WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */]])
    ], UserPlayerService);
    return UserPlayerService;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/user/user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_store_user_profile_user_profile_selectors__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/user-profile.selectors.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_api_app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserComponent = (function () {
    function UserComponent(appApi, store) {
        this.appApi = appApi;
        this.store = store;
        this.playlists$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__core_store_user_profile_user_profile_selectors__["c" /* getUserPlaylists */]);
        this.currentPlaylist$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__core_store_user_profile_user_profile_selectors__["d" /* getUserViewPlaylist */]);
        this.isSignedIn$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__core_store_user_profile_user_profile_selectors__["a" /* getIsUserSignedIn */]);
    }
    UserComponent.prototype.ngOnInit = function () { };
    UserComponent.prototype.signInUser = function () {
        this.appApi.signinUser();
    };
    UserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-user',
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            styles: [__webpack_require__("./resources/assets/src/app/containers/user/user.scss")],
            template: "\n  <article>\n    <app-navbar\n      [header]=\"'My Profile - My Playlists'\"\n      [headerIcon]=\"'heart'\"\n    ></app-navbar>\n    <p *ngIf=\"!(isSignedIn$ | async)\" class=\"well lead\">\n      To view your playlists in youtube, you need to sign in.\n      <button class=\"btn btn-lg btn-primary\"\n        (click)=\"signInUser()\">\n        <icon name=\"google\"></icon> Sign In\n      </button>\n    </p>\n    <router-outlet></router-outlet>\n  </article>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__core_api_app_api__["a" /* AppApi */], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/user/user.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(authorization, router) {
        this.authorization = authorization;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        // console.log('AuthGuard#canActivate called', { state });
        var url = state.url;
        return this.checkLogin(url);
    };
    AuthGuard.prototype.canActivateChild = function (route, state) {
        return this.canActivate(route, state);
    };
    AuthGuard.prototype.checkLogin = function (url) {
        if (this.authorization.isSignIn()) {
            return true;
        }
        // Store the attempted URL for redirecting
        // this.authService.redirectUrl = url;
        // Navigate to the login page with extras
        this.router.navigate(['/user']);
        return false;
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__core_services__["b" /* Authorization */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/user/user.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_resolvers_playlist_videos_resolver__ = __webpack_require__("./resources/assets/src/app/core/resolvers/playlist-videos.resolver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_resolvers_playlist_resolver__ = __webpack_require__("./resources/assets/src/app/core/resolvers/playlist.resolver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_component__ = __webpack_require__("./resources/assets/src/app/containers/user/user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__playlists__ = __webpack_require__("./resources/assets/src/app/containers/user/playlists/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__playlist_view_playlist_view_component__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/playlist-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__user_guard__ = __webpack_require__("./resources/assets/src/app/containers/user/user.guard.ts");







var routing = __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forChild([
    {
        path: '', component: __WEBPACK_IMPORTED_MODULE_3__user_component__["a" /* UserComponent */],
        children: [
            { path: '', redirectTo: 'playlists', pathMatch: 'full' },
            { path: 'playlists', component: __WEBPACK_IMPORTED_MODULE_4__playlists__["a" /* PlaylistsComponent */] },
            {
                path: 'playlist/:id', component: __WEBPACK_IMPORTED_MODULE_5__playlist_view_playlist_view_component__["a" /* PlaylistViewComponent */],
                canActivate: [__WEBPACK_IMPORTED_MODULE_6__user_guard__["a" /* AuthGuard */]], canActivateChild: [__WEBPACK_IMPORTED_MODULE_6__user_guard__["a" /* AuthGuard */]],
                resolve: {
                    videos: __WEBPACK_IMPORTED_MODULE_0__core_resolvers_playlist_videos_resolver__["a" /* PlaylistVideosResolver */],
                    playlist: __WEBPACK_IMPORTED_MODULE_1__core_resolvers_playlist_resolver__["a" /* PlaylistResolver */]
                }
            }
        ]
    },
]);


/***/ }),

/***/ "./resources/assets/src/app/containers/user/user.scss":
/***/ (function(module, exports) {

module.exports = "app-user article {\n  padding-bottom: 5rem;\n  padding-top: 7rem; }\n\napp-user h2 small {\n  color: gray; }\n\napp-user .youtube-items-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n"

/***/ })

});
//# sourceMappingURL=index.chunk.js.map