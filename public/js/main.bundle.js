webpackJsonp(["main"],{

/***/ "./resources/assets/src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"app/containers/user/index": [
		"./resources/assets/src/app/containers/user/index.ts",
		"index"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./resources/assets/src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./resources/assets/src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-player class=\"navbar navbar-default navbar-fixed-bottom\"></app-player>\n\n<app-sidebar [class.closed]=\"sidebarCollapsed$ | async\"></app-sidebar>\n\n<div class=\"container-main\">\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./resources/assets/src/app/app.component.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  background-color: var(--app-bg-color);\n  display: block;\n  height: 100%; }\n  :host .container-main {\n    height: 100vh;\n    display: block;\n    -webkit-transition: margin 0.3s ease-out;\n    transition: margin 0.3s ease-out;\n    margin-left: 0; }\n  @media (min-width: 768px) {\n  :host .closed + .container-main {\n    margin-left: 7rem; } }\n  @media (min-width: 1024px) {\n  :host .container-main {\n    margin-left: 29.5rem; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_version_checker_service__ = __webpack_require__("./resources/assets/src/app/core/services/version-checker.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_store_app_layout__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_api_app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = (function () {
    function AppComponent(store, versionCheckerService, appApi) {
        this.store = store;
        this.versionCheckerService = versionCheckerService;
        this.appApi = appApi;
        this.sidebarCollapsed$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__core_store_app_layout__["k" /* getSidebarCollapsed */]);
        this.theme$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__core_store_app_layout__["h" /* getAppTheme */]);
        // @HostBinding('class') style = 'arctic';
        this.style = 'bumblebee';
        versionCheckerService.start();
        appApi.checkUserAuth();
    }
    AppComponent.prototype.ngOnInit = function () {
        // this.theme$.subscribe(theme => (this.style = theme));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["HostBinding"])('class'),
        __metadata("design:type", Object)
    ], AppComponent.prototype, "style", void 0);
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'body',
            template: __webpack_require__("./resources/assets/src/app/app.component.html"),
            styles: [__webpack_require__("./resources/assets/src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1__core_services_version_checker_service__["a" /* VersionCheckerService */],
            __WEBPACK_IMPORTED_MODULE_4__core_api_app_api__["a" /* AppApi */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core__ = __webpack_require__("./resources/assets/src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_components__ = __webpack_require__("./resources/assets/src/app/core/components/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__containers__ = __webpack_require__("./resources/assets/src/app/containers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routes__ = __webpack_require__("./resources/assets/src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__("./resources/assets/src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientJsonpModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["c" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_routes__["a" /* ROUTES */], { useHash: true }),
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_6__core__["a" /* CoreModule */],
                __WEBPACK_IMPORTED_MODULE_7__shared_index__["a" /* SharedModule */]
            ].concat(__WEBPACK_IMPORTED_MODULE_8__core_components__["a" /* APP_CORE_MODULES */], __WEBPACK_IMPORTED_MODULE_9__containers__["a" /* APP_CONTAINER_MODULES */]),
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ROUTES; });
var ROUTES = [
    { path: '', redirectTo: 'search', pathMatch: 'full' },
    { path: 'user', loadChildren: 'app/containers/user/index#UserModule' }
];


/***/ }),

/***/ "./resources/assets/src/app/app.themes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Themes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DEFAULT_THEME; });
var Themes = ['arctic', 'halloween', 'bumblebee'];
var DEFAULT_THEME = Themes[0];


/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar-menu/app-navbar-menu.component.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host .btn-toggle {\n    padding: 1rem 1.5rem;\n    font-size: 2rem; }\n    :host .btn-toggle .update-indicator {\n      position: absolute;\n      top: 13px;\n      /* for pulse animation*/ }\n  :host .menu-dropdown {\n    position: absolute;\n    right: 0;\n    min-width: 250px;\n    z-index: 1030;\n    -webkit-transform-origin: top right;\n            transform-origin: top right; }\n    :host .menu-dropdown.end-animation {\n      top: -35rem; }\n    :host .menu-dropdown.slideInDown {\n      -webkit-animation: slideInDown 0.5s 1;\n              animation: slideInDown 0.5s 1;\n      top: 5rem;\n      -webkit-transform-origin: top right;\n              transform-origin: top right; }\n    :host .menu-dropdown.slideOutDown {\n      -webkit-animation: slideOutDown 0.8s 1 forwards;\n              animation: slideOutDown 0.8s 1 forwards;\n      top: 5rem;\n      -webkit-transform-origin: top right;\n              transform-origin: top right; }\n  :host .menu-backdrop {\n    position: fixed;\n    top: 0;\n    bottom: 0;\n    right: 0;\n    left: 0; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar-menu/app-navbar-menu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNavbarMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_animations_fade_in_animation__ = __webpack_require__("./resources/assets/src/app/shared/animations/fade-in.animation.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Key;
(function (Key) {
    Key[Key["Backspace"] = 8] = "Backspace";
    Key[Key["Tab"] = 9] = "Tab";
    Key[Key["Enter"] = 13] = "Enter";
    Key[Key["Shift"] = 16] = "Shift";
    Key[Key["Escape"] = 27] = "Escape";
    Key[Key["ArrowLeft"] = 37] = "ArrowLeft";
    Key[Key["ArrowRight"] = 39] = "ArrowRight";
    Key[Key["ArrowUp"] = 38] = "ArrowUp";
    Key[Key["ArrowDown"] = 40] = "ArrowDown";
})(Key || (Key = {}));
var AppNavbarMenuComponent = (function () {
    function AppNavbarMenuComponent() {
        this.end = false;
        this.hide = true;
        this.signedIn = false;
        this.appVersion = {
            semver: '',
            isNewAvailable: false,
            checkingForVersion: false
        };
        this.theme = { themes: [], selected: '' };
        this.signOut = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.versionUpdate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.versionCheck = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.themeChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    Object.defineProperty(AppNavbarMenuComponent.prototype, "menuState", {
        get: function () {
            return this.hide ? 'hide' : 'show';
        },
        enumerable: true,
        configurable: true
    });
    AppNavbarMenuComponent.prototype.handleKeyPress = function (event) {
        if (event.keyCode === Key.Escape) {
            this.hideMenu();
        }
    };
    AppNavbarMenuComponent.prototype.ngOnInit = function () { };
    AppNavbarMenuComponent.prototype.handleSignOut = function () {
        this.signOut.emit();
    };
    AppNavbarMenuComponent.prototype.hideMenu = function () {
        this.hide = true;
    };
    AppNavbarMenuComponent.prototype.toggleMenu = function () {
        this.end = false;
        this.hide = !this.hide;
    };
    AppNavbarMenuComponent.prototype.handleVersionUpdate = function () {
        this.versionUpdate.emit();
    };
    AppNavbarMenuComponent.prototype.handleVersionCheck = function () {
        this.versionCheck.emit();
    };
    AppNavbarMenuComponent.prototype.updateTheme = function (theme) {
        this.themeChange.emit(theme);
    };
    AppNavbarMenuComponent.prototype.endAnimation = function (_a) {
        var toState = _a.toState;
        if (toState === 'hide') {
            this.end = true;
        }
        console.log('animation done!');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavbarMenuComponent.prototype, "signedIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavbarMenuComponent.prototype, "appVersion", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavbarMenuComponent.prototype, "theme", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarMenuComponent.prototype, "signOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarMenuComponent.prototype, "versionUpdate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarMenuComponent.prototype, "versionCheck", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarMenuComponent.prototype, "themeChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], AppNavbarMenuComponent.prototype, "handleKeyPress", null);
    AppNavbarMenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar-menu',
            animations: [__WEBPACK_IMPORTED_MODULE_1__shared_animations_fade_in_animation__["a" /* expandFadeInAnimation */]],
            template: "\n    <button class=\"btn btn-navbar btn-link ux-maker btn-toggle\"\n      (click)=\"toggleMenu()\">\n      <icon name=\"ellipsis-v\"></icon>\n      <icon *ngIf=\"appVersion.isNewAvailable\" name=\"dot-circle-o\" class=\"pulse update-indicator text-primary\"></icon>\n    </button>\n    <div class=\"menu-backdrop\" *ngIf=\"!hide\" (click)=\"hideMenu()\"></div>\n    <div class=\"panel menu-dropdown\"\n      [class.end-animation]=\"end\"\n      [@expandFadeIn]=\"menuState\"\n      (@expandFadeIn.done)=\"endAnimation($event)\"\n      >\n      <div class=\"list-group\">\n        <div *ngIf=\"appVersion.isNewAvailable\" class=\"list-group-item\">\n          <button class=\"btn btn-success\" title=\"click to update Echoes\"\n            (click)=\"handleVersionUpdate()\">\n            New Version Is Available - UPDATE NOW\n          </button>\n        </div>\n        <a class=\"list-group-item\" href=\"http://github.com/orizens/echoes-player\" target=\"_blank\">\n        <icon name=\"github\"></icon> Source Code @Github\n        </a>\n        <a class=\"list-group-item\" *ngIf=\"!hide\" href=\"https://travis-ci.org/orizens/echoes-player\" target=\"_blank\">\n        <img src=\"https://travis-ci.org/orizens/echoes-player.svg?branch=master\">\n        </a>\n        <div class=\"list-group-item\" target=\"_blank\">\n        v.<a href=\"https://github.com/orizens/echoes-player/blob/master/CHANGELOG.md\" target=\"_blank\">\n        {{ appVersion.semver }}\n        </a>\n        <button *ngIf=\"!appVersion.isNewAvailable\"\n        class=\"btn btn-info\" (click)=\"handleVersionCheck()\">\n        Check For Updates\n        </button>\n        <div *ngIf=\"appVersion.checkingForVersion\" class=\"text-info\">\n        checking for version...\n        </div>\n        </div>\n        <div class=\"list-group-item\">\n          Theme: <button-group [buttons]=\"theme.themes\" [selectedButton]=\"theme.selected\"\n            (buttonClick)=\"updateTheme($event)\"></button-group>\n        </div>\n        <a class=\"list-group-item\" href=\"http://orizens.com\" target=\"_blank\">\n        Made with <icon name=\"heart\" class=\"text-danger\"></icon> By Orizens\n        </a>\n        <button class=\"list-group-item\"\n          *ngIf=\"signedIn\"\n          (click)=\"handleSignOut()\">\n          <icon name=\"sign-out\"></icon> Sign Out\n        </button>\n      </div>\n    </div>\n  ",
            styles: [__webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar-menu/app-navbar-menu.component.scss")],
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], AppNavbarMenuComponent);
    return AppNavbarMenuComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar-menu/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_navbar_menu_component__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar-menu/app-navbar-menu.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__app_navbar_menu_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar-user/app-navbar-user.component.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  padding: 0.2rem 0; }\n  :host .navbar-link {\n    line-height: 4rem;\n    margin: 0;\n    padding: 0; }\n"

/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar-user/app-navbar-user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNavbarUserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppNavbarUserComponent = (function () {
    function AppNavbarUserComponent() {
        this.userImageUrl = '';
        this.signedIn = false;
        this.signIn = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AppNavbarUserComponent.prototype.handleSignIn = function () {
        this.signIn.emit();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavbarUserComponent.prototype, "userImageUrl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavbarUserComponent.prototype, "signedIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarUserComponent.prototype, "signIn", void 0);
    AppNavbarUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar-user',
            template: "\n    <a class=\"btn btn-link navbar-link navbar-btn\"\n      *ngIf=\"signedIn; else userNotSignedIn\"\n      [routerLink]=\"['/user']\">\n      <img [src]=\"userImageUrl\" class=\"user-icon\">\n    </a>\n    <ng-template #userNotSignedIn>\n      <span class=\"btn btn-link navbar-link navbar-btn\"\n        (click)=\"handleSignIn()\">\n        <icon name=\"sign-in\"></icon>\n        Sign In\n      </span>\n    </ng-template>\n  ",
            styles: [__webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar-user/app-navbar-user.component.scss")],
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], AppNavbarUserComponent);
    return AppNavbarUserComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar-user/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_navbar_user_component__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar-user/app-navbar-user.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__app_navbar_user_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_api_app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppNavbarComponent = (function () {
    function AppNavbarComponent(authorization, appApi) {
        this.authorization = authorization;
        this.appApi = appApi;
        this.user$ = this.appApi.user$;
        this.appVersion$ = this.appApi.appVersion$;
        this.themes$ = this.appApi.themes$;
        this.headerIcon = '';
        this.mainIcon = '';
        this.signIn = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.signOut = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.headerMainIconClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AppNavbarComponent.prototype.ngOnInit = function () { };
    AppNavbarComponent.prototype.signInUser = function () {
        this.appApi.signinUser();
        this.signIn.next();
    };
    AppNavbarComponent.prototype.signOutUser = function () {
        this.appApi.signoutUser();
        this.signOut.next();
    };
    AppNavbarComponent.prototype.isSignIn = function () {
        return this.authorization.isSignIn();
    };
    AppNavbarComponent.prototype.updateVersion = function () {
        this.appApi.updateVersion();
    };
    AppNavbarComponent.prototype.checkVersion = function () {
        this.appApi.checkVersion();
    };
    AppNavbarComponent.prototype.handleMainIconClick = function () {
        this.headerMainIconClick.emit();
    };
    AppNavbarComponent.prototype.changeTheme = function (theme) {
        this.appApi.changeTheme(theme.value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], AppNavbarComponent.prototype, "header", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavbarComponent.prototype, "headerIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavbarComponent.prototype, "mainIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarComponent.prototype, "signIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarComponent.prototype, "signOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], AppNavbarComponent.prototype, "headerMainIconClick", void 0);
    AppNavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            styles: [__webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar.scss")],
            template: "\n    <nav class=\"row navbar navbar-default navbar-fixed-top\">\n      <div class=\"navbar-container\">\n        <div class=\"navbar__content\">\n        <h3 *ngIf=\"header\" class=\"navbar__header navbar-text\">\n            <button *ngIf=\"mainIcon\" class=\"navbar-btn__main btn-transparent\"\n              (click)=\"handleMainIconClick()\">\n              <icon [name]=\"mainIcon\"></icon>\n            </button>\n            <icon [name]=\"headerIcon\" *ngIf=\"headerIcon\"></icon> {{ header }}\n          </h3>\n          <ng-content></ng-content>\n        </div>\n        <section class=\"navbar-text navbar-actions\">\n          <a class=\"navbar-action-link\" href=\"https://docs.google.com/forms/d/e/1FAIpQLSdzGRIXoHuzRFZU03EyhgwBJgJp6W1LMatz6Bn44L-5SyuxZA/viewform\" target=\"_blank\">\n            <icon name=\"star\"></icon> Request New Features\n          </a>\n          <app-navbar-user\n            [signedIn]=\"isSignIn()\"\n            [userImageUrl]=\"(user$ | async).profile.imageUrl\"\n            (signIn)=\"signInUser()\"\n            ></app-navbar-user>\n          <app-navbar-menu\n            [appVersion]=\"appVersion$ | async\"\n            [theme]=\"themes$ | async\"\n            (themeChange)=\"changeTheme($event)\"\n            [signedIn]=\"isSignIn()\"\n            (signOut)=\"signOutUser()\"\n            (versionUpdate)=\"updateVersion()\"\n            (versionCheck)=\"checkVersion()\"\n          ></app-navbar-menu>\n        </section>\n      </div>\n    </nav>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__core_services__["b" /* Authorization */], __WEBPACK_IMPORTED_MODULE_2__core_api_app_api__["a" /* AppApi */]])
    ], AppNavbarComponent);
    return AppNavbarComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/app-navbar.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  app-navbar {\n    display: block; }\n    app-navbar nav.navbar {\n      margin-left: 0;\n      background-image: none;\n      background-color: var(--navbar-bg-color);\n      border-color: var(--navbar-bg-color);\n      color: var(--navbar-text-color);\n      -webkit-box-shadow: 0 1px 7px rgba(0, 0, 0, 0.5);\n              box-shadow: 0 1px 7px rgba(0, 0, 0, 0.5);\n      -webkit-transition: margin-left 0.3s ease-out, -webkit-transform;\n      transition: margin-left 0.3s ease-out, -webkit-transform;\n      transition: transform, margin-left 0.3s ease-out;\n      transition: transform, margin-left 0.3s ease-out, -webkit-transform;\n      z-index: 1020; }\n    app-navbar .user-icon {\n      width: 4rem;\n      height: 4rem;\n      vertical-align: middle;\n      border-radius: 50%;\n      border: 1px solid #fff;\n      -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.5);\n              box-shadow: 0 0 1px rgba(0, 0, 0, 0.5); }\n    app-navbar .navbar-container {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: row;\n              flex-direction: row;\n      -webkit-box-pack: justify;\n          -ms-flex-pack: justify;\n              justify-content: space-between;\n      padding: 0.5rem 5rem; }\n      app-navbar .navbar-container .navbar__content {\n        -webkit-box-flex: 3;\n            -ms-flex: 3 0 0px;\n                flex: 3 0 0;\n        padding-left: 0;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-flow: wrap;\n            flex-flow: wrap;\n        -webkit-box-pack: center;\n            -ms-flex-pack: center;\n                justify-content: center; }\n        app-navbar .navbar-container .navbar__content .navbar-header {\n          width: auto;\n          margin-right: 7px;\n          line-height: 1rem; }\n          app-navbar .navbar-container .navbar__content .navbar-header .fa {\n            margin-right: 10px; }\n          app-navbar .navbar-container .navbar__content .navbar-header .navbar-btn__main {\n            -webkit-box-shadow: none;\n                    box-shadow: none;\n            border: none;\n            padding-top: 0;\n            padding-bottom: 0; }\n      app-navbar .navbar-container .navbar__header {\n        color: var(--brand-primary); }\n    app-navbar .navbar-text {\n      margin: 0;\n      line-height: 2; }\n    app-navbar .navbar-brand {\n      margin-left: 0 !important; }\n    app-navbar .navbar-actions {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: reverse;\n          -ms-flex-direction: column-reverse;\n              flex-direction: column-reverse;\n      -webkit-box-pack: end;\n          -ms-flex-pack: end;\n              justify-content: flex-end;\n      padding-right: 1rem;\n      position: absolute;\n      right: 0;\n      top: 0; }\n      app-navbar .navbar-actions .navbar-action-link {\n        display: none; } }\n\n@media (min-width: 768px) {\n  app-navbar nav.navbar.navbar-fixed-top {\n    margin-left: 29.5rem; }\n  app-navbar .navbar-container {\n    padding: 0.5rem 0; }\n    app-navbar .navbar-container .navbar__content {\n      display: block;\n      padding-left: 2.5rem; }\n  app-navbar .navbar-actions {\n    position: relative;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row; }\n    app-navbar .navbar-actions .navbar-action-link {\n      display: block;\n      padding: 1rem; }\n  .closed + .container-main app-navbar .navbar {\n    margin-left: 7rem; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/containers/app-navbar/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_navbar_component__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_navbar_menu__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar-menu/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_navbar_user__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/app-navbar-user/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AppNavbarModule = (function () {
    function AppNavbarModule() {
    }
    AppNavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_index__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_navbar_component__["a" /* AppNavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_4__app_navbar_menu__["a" /* AppNavbarMenuComponent */],
                __WEBPACK_IMPORTED_MODULE_5__app_navbar_user__["a" /* AppNavbarUserComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__app_navbar_component__["a" /* AppNavbarComponent */]
            ]
        })
    ], AppNavbarModule);
    return AppNavbarModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/app-search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_store_user_profile__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// selectors


var AppSearchComponent = (function () {
    function AppSearchComponent(store, playerSearchActions) {
        this.store = store;
        this.playerSearchActions = playerSearchActions;
        this.query$ = this.store.pipe(Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_3__core_store_player_search__["j" /* getQuery */]));
        this.currentPlaylist$ = this.store.pipe(Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_2__core_store_user_profile__["i" /* getUserViewPlaylist */]));
        this.queryParamPreset$ = this.store.pipe(Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_3__core_store_player_search__["k" /* getQueryParamPreset */]));
        this.presets$ = this.store.pipe(Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_3__core_store_player_search__["i" /* getPresets */]));
    }
    AppSearchComponent.prototype.ngOnInit = function () { };
    AppSearchComponent.prototype.search = function (query) {
        if (!query.hasOwnProperty('isTrusted')) {
            this.store.dispatch(this.playerSearchActions.searchNewQuery(query));
        }
    };
    AppSearchComponent.prototype.resetPageToken = function (query) {
        this.store.dispatch(this.playerSearchActions.resetPageToken());
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__core_store_player_search__["e" /* UpdateQueryAction */](query));
    };
    AppSearchComponent.prototype.searchMore = function () {
        this.store.dispatch(this.playerSearchActions.searchMoreForQuery());
    };
    AppSearchComponent.prototype.updatePreset = function (preset) {
        this.store.dispatch(this.playerSearchActions.updateQueryParam({ preset: preset.value }));
    };
    AppSearchComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-search',
            styles: [__webpack_require__("./resources/assets/src/app/containers/app-search/app-search.scss")],
            template: "\n  <article\n    infiniteScroll\n    [infiniteScrollDistance]=\"2\"\n    [infiniteScrollDisabled]=\"currentPlaylist$ | async\"\n    (scrolled)=\"searchMore()\"\n    [immediateCheck]=\"true\">\n    <app-navbar>\n      <div class=\"navbar-header\">\n        <player-search\n          [query]=\"query$ | async\"\n          (queryChange)=\"resetPageToken($event)\"\n          (search)=\"search($event)\"\n        ></player-search>\n      </div>\n      <button-group class=\"nav-toolbar\"\n        [buttons]=\"presets$ | async\"\n        [selectedButton]=\"queryParamPreset$ | async\"\n        (buttonClick)=\"updatePreset($event)\"\n      ></button-group>\n      <search-navigator></search-navigator>\n    </app-navbar>\n    <router-outlet></router-outlet>\n    </article>\n    "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_3__core_store_player_search__["c" /* PlayerSearchActions */]])
    ], AppSearchComponent);
    return AppSearchComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/app-search.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__youtube_videos__ = __webpack_require__("./resources/assets/src/app/containers/app-search/youtube-videos/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_search_component__ = __webpack_require__("./resources/assets/src/app/containers/app-search/app-search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__youtube_playlists__ = __webpack_require__("./resources/assets/src/app/containers/app-search/youtube-playlists/index.ts");




var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'search',
        component: __WEBPACK_IMPORTED_MODULE_2__app_search_component__["a" /* AppSearchComponent */],
        children: [
            { path: '', redirectTo: 'videos', pathMatch: 'full' },
            { path: 'videos', component: __WEBPACK_IMPORTED_MODULE_1__youtube_videos__["a" /* YoutubeVideosComponent */] },
            { path: 'playlists', component: __WEBPACK_IMPORTED_MODULE_3__youtube_playlists__["a" /* YoutubePlaylistsComponent */] }
        ]
    }
]);


/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/app-search.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host > article {\n    padding-top: 10.5rem; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSearchModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_search_component__ = __webpack_require__("./resources/assets/src/app/containers/app-search/app-search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_navbar__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__youtube_videos__ = __webpack_require__("./resources/assets/src/app/containers/app-search/youtube-videos/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__youtube_playlists__ = __webpack_require__("./resources/assets/src/app/containers/app-search/youtube-playlists/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__player_search__ = __webpack_require__("./resources/assets/src/app/containers/app-search/player-search/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__search_navigator__ = __webpack_require__("./resources/assets/src/app/containers/app-search/search-navigator/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_search_routing__ = __webpack_require__("./resources/assets/src/app/containers/app-search/app-search.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppSearchModule = (function () {
    function AppSearchModule() {
    }
    AppSearchModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__shared_index__["a" /* SharedModule */], __WEBPACK_IMPORTED_MODULE_4__app_navbar__["a" /* AppNavbarModule */], __WEBPACK_IMPORTED_MODULE_0__angular_forms__["c" /* ReactiveFormsModule */], __WEBPACK_IMPORTED_MODULE_9__app_search_routing__["a" /* routing */]],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_search_component__["a" /* AppSearchComponent */],
                __WEBPACK_IMPORTED_MODULE_5__youtube_videos__["a" /* YoutubeVideosComponent */],
                __WEBPACK_IMPORTED_MODULE_6__youtube_playlists__["a" /* YoutubePlaylistsComponent */],
                __WEBPACK_IMPORTED_MODULE_7__player_search__["a" /* PlayerSearchComponent */],
                __WEBPACK_IMPORTED_MODULE_8__search_navigator__["a" /* SearchNavigatorComponent */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_3__app_search_component__["a" /* AppSearchComponent */]],
            providers: []
        })
    ], AppSearchModule);
    return AppSearchModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/player-search/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__player_search_component__ = __webpack_require__("./resources/assets/src/app/containers/app-search/player-search/player-search.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__player_search_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/player-search/player-search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerSearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerSearchComponent = (function () {
    function PlayerSearchComponent(fb) {
        var _this = this;
        this.fb = fb;
        this.queryChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.search = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.params = {
            hl: 'en',
            ds: 'yt',
            xhr: 't',
            client: 'youtube'
        };
        this.searchForm = fb.group({
            searchInput: this.query
        });
        this.formState = this.searchForm.valueChanges
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["c" /* debounceTime */])(400), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["e" /* filter */])(function (value) { return !value.hasOwnProperty('isTrusted'); }))
            .subscribe(function (formState) {
            _this.onQueryChange(formState.searchInput);
        });
    }
    PlayerSearchComponent.prototype.ngOnChanges = function (changes) {
        var changedQuery = changes && changes.query;
        if (changedQuery &&
            changedQuery.currentValue &&
            changedQuery.currentValue.hasOwnProperty('length')) {
            this.patchSearchInput(changedQuery.currentValue);
        }
    };
    PlayerSearchComponent.prototype.ngOnDestroy = function () {
        this.formState.unsubscribe();
    };
    PlayerSearchComponent.prototype.patchSearchInput = function (searchInput) {
        this.searchForm.patchValue({ searchInput: searchInput }, { emitEvent: false });
    };
    PlayerSearchComponent.prototype.onQueryChange = function (query) {
        this.queryChange.emit(query);
    };
    PlayerSearchComponent.prototype.onSearch = function () {
        var searchFormState = this.searchForm.value;
        this.selectSuggestion(searchFormState.searchInput);
    };
    PlayerSearchComponent.prototype.handleSelectSuggestion = function (suggestion) {
        this.selectSuggestion(suggestion);
    };
    PlayerSearchComponent.prototype.selectSuggestion = function (suggestion) {
        if (!suggestion.hasOwnProperty('isTrusted'))
            this.search.emit(suggestion);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PlayerSearchComponent.prototype, "query", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerSearchComponent.prototype, "queryChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerSearchComponent.prototype, "search", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('mediaSearch'),
        __metadata("design:type", Object)
    ], PlayerSearchComponent.prototype, "mediaSearch", void 0);
    PlayerSearchComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'player-search',
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            styles: [__webpack_require__("./resources/assets/src/app/containers/app-search/player-search/player-search.scss")],
            template: "\n    <form class=\"navbar-form form-search\" id=\"media-explorer\"\n      [formGroup]=\"searchForm\"\n      >\n      <div class=\"form-group clearfix\">\n        <input placeholder=\"Find My Echoes...\" id=\"media-search\"\n          #mediaSearch\n          ngxTypeahead\n          [taUrl]=\"'//suggestqueries.google.com/complete/search'\"\n          [taParams]=\"params\"\n          (taSelected)=\"handleSelectSuggestion($event)\"\n          type=\"search\" class=\"form-control\" autocomplete=\"off\"\n          name=\"mediaSearch\"\n          formControlName=\"searchInput\"\n          >\n        <button class=\"btn btn-transparent btn-submit\" tooltip=\"search with echoes\">\n          <icon name=\"search\"></icon>\n        </button>\n      </div>\n    </form>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        })
        // (input)="onQueryChange()"
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
    ], PlayerSearchComponent);
    return PlayerSearchComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/player-search/player-search.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  player-search {\n    display: inline-block; }\n    player-search .form-search {\n      float: none;\n      position: relative;\n      border: 0; }\n      player-search .form-search .btn-submit {\n        position: absolute;\n        right: 0rem;\n        top: 0;\n        padding: 0.4rem 1rem 0.7rem 1rem;\n        color: #555; }\n      player-search .form-search .dropdown-menu {\n        -webkit-box-shadow: rgba(0, 0, 0, 0.4) 0px 0px 8px 0px;\n                box-shadow: rgba(0, 0, 0, 0.4) 0px 0px 8px 0px;\n        -webkit-transform: translatey(0rem);\n                transform: translatey(0rem);\n        max-height: none;\n        background-color: white;\n        opacity: 1; }\n        player-search .form-search .dropdown-menu > li.active a {\n          color: white; }\n        player-search .form-search .dropdown-menu > li a {\n          color: #444;\n          line-height: 3; }\n      player-search .form-search .form-group {\n        background-color: white;\n        border-radius: 3px;\n        position: relative; }\n      player-search .form-search input.form-control {\n        width: 25rem;\n        height: 32px;\n        border: none;\n        -webkit-box-shadow: none;\n                box-shadow: none; }\n    player-search .results {\n      position: absolute;\n      top: 32px;\n      z-index: 10;\n      -webkit-box-shadow: 0rem 0.3rem 3rem -0.5rem rgba(0, 0, 0, 0.5);\n              box-shadow: 0rem 0.3rem 3rem -0.5rem rgba(0, 0, 0, 0.5); }\n      player-search .results .list-group-item {\n        border-radius: 0; }\n        player-search .results .list-group-item.active {\n          background-color: var(--brand-primary);\n          border-color: var(--brand-primary); } }\n\n@media (min-width: 460px) {\n  player-search .form-search input.form-control {\n    display: inline-block; }\n  player-search .form-search .btn-submit {\n    position: relative;\n    top: 0;\n    right: auto; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/search-navigator/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__search_navigator_component__ = __webpack_require__("./resources/assets/src/app/containers/app-search/search-navigator/search-navigator.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__search_navigator_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/search-navigator/search-navigator.component.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host {\n    --border-active-color: var(--brand-primary);\n    --link-active-color: var(--navbar-link-color); }\n    :host .search-selector {\n      clear: both; }\n      :host .search-selector.nav > li > a {\n        text-transform: uppercase;\n        border: none;\n        border-radius: none;\n        background-color: transparent;\n        color: var(--navbar-text-color); }\n      :host .search-selector.nav-tabs {\n        border-bottom: none; }\n        :host .search-selector.nav-tabs > li.active > a {\n          border-bottom: 0.2rem solid var(--border-active-color);\n          color: var(--link-active-color);\n          -webkit-box-shadow: none;\n                  box-shadow: none; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/search-navigator/search-navigator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchNavigatorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var SearchNavigatorComponent = (function () {
    function SearchNavigatorComponent() {
        this.searchTypes = [
            { label: 'Videos', link: '/search/videos', type: __WEBPACK_IMPORTED_MODULE_1__core_store_player_search__["b" /* CSearchTypes */].VIDEO },
            { label: 'Playlists', link: '/search/playlists', type: __WEBPACK_IMPORTED_MODULE_1__core_store_player_search__["b" /* CSearchTypes */].PLAYLIST },
        ];
    }
    SearchNavigatorComponent.prototype.ngOnInit = function () { };
    SearchNavigatorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'search-navigator',
            styles: [__webpack_require__("./resources/assets/src/app/containers/app-search/search-navigator/search-navigator.component.scss")],
            template: "\n  <ul class=\"nav nav-tabs search-selector\" role=\"tablist\">\n    <li *ngFor=\"let search of searchTypes\"\n      routerLinkActive=\"active\"\n      [routerLinkActiveOptions]=\"{ exact: true }\">\n      <a routerLink=\"{{ search.link }}\">{{ search.label }}</a>\n    </li>\n  </ul>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        })
    ], SearchNavigatorComponent);
    return SearchNavigatorComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/youtube-playlists/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__youtube_playlists_component__ = __webpack_require__("./resources/assets/src/app/containers/app-search/youtube-playlists/youtube-playlists.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__youtube_playlists_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/youtube-playlists/youtube-playlists.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubePlaylistsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_api_app_player_api__ = __webpack_require__("./resources/assets/src/app/core/api/app-player.api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_animations_fade_in_animation__ = __webpack_require__("./resources/assets/src/app/shared/animations/fade-in.animation.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// selectors



var YoutubePlaylistsComponent = (function () {
    function YoutubePlaylistsComponent(store, appPlayerApi) {
        this.store = store;
        this.appPlayerApi = appPlayerApi;
        this.results$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["h" /* getPlayerSearchResults */]);
        this.isSearching$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["g" /* getIsSearching */]);
    }
    YoutubePlaylistsComponent.prototype.ngOnInit = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["f" /* UpdateSearchType */](__WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["b" /* CSearchTypes */].PLAYLIST));
        this.store.dispatch(__WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["c" /* PlayerSearchActions */].PLAYLISTS_SEARCH_START.creator());
    };
    YoutubePlaylistsComponent.prototype.playPlaylist = function (media) {
        this.appPlayerApi.playPlaylist(media);
    };
    YoutubePlaylistsComponent.prototype.queueSelectedPlaylist = function (media) {
        this.appPlayerApi.queuePlaylist(media);
    };
    YoutubePlaylistsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'youtube-playlists',
            animations: [__WEBPACK_IMPORTED_MODULE_4__shared_animations_fade_in_animation__["b" /* fadeInAnimation */]],
            template: "\n  <loader [message]=\"'Loading Awesome Playlists Results'\" [loading]=\"isSearching$ | async\"></loader>\n  <section class=\"videos-list\">\n    <div class=\"ux-maker is-flex-row is-flex-wrap is-content-aligned-h\">\n      <youtube-playlist class=\"is-media-responsive\"\n        [@fadeIn]\n        *ngFor=\"let playlist of results$ | async\"\n        link=\"\"\n        [media]=\"playlist\"\n        (play)=\"playPlaylist(playlist)\"\n        (queue)=\"queueSelectedPlaylist(playlist)\">\n      </youtube-playlist>\n    </div>\n  </section>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__core_api_app_player_api__["a" /* AppPlayerApi */]])
    ], YoutubePlaylistsComponent);
    return YoutubePlaylistsComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/youtube-videos/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__youtube_videos_component__ = __webpack_require__("./resources/assets/src/app/containers/app-search/youtube-videos/youtube-videos.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__youtube_videos_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/youtube-videos/youtube-videos.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubeVideosComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_api_app_player_api__ = __webpack_require__("./resources/assets/src/app/core/api/app-player.api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// selectors

var YoutubeVideosComponent = (function () {
    function YoutubeVideosComponent(store, appPlayerApi) {
        this.store = store;
        this.appPlayerApi = appPlayerApi;
        this.videos$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["h" /* getPlayerSearchResults */]);
        this.playlistIds$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__core_store_now_playlist__["r" /* getPlaylistMediaIds */]);
        this.loading$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["g" /* getIsSearching */]);
    }
    YoutubeVideosComponent.prototype.ngOnInit = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["f" /* UpdateSearchType */](__WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["b" /* CSearchTypes */].VIDEO));
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__core_store_player_search__["d" /* SearchCurrentQuery */]());
    };
    YoutubeVideosComponent.prototype.playSelectedVideo = function (media) {
        this.appPlayerApi.playVideo(media);
    };
    YoutubeVideosComponent.prototype.queueSelectedVideo = function (media) {
        this.appPlayerApi.queueVideo(media);
    };
    YoutubeVideosComponent.prototype.removeVideoFromPlaylist = function (media) {
        this.appPlayerApi.removeVideoFromPlaylist(media);
    };
    YoutubeVideosComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'youtube-videos',
            styles: [__webpack_require__("./resources/assets/src/app/containers/app-search/youtube-videos/youtube-videos.scss")],
            // <loader [message]="'Loading Awesome Media Results'" [loading]="loading$ | async"></loader>
            template: "\n    <youtube-list\n      [list]=\"videos$ | async\"\n      [queued]=\"playlistIds$ | async\"\n      (play)=\"playSelectedVideo($event)\"\n      (queue)=\"queueSelectedVideo($event)\"\n      (unqueue)=\"removeVideoFromPlaylist($event)\"\n    ></youtube-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__core_api_app_player_api__["a" /* AppPlayerApi */]])
    ], YoutubeVideosComponent);
    return YoutubeVideosComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/app-search/youtube-videos/youtube-videos.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block; }\n  :host .nav-toolbar {\n    margin: 0.9rem .5rem;\n    display: inline-block; }\n  @media (min-width: 320px) {\n  :host {\n    position: relative; }\n    :host .nav-toolbar .btn {\n      padding: 1rem 2.7rem; } }\n  @media (min-width: 768px) {\n  :host {\n    padding-left: 0; }\n    :host .nav-toolbar .btn {\n      padding: 0.7rem 1.5rem; }\n    :host .videos-list {\n      padding-left: 1.6rem; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/containers/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APP_CONTAINER_MODULES; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_search__ = __webpack_require__("./resources/assets/src/app/containers/app-search/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_navbar__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__playlist_view__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/index.ts");



var APP_CONTAINER_MODULES = [
    __WEBPACK_IMPORTED_MODULE_0__app_search__["a" /* AppSearchModule */],
    // UserModule,
    __WEBPACK_IMPORTED_MODULE_1__app_navbar__["a" /* AppNavbarModule */],
    __WEBPACK_IMPORTED_MODULE_2__playlist_view__["a" /* PlaylistViewModule */]
];


/***/ }),

/***/ "./resources/assets/src/app/containers/playlist-view/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistViewModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_navbar__ = __webpack_require__("./resources/assets/src/app/containers/app-navbar/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__playlist_view_component__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/playlist-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__playlist_view_proxy__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/playlist-view.proxy.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__playlist_view_routing__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/playlist-view.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var PlaylistViewModule = (function () {
    function PlaylistViewModule() {
    }
    PlaylistViewModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_index__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__app_navbar__["a" /* AppNavbarModule */],
                __WEBPACK_IMPORTED_MODULE_5__playlist_view_routing__["a" /* routing */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__playlist_view_component__["a" /* PlaylistViewComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__playlist_view_component__["a" /* PlaylistViewComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__playlist_view_proxy__["a" /* PlaylistProxy */]
            ]
        })
    ], PlaylistViewModule);
    return PlaylistViewModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/playlist-view/playlist-view.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./resources/assets/src/app/containers/playlist-view/playlist-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__playlist_view_proxy__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/playlist-view.proxy.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlaylistViewComponent = (function () {
    function PlaylistViewComponent(playlistProxy, route) {
        this.playlistProxy = playlistProxy;
        this.route = route;
        this.playlist$ = this.playlistProxy.fetchPlaylist(this.route);
        this.videos$ = this.playlistProxy.fetchPlaylistVideos(this.route);
        this.header$ = this.playlistProxy.fetchPlaylistHeader(this.route);
        this.nowPlaylistIds$ = this.playlistProxy.nowPlaylistIds$;
    }
    PlaylistViewComponent.prototype.ngOnInit = function () { };
    PlaylistViewComponent.prototype.playPlaylist = function (playlist) {
        this.playlistProxy.playPlaylist(playlist);
    };
    PlaylistViewComponent.prototype.queuePlaylist = function (playlist) {
        this.playlistProxy.queuePlaylist(playlist);
    };
    PlaylistViewComponent.prototype.queueVideo = function (media) {
        this.playlistProxy.queueVideo(media);
    };
    PlaylistViewComponent.prototype.playVideo = function (media) {
        this.playlistProxy.playVideo(media);
    };
    PlaylistViewComponent.prototype.unqueueVideo = function (media) {
        this.playlistProxy.unqueueVideo(media);
    };
    PlaylistViewComponent.prototype.handleBack = function () {
        this.playlistProxy.goBack();
    };
    PlaylistViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'playlist-view',
            styles: [__webpack_require__("./resources/assets/src/app/containers/playlist-view/playlist-view.component.scss")],
            template: "\n  <article>\n    <app-navbar [header]=\"header$ | async\"\n      [mainIcon]=\"'chevron-left'\"\n      (headerMainIconClick)=\"handleBack()\">\n    </app-navbar>\n    <div class=\"row\">\n      <playlist-viewer class=\"clearfix\"\n        [videos]=\"videos$ | async\"\n        [playlist]=\"playlist$ | async\"\n        [queuedPlaylist]=\"nowPlaylistIds$ | async\"\n        (playPlaylist)=\"playPlaylist($event)\"\n        (queuePlaylist)=\"queuePlaylist($event)\"\n        (playVideo)=\"playVideo($event)\"\n        (queueVideo)=\"queueVideo($event)\"\n        (unqueueVideo)=\"unqueueVideo($event)\"\n      ></playlist-viewer>\n    </div>\n  </article>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__playlist_view_proxy__["a" /* PlaylistProxy */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], PlaylistViewComponent);
    return PlaylistViewComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/playlist-view/playlist-view.proxy.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistProxy; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_store_user_profile__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_api_app_player_api__ = __webpack_require__("./resources/assets/src/app/core/api/app-player.api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_api_app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PlaylistProxy = (function () {
    function PlaylistProxy(store, userProfileActions, appPlayerApi, appApi) {
        this.store = store;
        this.userProfileActions = userProfileActions;
        this.appPlayerApi = appPlayerApi;
        this.appApi = appApi;
        this.nowPlaylistIds$ = this.store.pipe(Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_3__core_store_now_playlist__["r" /* getPlaylistMediaIds */]));
    }
    PlaylistProxy.prototype.goBack = function () {
        this.appApi.navigateBack();
    };
    PlaylistProxy.prototype.toRouteData = function (route) {
        return route.data;
    };
    PlaylistProxy.prototype.fetchPlaylist = function (route) {
        return this.toRouteData(route).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["f" /* map */])(function (data) { return data.playlist; }));
    };
    PlaylistProxy.prototype.fetchPlaylistVideos = function (route) {
        return this.toRouteData(route).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["f" /* map */])(function (data) { return data.videos; }));
    };
    PlaylistProxy.prototype.fetchPlaylistHeader = function (route) {
        return this.fetchPlaylist(route).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["f" /* map */])(function (playlist) {
            var snippet = playlist.snippet, contentDetails = playlist.contentDetails;
            return snippet.title + " (" + contentDetails.itemCount + " videos)";
        }));
    };
    PlaylistProxy.prototype.playPlaylist = function (playlist) {
        this.appPlayerApi.playPlaylist(playlist);
    };
    PlaylistProxy.prototype.queuePlaylist = function (playlist) {
        this.appPlayerApi.queuePlaylist(playlist);
    };
    PlaylistProxy.prototype.queueVideo = function (media) {
        this.appPlayerApi.queueVideo(media);
    };
    PlaylistProxy.prototype.playVideo = function (media) {
        this.appPlayerApi.playVideo(media);
    };
    PlaylistProxy.prototype.unqueueVideo = function (media) {
        this.appPlayerApi.removeVideoFromPlaylist(media);
    };
    PlaylistProxy = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_4__core_store_user_profile__["b" /* UserProfileActions */],
            __WEBPACK_IMPORTED_MODULE_5__core_api_app_player_api__["a" /* AppPlayerApi */],
            __WEBPACK_IMPORTED_MODULE_6__core_api_app_api__["a" /* AppApi */]])
    ], PlaylistProxy);
    return PlaylistProxy;
}());



/***/ }),

/***/ "./resources/assets/src/app/containers/playlist-view/playlist-view.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_resolvers_playlist_videos_resolver__ = __webpack_require__("./resources/assets/src/app/core/resolvers/playlist-videos.resolver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_resolvers_playlist_resolver__ = __webpack_require__("./resources/assets/src/app/core/resolvers/playlist.resolver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__playlist_view_component__ = __webpack_require__("./resources/assets/src/app/containers/playlist-view/playlist-view.component.ts");




var routing = __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forChild([
    {
        path: 'playlist/:id', component: __WEBPACK_IMPORTED_MODULE_3__playlist_view_component__["a" /* PlaylistViewComponent */],
        resolve: {
            videos: __WEBPACK_IMPORTED_MODULE_0__core_resolvers_playlist_videos_resolver__["a" /* PlaylistVideosResolver */],
            playlist: __WEBPACK_IMPORTED_MODULE_1__core_resolvers_playlist_resolver__["a" /* PlaylistResolver */]
        }
    }
]);


/***/ }),

/***/ "./resources/assets/src/app/core/api/app-player.api.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPlayerApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_app_player__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_effects_now_playlist_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/now-playlist.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AppPlayerApi = (function () {
    function AppPlayerApi(store, nowPlaylistEffects) {
        this.store = store;
        this.nowPlaylistEffects = nowPlaylistEffects;
    }
    AppPlayerApi.prototype.playPlaylist = function (playlist) {
        var _this = this;
        this.nowPlaylistEffects.playPlaylistFirstTrack$
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_1__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["k" /* take */])(1))
            .subscribe(function (media) {
            return _this.playVideo(media);
        });
        this.queuePlaylist(playlist);
    };
    AppPlayerApi.prototype.queuePlaylist = function (playlist) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_now_playlist__["c" /* LoadPlaylistAction */](playlist.id));
    };
    AppPlayerApi.prototype.playVideo = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player__["c" /* LoadAndPlay */](media));
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_now_playlist__["n" /* SelectVideo */](media));
    };
    AppPlayerApi.prototype.queueVideo = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_now_playlist__["g" /* QueueVideo */](media));
    };
    AppPlayerApi.prototype.removeVideoFromPlaylist = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_now_playlist__["j" /* RemoveVideo */](media));
    };
    AppPlayerApi.prototype.pauseVideo = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player__["d" /* PauseVideo */]());
    };
    AppPlayerApi.prototype.togglePlayer = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player__["k" /* TogglePlayer */](true));
    };
    AppPlayerApi.prototype.toggleFullScreen = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player__["b" /* FullScreen */]());
    };
    AppPlayerApi.prototype.toggleRepeat = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_now_playlist__["o" /* ToggleRepeat */]());
    };
    AppPlayerApi.prototype.resetPlayer = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player__["h" /* Reset */]());
    };
    AppPlayerApi.prototype.setupPlayer = function (player) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player__["j" /* SetupPlayer */](player));
    };
    AppPlayerApi.prototype.changePlayerState = function (event) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player__["g" /* PlayerStateChange */](event.data));
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_now_playlist__["f" /* PlayerStateChange */](event.data));
    };
    AppPlayerApi = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_5__core_effects_now_playlist_effects__["a" /* NowPlaylistEffects */]])
    ], AppPlayerApi);
    return AppPlayerApi;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/api/app.api.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_app_layout__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_router_store__ = __webpack_require__("./resources/assets/src/app/core/store/router-store/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_user_profile__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// Actions



var AppApi = (function () {
    function AppApi(store) {
        this.store = store;
        this.themes$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__store_app_layout__["i" /* getAppThemes */]);
        this.appVersion$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__store_app_layout__["j" /* getAppVersion */]);
        this.user$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["h" /* getUser */]);
    }
    AppApi.prototype.toggleSidebar = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_app_layout__["e" /* ToggleSidebar */]());
    };
    AppApi.prototype.navigateBack = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_router_store__["b" /* Back */]());
    };
    AppApi.prototype.updateVersion = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_app_layout__["f" /* UpdateAppVersion */]());
    };
    AppApi.prototype.checkVersion = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_app_layout__["b" /* CheckVersion */]());
    };
    AppApi.prototype.changeTheme = function (theme) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_app_layout__["d" /* ThemeChange */](theme));
    };
    AppApi.prototype.notifyNewVersion = function (response) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_app_layout__["c" /* RecievedAppVersion */](response));
    };
    AppApi.prototype.recievedNewVersion = function (response) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_app_layout__["c" /* RecievedAppVersion */](response));
    };
    // AUTHORIZATION
    AppApi.prototype.signinUser = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["c" /* UserSignin */]());
    };
    AppApi.prototype.signoutUser = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["f" /* UserSignout */]());
    };
    AppApi.prototype.checkUserAuth = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["a" /* CheckUserAuth */]());
    };
    AppApi = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */]])
    ], AppApi);
    return AppApi;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/api/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APP_APIS; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_player_api__ = __webpack_require__("./resources/assets/src/app/core/api/app-player.api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");


var APP_APIS = [
    __WEBPACK_IMPORTED_MODULE_0__app_player_api__["a" /* AppPlayerApi */],
    __WEBPACK_IMPORTED_MODULE_1__app_api__["a" /* AppApi */]
];


/***/ }),

/***/ "./resources/assets/src/app/core/components/app-brand/app-brand.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppBrandComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppBrandComponent = (function () {
    function AppBrandComponent(appApi) {
        this.appApi = appApi;
    }
    AppBrandComponent.prototype.ngOnInit = function () { };
    AppBrandComponent.prototype.toggleSidebar = function () {
        return this.appApi.toggleSidebar();
    };
    AppBrandComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-brand',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-brand/app-brand.scss")],
            template: "\n  <div class=\"brand-container bg-primary\"\n    (click)=\"toggleSidebar()\">\n    <section class=\"brand-text\">\n      <h3 class=\"text brand-text-item\">AMM</h3>\n      <h3 appIcon name=\"headphones\" class=\"brand-icon brand-text-item\"></h3>\n      <h3 class=\"text brand-text-item\">JB</h3>\n    </section>\n    <button class=\"btn btn-transparent sidebar-toggle\">\n      <icon name=\"bars 2x\"></icon>\n    </button>\n  </div>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__api_app_api__["a" /* AppApi */]])
    ], AppBrandComponent);
    return AppBrandComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-brand/app-brand.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host .brand-container {\n    padding: 0.5rem 1.5rem;\n    margin: 0;\n    text-transform: uppercase;\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n    :host .brand-container .brand-text {\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: row;\n              flex-direction: row; }\n      :host .brand-container .brand-text .brand-text-item {\n        margin: 0;\n        -webkit-transition: opacity 0.3s ease-in, -webkit-box-flex;\n        transition: opacity 0.3s ease-in, -webkit-box-flex;\n        transition: flex, opacity 0.3s ease-in;\n        transition: flex, opacity 0.3s ease-in, -webkit-box-flex, -ms-flex;\n        line-height: 2; }\n    :host .brand-container .brand-icon {\n      font-size: 30px;\n      margin: 0 0.3rem;\n      color: #ecf0f1;\n      padding-top: 9px;\n      -webkit-box-flex: 0;\n          -ms-flex: 0 0 0%;\n              flex: 0 0 0%; }\n      :host .brand-container .brand-icon.brand-text-item {\n        line-height: 1 !important; }\n      .closed :host .brand-container .brand-icon {\n        -webkit-box-flex: 0;\n            -ms-flex: 0;\n                flex: 0; }\n    :host .brand-container .text {\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 0%;\n              flex: 0 1 0%; }\n    :host .brand-container .sidebar-toggle {\n      -webkit-transition: -webkit-transform;\n      transition: -webkit-transform;\n      transition: transform;\n      transition: transform, -webkit-transform;\n      -webkit-transform: translateX(37.5rem);\n              transform: translateX(37.5rem);\n      color: var(--brand-primary); }\n      .closed :host .brand-container .sidebar-toggle {\n        position: absolute;\n        -webkit-transform: translateY(-4rem);\n                transform: translateY(-4rem); } }\n\n@media (min-width: 768px) {\n  :host .brand-container .sidebar-toggle {\n    -webkit-transform: translateX(0);\n            transform: translateX(0);\n    color: var(--brand-inverse-text); }\n  .closed :host .brand-container .brand-icon {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 100%;\n            flex: 1 1 100%; } }\n\n@media (min-width: 1024px) {\n  .closed :host .brand-container {\n    text-align: center; }\n  .closed :host .brand-container .text {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 00%;\n            flex: 0 1 00%;\n    width: 0;\n    overflow: hidden;\n    opacity: 0; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-brand/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppBrandModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_brand_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-brand/app-brand.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppBrandModule = (function () {
    function AppBrandModule() {
    }
    AppBrandModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__shared_index__["a" /* SharedModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__app_brand_component__["a" /* AppBrandComponent */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__app_brand_component__["a" /* AppBrandComponent */]],
            providers: [],
        })
    ], AppBrandModule);
    return AppBrandModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-navigator/app-navigator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNavigatorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppNavigatorComponent = (function () {
    function AppNavigatorComponent(store, router) {
        this.store = store;
        this.router = router;
        this.closed = false;
        this.searchType = __WEBPACK_IMPORTED_MODULE_3__core_store_player_search__["b" /* CSearchTypes */].VIDEO;
        this.searchType$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__core_store_player_search__["l" /* getSearchType */]);
        this.routes = [
            { link: 'search', icon: 'music', label: 'Explore' },
            { link: 'jobs', icon: 'music', label: 'Jobs' }
            // { link: '/user', icon: 'heart', label: 'My Profile' }
        ];
    }
    AppNavigatorComponent.prototype.ngOnInit = function () {
    };
    AppNavigatorComponent.prototype.go = function (link) {
        this.router.navigate(["/" + link + "/" + this.searchType + "s"]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavigatorComponent.prototype, "closed", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AppNavigatorComponent.prototype, "searchType", void 0);
    AppNavigatorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'app-navigator',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-navigator/app-navigator.scss")],
            template: "\n  <div class=\"list-group\"\n    [class.closed]=\"closed\">\n    <button class=\"list-group-item ux-maker\"\n      *ngFor=\"let route of routes;\"\n      (click)=\"go(route.link)\">\n      <icon [name]=\"route.icon\"></icon>\n      <span class=\"text\">{{ route.label }}</span>\n    </button>\n  </div>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_2__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]])
    ], AppNavigatorComponent);
    return AppNavigatorComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-navigator/app-navigator.scss":
/***/ (function(module, exports) {

module.exports = ":host .list-group {\n  margin: 0; }\n\n:host .list-group-item {\n  color: #ecf0f1;\n  border: none;\n  background: transparent; }\n\n:host .list-group-item:hover {\n    background: rgba(10, 10, 10, 0.2);\n    -webkit-box-shadow: inset 0px 0px 6px rgba(10, 10, 10, 0.5);\n            box-shadow: inset 0px 0px 6px rgba(10, 10, 10, 0.5); }\n\n.closed :host .list-group-item {\n    text-align: center; }\n\n:host .list-group-item .fa {\n    color: var(--brand-primary);\n    margin-right: 10px; }\n\n.closed :host .list-group-item .fa {\n      margin: 0; }\n\n@media (min-width: 768px) {\n  :host .closed {\n    width: 7rem; }\n    :host .closed a {\n      text-indent: 0.6rem; }\n    :host .closed .text {\n      display: none; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-navigator/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppNavigatorModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_navigator_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-navigator/app-navigator.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppNavigatorModule = (function () {
    function AppNavigatorModule() {
    }
    AppNavigatorModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__shared_index__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_navigator_component__["a" /* AppNavigatorComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__app_navigator_component__["a" /* AppNavigatorComponent */]
            ],
            providers: []
        })
    ], AppNavigatorModule);
    return AppNavigatorModule;
}());

// export * from './navigator.component';


/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/app-player.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPlayerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_effects_now_playlist_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/now-playlist.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__store_app_player__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_now_playlist_now_playlist_selectors__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/now-playlist.selectors.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_take__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/take.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__api_app_player_api__ = __webpack_require__("./resources/assets/src/app/core/api/app-player.api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AppPlayerComponent = (function () {
    function AppPlayerComponent(nowPlaylistService, store, nowPlaylistEffects, appPlayerApi) {
        this.nowPlaylistService = nowPlaylistService;
        this.store = store;
        this.nowPlaylistEffects = nowPlaylistEffects;
        this.appPlayerApi = appPlayerApi;
        this.player$ = this.store.select(__WEBPACK_IMPORTED_MODULE_1__store_app_player__["o" /* getPlayer */]);
        this.media$ = this.store.select(__WEBPACK_IMPORTED_MODULE_1__store_app_player__["m" /* getCurrentMedia */]);
        this.isPlayerPlaying$ = this.store.select(__WEBPACK_IMPORTED_MODULE_1__store_app_player__["n" /* getIsPlayerPlaying */]);
        this.isPlayerInRepeat$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__store_now_playlist_now_playlist_selectors__["d" /* isPlayerInRepeat */]);
        this.isPlayerFullscreen$ = this.store.select(__WEBPACK_IMPORTED_MODULE_1__store_app_player__["p" /* getPlayerFullscreen */]);
        this.isShowPlayer$ = this.store.select(__WEBPACK_IMPORTED_MODULE_1__store_app_player__["q" /* getShowPlayer */]);
        this.style = true;
    }
    AppPlayerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appPlayerApi.resetPlayer();
        this.nowPlaylistEffects.loadNextTrack$.subscribe(function (action) {
            return _this.playVideo(action.payload);
        });
    };
    AppPlayerComponent.prototype.setupPlayer = function (player) {
        this.appPlayerApi.setupPlayer(player);
    };
    AppPlayerComponent.prototype.updatePlayerState = function (event) {
        this.appPlayerApi.changePlayerState(event);
    };
    AppPlayerComponent.prototype.playVideo = function (media) {
        this.appPlayerApi.playVideo(media);
    };
    AppPlayerComponent.prototype.pauseVideo = function () {
        this.appPlayerApi.pauseVideo();
    };
    AppPlayerComponent.prototype.togglePlayer = function () {
        this.appPlayerApi.togglePlayer();
    };
    AppPlayerComponent.prototype.toggleFullScreen = function () {
        this.appPlayerApi.toggleFullScreen();
    };
    AppPlayerComponent.prototype.playNextTrack = function () {
        this.nowPlaylistService.selectNextIndex();
        this.playVideo(this.nowPlaylistService.getCurrent());
    };
    AppPlayerComponent.prototype.playPreviousTrack = function () {
        this.nowPlaylistService.selectPreviousIndex();
        this.playVideo(this.nowPlaylistService.getCurrent());
    };
    AppPlayerComponent.prototype.toggleRepeat = function () {
        this.appPlayerApi.toggleRepeat();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["HostBinding"])('class.youtube-player'),
        __metadata("design:type", Object)
    ], AppPlayerComponent.prototype, "style", void 0);
    AppPlayerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Component"])({
            selector: 'app-player',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-player/app-player.scss")],
            template: "\n  <section\n    [class.show-youtube-player]=\"isShowPlayer$ | async\"\n    [class.fullscreen]=\"(isPlayerFullscreen$ | async).on\">\n    <div class=\"yt-player ux-maker\">\n      <player-resizer\n        (toggle)=\"togglePlayer()\"\n        [fullScreen]=\"isShowPlayer$ | async\"\n      ></player-resizer>\n      <youtube-player\n        (ready)=\"setupPlayer($event)\"\n        (change)=\"updatePlayerState($event)\"\n      ></youtube-player>\n    </div>\n    <div class=\"container\">\n      <image-blur [media]=\"media$ | async\" *ngIf=\"!(isPlayerFullscreen$ | async).on\"></image-blur>\n      <media-info\n        [player]=\"player$ | async\"\n        [minimized]=\"media$ | async\"\n        (thumbClick)=\"toggleFullScreen()\"\n      ></media-info>\n      <player-controls class=\"controls-container nicer-ux\"\n        [isRepeat]=\"isPlayerInRepeat$ | async\"\n        [playing]=\"isPlayerPlaying$ | async\"\n        [media]=\"media$ | async\"\n        (pause)=\"pauseVideo()\"\n        (next)=\"playNextTrack()\"\n        (play)=\"playVideo($event)\"\n        (previous)=\"playPreviousTrack()\"\n        (repeat)=\"toggleRepeat()\"\n      ></player-controls>\n    </div>\n  </section>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_4__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__core_services__["d" /* NowPlaylistService */],
            __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_0__core_effects_now_playlist_effects__["a" /* NowPlaylistEffects */],
            __WEBPACK_IMPORTED_MODULE_7__api_app_player_api__["a" /* AppPlayerApi */]])
    ], AppPlayerComponent);
    return AppPlayerComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/app-player.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  z-index: 1031 !important; }\n  :host .show-youtube-player .yt-player {\n    -webkit-transform: translate3d(-10%, -90%, 0) scale(0.8, 0.8);\n            transform: translate3d(-10%, -90%, 0) scale(0.8, 0.8); }\n  :host .show-youtube-player .social-shares {\n    display: block; }\n  :host .fullscreen .btn.fullscreen {\n    border-bottom: 4px solid #1abc9c; }\n  :host .fullscreen .btn.fullscreen i {\n      -webkit-transform: scale(0.8) rotate(45deg);\n      transform: scale(0.8) rotate(45deg); }\n  :host .fullscreen.show-youtube-player .container {\n    z-index: 1040;\n    position: fixed;\n    -webkit-transform: translateY(-9rem);\n            transform: translateY(-9rem);\n    opacity: 0; }\n  :host .fullscreen.show-youtube-player .container:hover {\n      opacity: 0.8; }\n  :host .fullscreen.show-youtube-player .yt-player {\n    top: 40px; }\n  :host .fullscreen.show-youtube-player .yt-player:hover + .container {\n      -webkit-transform: translateY(-9rem);\n              transform: translateY(-9rem);\n      opacity: 0.3; }\n  :host .container {\n    overflow: hidden;\n    padding: 0;\n    z-index: 1030;\n    background: var(--player-controls-bar-bg) url(\"/assets/images/developed-with-youtube-sentence-case-light.png\") no-repeat right center;\n    background-size: contain;\n    position: relative;\n    -webkit-transition: all 0.3s;\n    transition: all 0.3s;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row; }\n  :host .container media-info {\n      width: 41%;\n      z-index: 10; }\n  :host .yt-player {\n    z-index: 1040;\n    position: absolute;\n    left: 0;\n    top: 0;\n    -webkit-box-shadow: 1px -1px 2px 0px black;\n            box-shadow: 1px -1px 2px 0px black;\n    background: var(--brand-dark-bg-color-transparent);\n    -webkit-transform: translate3d(-41%, -38%, 0) scale(0.23);\n            transform: translate3d(-41%, -38%, 0) scale(0.23); }\n  :host .show-youtube-player .btn.show-player {\n    -webkit-transform: translatey(0rem);\n            transform: translatey(0rem);\n    border-bottom: 4px solid #1abc9c; }\n  @media (min-width: 320px) {\n  :host {\n    border: 0 !important; }\n    :host .yt-player iframe {\n      display: block;\n      clear: both; }\n    :host .dropdown .dropdown-menu,\n    :host .dropup .dropdown-menu {\n      position: absolute; }\n    :host .nav.navbar-nav {\n      margin: 0; }\n    :host .current-playlist-info {\n      display: none; }\n      :host .current-playlist-info .playlist-provider-item {\n        height: 52px; }\n        :host .current-playlist-info .playlist-provider-item img {\n          width: 60px; }\n    :host .current-track-info-container .btn.dropdown-toggle {\n      padding: 1px;\n      height: 46px;\n      width: 100px; } }\n  @media (min-width: 768px) {\n  :host .fullscreen.show-youtube-player .container {\n    width: 70%;\n    left: 15%;\n    border-radius: 50px; }\n  :host .fullscreen.show-youtube-player .yt-player {\n    top: 40px;\n    -webkit-transform: translate3d(0, -100%, 0) scale(1, 1);\n            transform: translate3d(0, -100%, 0) scale(1, 1); }\n    :host .fullscreen.show-youtube-player .yt-player:hover + .container {\n      -webkit-transform: translatey(-9rem);\n      transform: translatey(-9rem); }\n  :host .current-track-info-container .btn.dropdown-toggle {\n    width: 405px; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/image-blur/image-blur.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageBlurComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImageBlurComponent = (function () {
    function ImageBlurComponent() {
    }
    Object.defineProperty(ImageBlurComponent.prototype, "style", {
        get: function () {
            var hasMedia = this.media && this.media.snippet;
            return {
                backgroundImage: hasMedia
                    ? "url(" + this.extractBestImage(hasMedia.thumbnails) + ")"
                    : ''
            };
        },
        enumerable: true,
        configurable: true
    });
    ImageBlurComponent.prototype.extractBestImage = function (thumbnails) {
        var quality = thumbnails && thumbnails.hasOwnProperty('high') ? 'high' : 'default';
        var hasContent = thumbnails && quality && thumbnails[quality];
        return hasContent ? thumbnails[quality].url : '';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ImageBlurComponent.prototype, "media", void 0);
    ImageBlurComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'image-blur',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-player/image-blur/image-blur.scss")],
            template: "\n  <div class=\"media-bg\" [ngStyle]=\"style\"></div>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        })
    ], ImageBlurComponent);
    return ImageBlurComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/image-blur/image-blur.scss":
/***/ (function(module, exports) {

module.exports = ":host .media-bg {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  top: 0;\n  background-size: 40%;\n  background-position: top;\n  -webkit-filter: blur(20px);\n          filter: blur(20px);\n  overflow: hidden;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  height: 290px;\n  -webkit-transform: rotate(-20deg) translateY(-120px);\n          transform: rotate(-20deg) translateY(-120px); }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/image-blur/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__image_blur_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/image-blur/image-blur.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__image_blur_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPlayerModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_player_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/app-player.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__media_info__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/media-info/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__player_controls_player_controls_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/player-controls/player-controls.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__player_resizer_player_resizer_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/player-resizer/player-resizer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__image_blur__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/image-blur/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppPlayerModule = (function () {
    function AppPlayerModule() {
    }
    AppPlayerModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_index__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_player_component__["a" /* AppPlayerComponent */],
                __WEBPACK_IMPORTED_MODULE_3__media_info__["a" /* MediaInfoComponent */],
                __WEBPACK_IMPORTED_MODULE_4__player_controls_player_controls_component__["a" /* PlayerControlsComponent */],
                __WEBPACK_IMPORTED_MODULE_5__player_resizer_player_resizer_component__["a" /* PlayerResizerComponent */],
                __WEBPACK_IMPORTED_MODULE_6__image_blur__["a" /* ImageBlurComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__app_player_component__["a" /* AppPlayerComponent */]
            ]
        })
    ], AppPlayerModule);
    return AppPlayerModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/media-info/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__media_info_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/media-info/media-info.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__media_info_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/media-info/media-info.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MediaInfoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MediaInfoComponent = (function () {
    function MediaInfoComponent() {
        this.player = {};
        this.thumbClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    MediaInfoComponent.prototype.ngOnInit = function () { };
    MediaInfoComponent.prototype.keyEvent = function (event) {
        if (this.player.fullscreen.on) {
            this.handleThumbClick();
        }
    };
    MediaInfoComponent.prototype.handleThumbClick = function () {
        this.thumbClick.next();
    };
    Object.defineProperty(MediaInfoComponent.prototype, "_minimized", {
        get: function () {
            return !this.minimized.hasOwnProperty('id');
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MediaInfoComponent.prototype, "player", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MediaInfoComponent.prototype, "minimized", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], MediaInfoComponent.prototype, "thumbClick", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:keyup.Escape', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], MediaInfoComponent.prototype, "keyEvent", null);
    MediaInfoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'media-info',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-player/media-info/media-info.scss")],
            template: "\n  <article class=\"media-info ellipsis\">\n    <h3 class=\"yt-media-title ellipsis\">\n      <aside class=\"media-thumb-container pull-left\"\n        title=\"maximize / minimize\"\n        (click)=\"handleThumbClick()\">\n        <img class=\"media-thumb\" src=\"{{ player?.media?.snippet?.thumbnails?.default?.url }}\">\n        <icon name=\"arrows-alt\" [class.invisible]=\"_minimized\"></icon>\n      </aside>\n      <a class=\"title\">{{ player?.media?.snippet?.title }}</a>\n    </h3>\n  </article>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], MediaInfoComponent);
    return MediaInfoComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/media-info/media-info.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  --media-title-text-color: var(--brand-primary);\n  --media-title-shadow: var(--brand-dark-bg-color);\n  --media-expand-icon-bg-color: var(--brand-dark-bg-color-transparent);\n  line-height: 1.5rem; }\n  :host .yt-media-title {\n    color: var(--media-title-text-color);\n    margin: 0 1rem 0 0;\n    display: inline-block;\n    max-width: 40rem;\n    font-size: 1.5rem;\n    line-height: 5.5rem; }\n  :host .yt-media-title .media-thumb-container {\n      position: relative;\n      margin-right: 1rem;\n      cursor: pointer; }\n  :host .yt-media-title .media-thumb-container .media-thumb {\n        height: 5.5rem; }\n  :host .yt-media-title .media-thumb-container .fa {\n        position: absolute;\n        right: 0;\n        bottom: 0;\n        background: var(--media-expand-icon-bg-color); }\n  :host .yt-media-title .title {\n      text-shadow: 0px 0px 0.5rem var(--media-title-shadow);\n      font-size: 2rem; }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/player-controls/player-controls.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerControlsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlayerControlsComponent = (function () {
    function PlayerControlsComponent() {
        this.isRepeat = false;
        this.playing = false;
        this.play = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.pause = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.previous = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.next = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.repeat = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.controls = [
            {
                title: 'previous',
                icon: 'step-backward',
                handler: this.handlePrevious,
                feature: 'previous'
            },
            {
                title: 'pause',
                icon: 'pause',
                handler: this.handlePause,
                feature: 'pause'
            },
            {
                title: 'play',
                icon: 'play',
                handler: this.handlePlay,
                feature: 'play'
            },
            {
                title: 'play next track',
                icon: 'step-forward',
                handler: this.handleNext,
                feature: 'next'
            },
            {
                title: 'repeate playlist',
                icon: 'refresh',
                handler: this.handleRepeat,
                feature: 'repeat'
            }
        ];
    }
    PlayerControlsComponent.prototype.handlePlay = function () {
        this.play.emit(this.media);
    };
    PlayerControlsComponent.prototype.handlePrevious = function () {
        this.previous.emit();
    };
    PlayerControlsComponent.prototype.handlePause = function () {
        this.pause.emit();
    };
    PlayerControlsComponent.prototype.handleNext = function () {
        this.next.emit();
    };
    PlayerControlsComponent.prototype.handleRepeat = function () {
        this.repeat.emit();
    };
    PlayerControlsComponent.prototype.handleControl = function (control) {
        control.handler.call(this);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "media", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.yt-repeat-on'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "isRepeat", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.yt-playing'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "playing", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "play", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "pause", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "previous", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "next", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerControlsComponent.prototype, "repeat", void 0);
    PlayerControlsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'player-controls',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-player/player-controls/player-controls.scss")],
            template: "\n  <div class=\"btn-group player-controls\">\n    <button *ngFor=\"let control of controls\"\n      [title]=\"control.title\"\n      class=\"btn btn-default btn-lg navbar-btn\"\n      [ngClass]=\"[control.feature]\"\n      (click)=\"handleControl(control)\">\n      <icon [name]=\"control.icon\"></icon>\n    </button>\n  </div>\n  "
        })
    ], PlayerControlsComponent);
    return PlayerControlsComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/player-controls/player-controls.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  --btn-color:var(--brand-inverse-text);\n  --btn-control-primary-color: var(--brand-primary); }\n  :host .btn.next, :host .btn.previous, :host .btn.repeat {\n    padding: 1.5rem 1.7rem; }\n  :host .btn {\n    border: none;\n    background: transparent;\n    color: var(--btn-color);\n    border-radius: 0;\n    outline: none;\n    font-size: 1.5rem;\n    vertical-align: middle;\n    line-height: 0; }\n  :host .btn:hover {\n      border: none;\n      background: transparent;\n      color: var(--btn-color);\n      border-radius: 0;\n      outline: none;\n      font-size: 1.5rem; }\n  :host .btn.pause {\n      display: none; }\n  :host .btn.play, :host .btn.pause {\n      border: 0;\n      color: var(--btn-control-primary-color);\n      padding: 0.9rem 1.4rem;\n      margin: 0;\n      font-size: 3rem;\n      top: 3px; }\n  :host .show-player {\n    -webkit-transform: translatey(0);\n            transform: translatey(0); }\n  :host .player-controls {\n    padding: .5rem; }\n  :host .play,\n  :host .pause {\n    mix-blend-mode: screen; }\n  :host.yt-playing .player-controls .play {\n    display: none; }\n  :host.yt-playing .player-controls .pause {\n    display: inline-block; }\n  :host.yt-repeat-on .player-controls .repeat {\n    color: var(--btn-control-primary-color); }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/player-resizer/player-resizer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerResizerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlayerResizerComponent = (function () {
    function PlayerResizerComponent() {
        this.toggle = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    PlayerResizerComponent.prototype.ngOnInit = function () { };
    PlayerResizerComponent.prototype.togglePlayer = function () {
        this.toggle.next();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], PlayerResizerComponent.prototype, "fullScreen", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlayerResizerComponent.prototype, "toggle", void 0);
    PlayerResizerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'player-resizer',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-player/player-resizer/player-resizer.scss")],
            template: "\n    <button title=\"minimize / maximize player\"\n      [class.full-screen]=\"!fullScreen\"\n      (click)=\"togglePlayer()\"\n      class=\"btn btn-sm navbar-btn show-player pull-right\">\n      <icon name=\"chevron-down\" class=\"icon-minimize\"></icon>\n      <icon name=\"expand\" class=\"icon-max\"></icon>\n    </button>\n  "
        }),
        __metadata("design:paramtypes", [])
    ], PlayerResizerComponent);
    return PlayerResizerComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-player/player-resizer/player-resizer.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host .btn.show-player {\n    background-color: var(--brand-primary);\n    color: var(--brand-inverse-text); }\n  :host .full-screen.show-player {\n    -webkit-transform: scale(5);\n            transform: scale(5);\n    background-color: transparent;\n    color: var(--brand-primary);\n    border: none;\n    left: 14rem;\n    position: absolute;\n    font-size: 4rem;\n    top: 12rem;\n    padding: 1rem 2.5rem; }\n  :host .icon-max {\n    display: block; }\n  :host .icon-minimize {\n    display: none; }\n  .show-youtube-player :host {\n    display: block; }\n    .show-youtube-player :host .icon-max {\n      display: none; }\n    .show-youtube-player :host .icon-minimize {\n      display: block; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-sidebar/app-sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_sidebar_proxy__ = __webpack_require__("./resources/assets/src/app/core/components/app-sidebar/app-sidebar.proxy.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppSidebarComponent = (function () {
    function AppSidebarComponent(appSidebarProxy) {
        this.appSidebarProxy = appSidebarProxy;
        this.sidebarCollapsed$ = this.appSidebarProxy.sidebarCollapsed$;
        this.searchType$ = this.appSidebarProxy.searchType$;
    }
    AppSidebarComponent.prototype.toggleSidebar = function () {
        this.appSidebarProxy.toggleSidebar();
    };
    AppSidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sidebar',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/app-sidebar/app-sidebar.scss")],
            template: "\n  <div id=\"sidebar\" class=\"sidebar ux-maker\"\n    [class.closed]=\"sidebarCollapsed$ | async\">\n    <div class=\"sidebar-backdrop\" (click)=\"toggleSidebar()\"></div>\n    <nav class=\"navbar navbar-transparent\">\n      <app-brand></app-brand>\n      <app-navigator [closed]=\"sidebarCollapsed$ | async\" [searchType]=\"searchType$ | async\">\n      </app-navigator>\n    </nav>\n    <now-playing></now-playing>\n  </div>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_sidebar_proxy__["a" /* AppSidebarProxy */]])
    ], AppSidebarComponent);
    return AppSidebarComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-sidebar/app-sidebar.proxy.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSidebarProxy; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_app_layout__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppSidebarProxy = (function () {
    function AppSidebarProxy(store, appApi) {
        this.store = store;
        this.appApi = appApi;
        this.sidebarCollapsed$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__store_app_layout__["k" /* getSidebarCollapsed */]);
        this.searchType$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__store_player_search__["l" /* getSearchType */]);
    }
    AppSidebarProxy.prototype.toggleSidebar = function () {
        this.appApi.toggleSidebar();
    };
    AppSidebarProxy = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_2__api_app_api__["a" /* AppApi */]])
    ], AppSidebarProxy);
    return AppSidebarProxy;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/app-sidebar/app-sidebar.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host {\n    position: fixed;\n    bottom: 0;\n    top: 0;\n    left: -30rem;\n    margin: 0;\n    padding: 0rem;\n    z-index: 1030;\n    -webkit-transition: left .3s;\n    transition: left .3s;\n    -webkit-transition-timing-function: cubic-bezier(0.65, 0.05, 0.36, 1);\n            transition-timing-function: cubic-bezier(0.65, 0.05, 0.36, 1); }\n    :host.closed {\n      left: 0; }\n    :host .sidebar {\n      --sidebar-bg-color: var(--sidebar-bg);\n      --sidebar-brand-bg: var(--brand-primary);\n      --sidebar-text-color: var(--sidebar-text);\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: justify;\n          -ms-flex-pack: justify;\n              justify-content: space-between;\n      -webkit-box-align: initial;\n          -ms-flex-align: initial;\n              align-items: initial;\n      -ms-flex-item-align: auto;\n          align-self: auto;\n      height: 100%;\n      width: 29.5rem;\n      z-index: 1030;\n      -webkit-transform: translatex(-30rem);\n              transform: translatex(-30rem);\n      background-color: var(--sidebar-bg-color);\n      padding-bottom: 21%;\n      border: none;\n      border-radius: 0; }\n      :host .sidebar .navbar-brand {\n        float: none;\n        width: 100%;\n        line-height: 5rem;\n        padding-left: 1.5rem;\n        color: #323538; }\n      :host .sidebar.closed {\n        -webkit-transform: none;\n                transform: none; }\n        :host .sidebar.closed .sidebar-backdrop {\n          position: fixed;\n          left: 29.5rem;\n          right: 0;\n          top: 0;\n          bottom: 0;\n          display: block;\n          background-color: var(--brand-dark-bg-color-transparent); }\n      :host .sidebar .sidebar-backdrop {\n        display: none; }\n      :host .sidebar .navbar {\n        margin-bottom: 0;\n        border-bottom: none;\n        z-index: 1030;\n        border-radius: 0;\n        border: none; }\n      :host .sidebar .sidebar-backdrop {\n        z-index: 1029; } }\n\n@media (min-width: 768px) {\n  :host {\n    left: 0; }\n    :host .sidebar {\n      -webkit-transition: width .3s ease-out, -webkit-transform;\n      transition: width .3s ease-out, -webkit-transform;\n      transition: transform, width .3s ease-out;\n      transition: transform, width .3s ease-out, -webkit-transform;\n      -webkit-transform: none;\n              transform: none; }\n      :host .sidebar.closed > .sidebar-backdrop {\n        display: none; }\n      :host .sidebar + .sidebar-backdrop {\n        display: none; }\n      :host .sidebar.closed {\n        width: 7rem; }\n        :host .sidebar.closed .navbar-brand {\n          /*width: @sidebar-closed-width;*/\n          padding: 0;\n          text-align: center; } }\n\n@media (min-width: 1024px) {\n  .sidebar-left-fixed.closed {\n    -webkit-transform: translatex(0rem);\n    transform: translatex(0rem); }\n    .sidebar-left-fixed.closed .sidebar-backdrop {\n      display: none; }\n  .sidebar .sidebar-backdrop {\n    display: none; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/app-sidebar/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSidebarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_sidebar_component__ = __webpack_require__("./resources/assets/src/app/core/components/app-sidebar/app-sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_brand__ = __webpack_require__("./resources/assets/src/app/core/components/app-brand/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_navigator__ = __webpack_require__("./resources/assets/src/app/core/components/app-navigator/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__now_playing__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_sidebar_proxy__ = __webpack_require__("./resources/assets/src/app/core/components/app-sidebar/app-sidebar.proxy.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppSidebarModule = (function () {
    function AppSidebarModule() {
    }
    AppSidebarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_index__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_3__app_brand__["a" /* AppBrandModule */],
                __WEBPACK_IMPORTED_MODULE_4__app_navigator__["a" /* AppNavigatorModule */],
                __WEBPACK_IMPORTED_MODULE_5__now_playing__["a" /* NowPlayingModule */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_2__app_sidebar_component__["a" /* AppSidebarComponent */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__app_sidebar_component__["a" /* AppSidebarComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_6__app_sidebar_proxy__["a" /* AppSidebarProxy */]],
        })
    ], AppSidebarModule);
    return AppSidebarModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APP_CORE_MODULES; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_player__ = __webpack_require__("./resources/assets/src/app/core/components/app-player/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_sidebar__ = __webpack_require__("./resources/assets/src/app/core/components/app-sidebar/index.ts");


var APP_CORE_MODULES = [
    __WEBPACK_IMPORTED_MODULE_0__app_player__["a" /* AppPlayerModule */],
    __WEBPACK_IMPORTED_MODULE_1__app_sidebar__["a" /* AppSidebarModule */]
];


/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NowPlayingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_index__ = __webpack_require__("./resources/assets/src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__now_playing_component__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__now_playlist__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__now_playlist_filter__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist-filter/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NowPlayingModule = (function () {
    function NowPlayingModule() {
    }
    NowPlayingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__shared_index__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__now_playing_component__["a" /* NowPlayingComponent */],
                __WEBPACK_IMPORTED_MODULE_3__now_playlist__["a" /* NowPlaylistComponent */],
                __WEBPACK_IMPORTED_MODULE_3__now_playlist__["b" /* NowPlaylistTrackComponent */],
                __WEBPACK_IMPORTED_MODULE_4__now_playlist_filter__["a" /* NowPlaylistFilterComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__now_playing_component__["a" /* NowPlayingComponent */]
            ]
        })
    ], NowPlayingModule);
    return NowPlayingModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NowPlayingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_now_playlist_service__ = __webpack_require__("./resources/assets/src/app/core/services/now-playlist.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_app_player_app_player_actions__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/app-player.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__now_playlist__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NowPlayingComponent = (function () {
    function NowPlayingComponent(store, nowPlaylistService) {
        this.store = store;
        this.nowPlaylistService = nowPlaylistService;
    }
    NowPlayingComponent.prototype.ngOnInit = function () {
        this.nowPlaylist$ = this.nowPlaylistService.playlist$;
    };
    NowPlayingComponent.prototype.selectVideo = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player_app_player_actions__["f" /* PlayVideo */](media));
        this.nowPlaylistService.updateIndexByMedia(media.id);
    };
    NowPlayingComponent.prototype.sortVideo = function () { };
    NowPlayingComponent.prototype.updateFilter = function (searchFilter) {
        this.nowPlaylistService.updateFilter(searchFilter);
    };
    NowPlayingComponent.prototype.resetFilter = function () {
        this.nowPlaylistService.updateFilter('');
    };
    NowPlayingComponent.prototype.clearPlaylist = function () {
        this.nowPlaylistService.clearPlaylist();
    };
    NowPlayingComponent.prototype.removeVideo = function (media) {
        this.nowPlaylistService.removeVideo(media);
    };
    NowPlayingComponent.prototype.onHeaderClick = function () {
        this.nowPlaylistComponent.scrollToActiveTrack();
    };
    NowPlayingComponent.prototype.selectTrackInVideo = function (trackEvent) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__store_app_player_app_player_actions__["f" /* PlayVideo */](trackEvent.media));
        this.nowPlaylistService.seekToTrack(trackEvent);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__now_playlist__["a" /* NowPlaylistComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__now_playlist__["a" /* NowPlaylistComponent */])
    ], NowPlayingComponent.prototype, "nowPlaylistComponent", void 0);
    NowPlayingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'now-playing',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playing.scss")],
            template: "\n  <div class=\"sidebar-pane\">\n    <now-playlist-filter\n      [playlist]=\"nowPlaylist$ | async\"\n      (clear)=\"clearPlaylist()\"\n      (filter)=\"updateFilter($event)\"\n      (reset)=\"resetFilter()\"\n      (headerClick)=\"onHeaderClick()\"\n    ></now-playlist-filter>\n    <now-playlist\n      [playlist]=\"nowPlaylist$ | async\"\n      (select)=\"selectVideo($event)\"\n      (selectTrack)=\"selectTrackInVideo($event)\"\n      (remove)=\"removeVideo($event)\"\n    ></now-playlist>\n  </div>\n  ",
            // (sort)="sortVideo($event)"
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_2__core_services_now_playlist_service__["a" /* NowPlaylistService */]])
    ], NowPlayingComponent);
    return NowPlayingComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playing.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  -webkit-box-flex: 16;\n      -ms-flex-positive: 16;\n          flex-grow: 16;\n  -ms-flex-negative: 0;\n      flex-shrink: 0;\n  -ms-flex-preferred-size: 0rem;\n      flex-basis: 0rem;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n  :host .sidebar-pane {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 0px;\n            flex: 1 1 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    overflow-y: hidden; }\n  :host now-playlist-filter {\n    -webkit-box-flex: 0;\n        -ms-flex-positive: 0;\n            flex-grow: 0;\n    -ms-flex-negative: 1;\n        flex-shrink: 1;\n    -ms-flex-preferred-size: 0rem;\n        flex-basis: 0rem; }\n  :host now-playlist {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 0px;\n            flex: 1 1 0;\n    overflow-y: auto;\n    padding-bottom: 31rem; }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist-filter/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__now_playlist_filter_component__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist-filter/now-playlist-filter.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__now_playlist_filter_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist-filter/now-playlist-filter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NowPlaylistFilterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NowPlaylistFilterComponent = (function () {
    function NowPlaylistFilterComponent() {
        // @Output() save = new EventEmitter();
        this.clear = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.filter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.reset = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.headerClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    NowPlaylistFilterComponent.prototype.handleFilterChange = function (searchFilter) {
        this.filter.next(searchFilter);
    };
    NowPlaylistFilterComponent.prototype.resetSearchFilter = function () {
        this.reset.next('');
    };
    NowPlaylistFilterComponent.prototype.isFilterEmpty = function () {
        return this.playlist.filter === '';
    };
    NowPlaylistFilterComponent.prototype.clearPlaylist = function () {
        this.clear.next('');
    };
    NowPlaylistFilterComponent.prototype.isPlaylistEmpty = function () {
        return this.playlistLength === 0;
    };
    NowPlaylistFilterComponent.prototype.onNowPlayingClick = function () {
        this.headerClick.next();
    };
    Object.defineProperty(NowPlaylistFilterComponent.prototype, "playlistLength", {
        get: function () {
            return this.playlist.videos.length;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], NowPlaylistFilterComponent.prototype, "playlist", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistFilterComponent.prototype, "clear", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistFilterComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistFilterComponent.prototype, "reset", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistFilterComponent.prototype, "headerClick", void 0);
    NowPlaylistFilterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'now-playlist-filter',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist-filter/now-playlist-filter.scss")],
            template: "\n  <section class=\"nav-header user-playlists-filter\">\n  <span class=\"playlist-header\" (click)=\"onNowPlayingClick()\">\n      <icon name=\"play-circle-o\" class=\"text-primary\"></icon>\n      <span class=\"text btn-transparent playlist-count\"\n        tooltip=\"Reveal now playing track\">\n        Now Playing <span *ngIf=\"!isPlaylistEmpty()\">({{ playlistLength }})</span>\n      </span>\n    </span>\n    <button class=\"btn btn-link btn-xs btn-clear\"\n      tooltip=\"Clear All Tracks In Now Playlist\"\n      [disabled]=\"isPlaylistEmpty()\"\n      (click)=\"clearPlaylist()\">\n      <icon name=\"trash-o\"></icon>\n    </button>\n    <button class=\"btn btn-link btn-xs btn-save\" title=\"Save All These Tracks To A New Playlist\"\n      disabled\n      ng-disabled=\"!nowPlaylistFilter.playlist.length\"\n      ng-click=\"nowPlaylistFilter.togglePlaylistSaver()\">\n      <icon name=\"cloud-upload\"></icon>\n    </button>\n    <div class=\"playlist-filter\">\n      <icon name=\"search\" *ngIf=\"isFilterEmpty()\"></icon>\n      <icon name=\"remove\" class=\"text-danger\"\n        *ngIf=\"!isFilterEmpty()\"\n        (click)=\"resetSearchFilter()\"\n      ></icon>\n      <input type=\"search\" name=\"playlist-search\"\n        [value]=\"playlist.filter\"\n        #searchFilter\n        (input)=\"handleFilterChange(searchFilter.value)\">\n    </div>\n  </section>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], NowPlaylistFilterComponent);
    return NowPlaylistFilterComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist-filter/now-playlist-filter.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host .user-playlists-filter {\n    --filter-bg: var(--sidebar-bg);\n    --filter-darker-bg: var(--sidebar-bg-secondary);\n    position: relative;\n    background-color: var(--filter-bg);\n    padding-top: 0.5rem;\n    padding-bottom: 0.5rem;\n    padding-left: 1.5rem;\n    font-size: 10px; }\n    :host .user-playlists-filter .playlist-count,\n    :host .user-playlists-filter .playlist-filter {\n      color: var(--sidebar-text-color); }\n    :host .user-playlists-filter.nav-header {\n      -webkit-box-shadow: -1rem -0.2rem 2rem #000 !important;\n              box-shadow: -1rem -0.2rem 2rem #000 !important;\n      line-height: 26px;\n      color: var(--sidebar-text-color);\n      text-transform: uppercase;\n      font-size: 10px; }\n    :host .user-playlists-filter .btn-clear {\n      color: #e74c3c; }\n      :host .user-playlists-filter .btn-clear:disabled {\n        color: grey; }\n    :host .user-playlists-filter .playlist-header {\n      cursor: pointer; }\n    :host .user-playlists-filter .playlist-filter {\n      position: absolute;\n      right: 0;\n      top: 0.6rem;\n      background: var(--filter-darker-bg);\n      padding-left: 5px; }\n      :host .user-playlists-filter .playlist-filter input {\n        background: var(--filter-darker-bg);\n        border: none;\n        padding: 0 3px;\n        margin-bottom: 0; } }\n\n@media (min-width: 768px) {\n  :host .text.btn-transparent {\n    cursor: pointer; }\n  .closed :host .user-playlists-filter .playlist-filter {\n    -webkit-transform: translateX(-100rem);\n            transform: translateX(-100rem); }\n  .closed :host .text,\n  .closed :host .btn-save,\n  .closed :host .playlist-count,\n  .closed :host .btn-clear {\n    display: none; }\n  .closed :host .playlist-header {\n    font-size: 2rem; }\n  .closed :host .nav-header.user-playlists-filter {\n    padding: 0 1rem;\n    text-align: center; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__now_playlist_component__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__now_playlist_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__now_playlist_track_component__ = __webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist-track.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_track_component__["a"]; });




/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist-track.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NowPlaylistTrackComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_services_media_parser_service__ = __webpack_require__("./resources/assets/src/app/core/services/media-parser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NowPlaylistTrackComponent = (function () {
    function NowPlaylistTrackComponent(mediaParser) {
        this.mediaParser = mediaParser;
        this.remove = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.select = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.selectTrack = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.displayTracks = false;
        this.displayInfo = false;
        this.tracks = [];
        this.hasTracks = false;
        this.parsedTracks = false;
    }
    NowPlaylistTrackComponent.prototype.ngAfterContentInit = function () {
        this.extractTracks(this.video);
    };
    NowPlaylistTrackComponent.prototype.extractTracks = function (media) {
        if (!this.parsedTracks) {
            var tracks = this.mediaParser.extractTracks(media);
            if (Array.isArray(tracks)) {
                this.parsedTracks = true;
                this.tracks = tracks;
                this.hasTracks = true;
            }
        }
    };
    NowPlaylistTrackComponent.prototype.isPlaylistMedia = function (media) {
        return this.hasTracks;
    };
    NowPlaylistTrackComponent.prototype.toggleTracks = function (media) {
        this.displayTracks = !this.displayTracks;
        return this.displayTracks;
    };
    NowPlaylistTrackComponent.prototype.handleToggleTracks = function (event, media) {
        event.stopImmediatePropagation();
        this.toggleTracks(media);
    };
    NowPlaylistTrackComponent.prototype.handleSelectTrack = function ($event, track, media) {
        $event.stopImmediatePropagation();
        var time = this.mediaParser.extractTime(track);
        if (time) {
            this.selectTrack.emit({ time: time[0], media: media });
        }
    };
    NowPlaylistTrackComponent.prototype.markSelected = function (video) {
        this.select.emit(video);
    };
    NowPlaylistTrackComponent.prototype.toggleInfo = function () {
        this.displayInfo = !this.displayInfo;
        return this.displayInfo;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], NowPlaylistTrackComponent.prototype, "video", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], NowPlaylistTrackComponent.prototype, "index", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistTrackComponent.prototype, "remove", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistTrackComponent.prototype, "select", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistTrackComponent.prototype, "selectTrack", void 0);
    NowPlaylistTrackComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'now-playlist-track',
            styles: [__webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist-track.scss")],
            template: "\n  <div class=\"now-playlist-track__trigger\">\n    <div class=\"track-contents\">\n      <section class=\"video-thumb playlist-track__thumb\"\n        (click)=\"markSelected(video)\">\n        <span class=\"track-number\">{{ index + 1 }}</span>\n        <img draggable=\"false\" class=\"video-thumb__image\"\n        src=\"{{ video | videoToThumb }}\"\n        xtitle=\"Drag to sort\">\n        <span class=\"badge badge-info\">\n          {{ video.contentDetails.duration | toFriendlyDuration }}\n        </span>\n      </section>\n\n      <section class=\"video-title\" (click)=\"markSelected(video)\" [tooltip]=\"video.snippet.title\">{{ video.snippet.title }}</section>\n      </div>\n    <aside class=\"playlist-track__content\">\n      <section class=\"track-actions\">\n        <button class=\"btn label btn-primary playlist-track\"\n          *ngIf=\"isPlaylistMedia(video)\"\n          (click)=\"handleToggleTracks($event, video)\"\n          tooltip=\"Album Track - click to select cued tracks\">\n          <icon name=\"list-ul\"></icon>\n        </button>\n        <button class=\"btn label btn-info playlist-track\"\n          (click)=\"toggleInfo()\"\n          tooltip=\"More information for this media\">\n          <icon name=\"info-circle\"></icon>\n        </button>\n      </section>\n      <span class=\"label label-danger ux-maker remove-track\" tooltip=\"Remove From Playlist\"\n        (click)=\"remove.emit(video)\">\n        <icon name=\"trash\"></icon>\n      </span>\n    </aside>\n    <article *ngIf=\"displayTracks\" class=\"track-tracks list-group\">\n      <aside class=\"album-tracks-heading\">Tracks</aside>\n      <button type=\"button\" class=\"list-group-item btn-transparent\"\n        *ngFor=\"let track of tracks | parseTracks\"\n        (click)=\"handleSelectTrack($event, track, video)\">\n        {{ track }}\n      </button>\n    </article>\n    <article *ngIf=\"displayInfo\" class=\"track-info\">\n      {{ video.snippet.description }}\n    </article>\n  </div>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__core_services_media_parser_service__["a" /* MediaParserService */]])
    ], NowPlaylistTrackComponent);
    return NowPlaylistTrackComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist-track.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host {\n    --track-badge-color: var(--brand-dark-bg-color);\n    --track-active-border-color: var(--brand-primary);\n    --track-bg-active-color: var(--brand-dark-bg-color-transparent);\n    --text-color: var(--brand-inverse-text);\n    --track-active-border-color: var(--brand-primary);\n    --track-bg-active-color: var(--brand-dark-bg-color-transparent); }\n    :host .now-playlist-track__trigger {\n      color: #c9c7c7;\n      text-shadow: none;\n      padding: 1.5rem;\n      border-bottom: 1px solid var(--sidebar-bg-secondary); }\n      :host .now-playlist-track__trigger .track-contents {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: horizontal;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: row;\n                flex-direction: row; }\n      .active :host .now-playlist-track__trigger {\n        color: var(--text-color);\n        -webkit-box-shadow: inset 0px 0px 6px var(--track-bg-active-color);\n                box-shadow: inset 0px 0px 6px var(--track-bg-active-color);\n        border-right: 6px solid var(--track-active-border-color);\n        background: var(--track-bg-active-color); }\n    :host .track-number {\n      position: absolute;\n      left: 1.5rem;\n      padding: 0.5rem;\n      background-color: rgba(0, 0, 0, 0.9);\n      font-size: 1rem;\n      font-weight: bold; }\n    :host .track-actions {\n      position: absolute;\n      top: 5rem;\n      right: 1rem; }\n    :host .track-info {\n      max-height: 400px;\n      overflow: auto; }\n    :host .playlist-track {\n      font-size: 1.3rem;\n      display: inline-block; }\n    :host .video-title {\n      font-size: small;\n      overflow: hidden;\n      max-height: 6rem;\n      min-height: 6.5rem; }\n    :host .video-thumb {\n      height: 4.8rem;\n      width: 6.4rem;\n      margin-right: 8px; }\n      :host .video-thumb .video-thumb__image {\n        height: 100%; }\n    :host .remove-track {\n      position: absolute;\n      top: 1rem;\n      right: 0rem;\n      z-index: 15;\n      font-size: 1.5rem;\n      -webkit-transform: translatex(-1rem);\n              transform: translatex(-1rem); }\n    :host .badge-info {\n      border-radius: 0;\n      background: var(--track-badge-color);\n      margin: 0 auto;\n      display: block;\n      font-size: 1rem;\n      margin-top: 0;\n      z-index: 10;\n      position: relative; }\n    :host .track-tracks {\n      margin: 1.5rem -1.5rem 0;\n      border-top: 1px solid var(--track-active-border-color); }\n      :host .track-tracks .album-tracks-heading {\n        margin: 0 2rem;\n        color: var(--track-active-border-color);\n        font-size: 1.5rem; }\n      :host .track-tracks .list-group-item {\n        border: none;\n        color: var(--text-color); }\n        :host .track-tracks .list-group-item:hover {\n          color: var(--track-active-border-color); } }\n\n@media (min-width: 768px) {\n  :host .remove-track {\n    -webkit-transform: translatex(5rem);\n            transform: translatex(5rem); }\n  :host:hover .remove-track {\n    -webkit-transform: translatex(-1rem);\n            transform: translatex(-1rem); }\n  .closed :host .track-actions {\n    -webkit-transform: translateX(20px);\n            transform: translateX(20px); }\n  .closed :host .track-info {\n    display: none; }\n  :host .track-tracks .btn {\n    font-size: 1.5rem; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NowPlaylistComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_animations_fade_in_animation__ = __webpack_require__("./resources/assets/src/app/shared/animations/fade-in.animation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NowPlaylistComponent = (function () {
    function NowPlaylistComponent(zone) {
        this.zone = zone;
        this.select = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.selectTrack = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // @Output() sort = new EventEmitter<GoogleApiYouTubeSearchResource>();
        this.remove = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.hasActiveChanged = false;
    }
    NowPlaylistComponent.prototype.ngAfterViewChecked = function () {
        var _this = this;
        if (this.hasActiveChanged && this.activeTrackElement) {
            this.zone.runOutsideAngular(function () { return _this.scrollToActiveTrack(); });
        }
    };
    NowPlaylistComponent.prototype.ngOnChanges = function (_a) {
        var activeId = _a.activeId;
        if (activeId) {
            this.hasActiveChanged = Object(__WEBPACK_IMPORTED_MODULE_3__shared_utils_data_utils__["a" /* isNewChange */])(activeId);
        }
    };
    NowPlaylistComponent.prototype.scrollToActiveTrack = function () {
        if (this.activeTrackElement) {
            this.activeTrackElement.scrollIntoView();
        }
    };
    NowPlaylistComponent.prototype.selectVideo = function (media) {
        this.select.emit(media);
    };
    NowPlaylistComponent.prototype.removeVideo = function (media) {
        this.remove.emit(media);
    };
    NowPlaylistComponent.prototype.sortVideo = function (media) {
        // this.sort.next(media);
    };
    NowPlaylistComponent.prototype.isActiveMedia = function (mediaId, trackElement) {
        var isActive = this.playlist.selectedId === mediaId;
        if (isActive) {
            this.activeTrackElement = trackElement;
        }
        return isActive;
    };
    NowPlaylistComponent.prototype.selectTrackInVideo = function (trackEvent) {
        this.selectTrack.emit(trackEvent);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], NowPlaylistComponent.prototype, "playlist", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistComponent.prototype, "select", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistComponent.prototype, "selectTrack", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NowPlaylistComponent.prototype, "remove", void 0);
    NowPlaylistComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'now-playlist',
            animations: [__WEBPACK_IMPORTED_MODULE_2__shared_animations_fade_in_animation__["c" /* flyOut */]],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            styles: [__webpack_require__("./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist.scss")],
            template: "\n  <section class=\"now-playlist ux-maker\">\n    <ul class=\"nav nav-list ux-maker nicer-ux\">\n      <li class=\"now-playlist-track\" #playlistTrack\n        [ngClass]=\"{\n          'active': isActiveMedia(video.id, playlistTrack)\n        }\"\n        *ngFor=\"let video of playlist.videos | search:playlist.filter; let index = index\"\n        [@flyOut]>\n        <now-playlist-track\n          [video]=\"video\" [index]=\"index\"\n          (remove)=\"removeVideo($event)\"\n          (select)=\"selectVideo(video)\"\n          (selectTrack)=\"selectTrackInVideo($event)\"\n        ></now-playlist-track>\n      </li>\n    </ul>\n  </section>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], NowPlaylistComponent);
    return NowPlaylistComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/components/now-playing/now-playlist/now-playlist.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  --track-active-border-color: var(--brand-primary);\n  --track-bg-active-color: var(--brand-dark-bg-color-transparent);\n  .now-playlist {\n    position: relative; }\n    .now-playlist .nav-list {\n      overflow-x: hidden; }\n      .now-playlist .nav-list > :not(.active) a:hover {\n        background: rgba(10, 10, 10, 0.2); }\n    .now-playlist .now-playlist-track {\n      cursor: pointer;\n      opacity: 1; }\n      .now-playlist .now-playlist-track a .playlist-track__content {\n        height: 6rem; }\n  .ng-enter + .now-playlist.slide-down {\n    -webkit-transform: translatey(10px);\n            transform: translatey(10px); } }\n\n@media (min-width: 768px) {\n  .now-playlist .now-playlist-track a .playlist-track__content {\n    overflow: hidden; }\n  .closed now-playlist .now-playlist-track a,\n  .closed now-playlist .now-playlist-track a.active {\n    padding: 0;\n    min-height: 6rem;\n    margin: 0.7rem 0; }\n  .closed now-playlist .now-playlist-track .now-playlist-track__trigger {\n    padding: 0; }\n  .closed now-playlist .now-playlist-track .playlist-track__content,\n  .closed now-playlist .now-playlist-track .video-title {\n    height: 0; }\n  .closed now-playlist .now-playlist-track .track-number {\n    left: 0; }\n  .closed now-playlist .now-playlist-track .video-thumb {\n    height: 5rem;\n    width: 7rem; }\n  .closed now-playlist .now-playlist-track .track-tracks {\n    display: none; }\n  .closed now-playlist .now-playlist-track .playlist-track {\n    -webkit-transform: translateX(5rem);\n            transform: translateX(5rem); } }\n"

/***/ }),

/***/ "./resources/assets/src/app/core/effects/analytics.effects.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnalyticsEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_user_profile__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__store_app_player__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_analytics_service__ = __webpack_require__("./resources/assets/src/app/core/services/analytics.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AnalyticsEffects = (function () {
    function AnalyticsEffects(actions$, store, userProfileActions, analytics) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.userProfileActions = userProfileActions;
        this.analytics = analytics;
        this.trackToken$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_3__store_user_profile__["b" /* UserProfileActions */].USER_PROFILE_RECIEVED)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["l" /* tap */])(function () { return _this.analytics.trackSignin(); }));
        this.trackSearch$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_4__store_player_search__["c" /* PlayerSearchActions */].SEARCH_NEW_QUERY, __WEBPACK_IMPORTED_MODULE_4__store_player_search__["c" /* PlayerSearchActions */].SEARCH_MORE_FOR_QUERY)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["n" /* withLatestFrom */])(this.store.select(__WEBPACK_IMPORTED_MODULE_4__store_player_search__["l" /* getSearchType */])), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["l" /* tap */])(function (states) { return _this.analytics.trackSearch(states[1].presets); }));
        this.trackPlay$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_5__store_app_player__["a" /* ActionTypes */].PLAY_STARTED)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["l" /* tap */])(function () { return _this.analytics.trackVideoPlay(); }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AnalyticsEffects.prototype, "trackToken$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AnalyticsEffects.prototype, "trackSearch$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AnalyticsEffects.prototype, "trackPlay$", void 0);
    AnalyticsEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__store_user_profile__["b" /* UserProfileActions */],
            __WEBPACK_IMPORTED_MODULE_6__core_services_analytics_service__["a" /* AnalyticsService */]])
    ], AnalyticsEffects);
    return AnalyticsEffects;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/effects/app-player.effects.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppPlayerEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_observable_defer__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/defer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__store_app_player__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_services_youtube_player_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-player.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_services_youtube_videos_info_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-videos-info.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AppPlayerEffects = (function () {
    function AppPlayerEffects(actions$, store, youtubePlayerService, youtubeVideosInfo) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.youtubePlayerService = youtubePlayerService;
        this.youtubeVideosInfo = youtubeVideosInfo;
        this.init$ = Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_observable_defer__["a" /* defer */])(function () { return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])(new __WEBPACK_IMPORTED_MODULE_7__store_app_player__["i" /* ResetFullScreen */]()); });
        this.playVideo$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_7__store_app_player__["a" /* ActionTypes */].PLAY)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_3__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["j" /* switchMap */])(function (media) {
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])(_this.youtubePlayerService.playVideo(media)).pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(function (video) { return new __WEBPACK_IMPORTED_MODULE_7__store_app_player__["e" /* PlayStarted */](video); }));
        }));
        this.pauseVideo$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_7__store_app_player__["a" /* ActionTypes */].PAUSE)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["l" /* tap */])(function () { return _this.youtubePlayerService.pause(); }));
        this.loadAndPlay$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_7__store_app_player__["a" /* ActionTypes */].LOAD_AND_PLAY)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_3__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["j" /* switchMap */])(function (media) {
            return _this.youtubeVideosInfo
                .fetchVideoData(media.id || media.id.videoId)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(function (video) { return new __WEBPACK_IMPORTED_MODULE_7__store_app_player__["f" /* PlayVideo */](video); }));
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["a" /* catchError */])(function () { return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])({ type: 'LOAD_AND_PLAY_ERROR' }); }));
        this.toggleFullscreen$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_7__store_app_player__["a" /* ActionTypes */].FULLSCREEN)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["n" /* withLatestFrom */])(this.store.select(__WEBPACK_IMPORTED_MODULE_7__store_app_player__["p" /* getPlayerFullscreen */])), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["l" /* tap */])(function (states) {
            return _this.youtubePlayerService.setSize(states[1].height, states[1].width);
        }));
        this.setupPlayer$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_7__store_app_player__["a" /* ActionTypes */].SETUP_PLAYER)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_3__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["l" /* tap */])(function (player) { return _this.youtubePlayerService.setupPlayer(player); }));
        this.playerStateChange$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_7__store_app_player__["a" /* ActionTypes */].PLAYER_STATE_CHANGE)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_3__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["f" /* map */])(function (data) { return new __WEBPACK_IMPORTED_MODULE_7__store_app_player__["l" /* UpdateState */](data); }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], AppPlayerEffects.prototype, "init$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], AppPlayerEffects.prototype, "playVideo$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AppPlayerEffects.prototype, "pauseVideo$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], AppPlayerEffects.prototype, "loadAndPlay$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AppPlayerEffects.prototype, "toggleFullscreen$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AppPlayerEffects.prototype, "setupPlayer$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], AppPlayerEffects.prototype, "playerStateChange$", void 0);
    AppPlayerEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_8__core_services_youtube_player_service__["a" /* YoutubePlayerService */],
            __WEBPACK_IMPORTED_MODULE_9__core_services_youtube_videos_info_service__["a" /* YoutubeVideosInfo */]])
    ], AppPlayerEffects);
    return AppPlayerEffects;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/effects/app-settings.effects.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingsEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_layout__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_version_checker_service__ = __webpack_require__("./resources/assets/src/app/core/services/version-checker.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppSettingsEffects = (function () {
    function AppSettingsEffects(actions$, store, versionCheckerService) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.versionCheckerService = versionCheckerService;
        this.updateAppVersion$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_4__store_app_layout__["a" /* ActionTypes */].APP_UPDATE_VERSION)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function () { return _this.versionCheckerService.updateVersion(); }));
        this.checkForNewAppVersion$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_4__store_app_layout__["a" /* ActionTypes */].APP_CHECK_VERSION)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function () { return _this.versionCheckerService.checkForVersion(); }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AppSettingsEffects.prototype, "updateAppVersion$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], AppSettingsEffects.prototype, "checkForNewAppVersion$", void 0);
    AppSettingsEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_0__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_5__core_services_version_checker_service__["a" /* VersionCheckerService */]])
    ], AppSettingsEffects);
    return AppSettingsEffects;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/effects/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppEffectsModules; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_player_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/app-player.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__analytics_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/analytics.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__now_playlist_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/now-playlist.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_profile_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/user-profile.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__player_search_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/player-search.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_settings_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/app-settings.effects.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__router_effects__ = __webpack_require__("./resources/assets/src/app/core/effects/router.effects.ts");








var AppEffectsModules = __WEBPACK_IMPORTED_MODULE_0__ngrx_effects__["c" /* EffectsModule */].forRoot([
    __WEBPACK_IMPORTED_MODULE_1__app_player_effects__["a" /* AppPlayerEffects */],
    __WEBPACK_IMPORTED_MODULE_3__now_playlist_effects__["a" /* NowPlaylistEffects */],
    __WEBPACK_IMPORTED_MODULE_4__user_profile_effects__["a" /* UserProfileEffects */],
    __WEBPACK_IMPORTED_MODULE_5__player_search_effects__["a" /* PlayerSearchEffects */],
    __WEBPACK_IMPORTED_MODULE_6__app_settings_effects__["a" /* AppSettingsEffects */],
    __WEBPACK_IMPORTED_MODULE_7__router_effects__["a" /* RouterEffects */],
    __WEBPACK_IMPORTED_MODULE_2__analytics_effects__["a" /* AnalyticsEffects */]
]);


/***/ }),

/***/ "./resources/assets/src/app/core/effects/now-playlist.effects.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NowPlaylistEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_switchMap__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_mergeMap__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/mergeMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_zip__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/zip.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_withLatestFrom__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/withLatestFrom.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_services_user_profile_service__ = __webpack_require__("./resources/assets/src/app/core/services/user-profile.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var NowPlaylistEffects = (function () {
    function NowPlaylistEffects(actions$, store, mediaParser, playerService, userProfile) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.mediaParser = mediaParser;
        this.playerService = playerService;
        this.userProfile = userProfile;
        this.queueVideo$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].SELECT)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function (media) {
            return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["g" /* QueueVideo */](media);
        }));
        this.playerStateChange$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].PLAYER_STATE_CHANGE)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["e" /* filter */])(function (data) { return data === YT.PlayerState.ENDED; }), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function () { return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["e" /* MediaEnded */](); }));
        /* if it's the last track
         * AND repeat is on
         * THEN play the first track
        **/
        this.loadNextTrack$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].MEDIA_ENDED)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["n" /* withLatestFrom */])(this.store.select(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["s" /* getSelectedMedia */])), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["e" /* filter */])(function (states) {
            return states[1] && states[1].hasOwnProperty('id');
        }), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function (states) {
            return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["n" /* SelectVideo */](states[1]);
        }));
        this.selectBeforeSeekToTime$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].SELECT_AND_SEEK_TO_TIME)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function (trackEvent) {
            return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["p" /* UpdateIndexByMedia */](trackEvent.media.id);
        }));
        this.seekToTime$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].SELECT_AND_SEEK_TO_TIME)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["l" /* tap */])(function (trackEvent) {
            return _this.playerService.seekTo(_this.mediaParser.toNumber(trackEvent.time));
        }));
        this.loadPlaylist$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].LOAD_PLAYLIST_START)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["j" /* switchMap */])(function (id) { return _this.userProfile.fetchAllPlaylistItems(id); }), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function (playlistItems) {
            return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["d" /* LoadPlaylistEndAction */](playlistItems);
        }));
        this.addPlaylistItems$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].LOAD_PLAYLIST_END)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function (playlistItems) {
            return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["h" /* QueueVideos */](playlistItems);
        }));
        this.playPlaylistFirstTrack$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].LOAD_PLAYLIST_END)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function (playlistItems) {
            return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["n" /* SelectVideo */](playlistItems[0]);
        }));
        this.playPlaylist$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["a" /* ActionTypes */].PLAY_PLAYLIST)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_10__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_11_rxjs_operators__["f" /* map */])(function (id) { return new __WEBPACK_IMPORTED_MODULE_8__store_now_playlist__["c" /* LoadPlaylistAction */](id); }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "queueVideo$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "playerStateChange$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "loadNextTrack$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "selectBeforeSeekToTime$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "seekToTime$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "loadPlaylist$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "addPlaylistItems$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "playPlaylistFirstTrack$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], NowPlaylistEffects.prototype, "playPlaylist$", void 0);
    NowPlaylistEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_0__core_services__["c" /* MediaParserService */],
            __WEBPACK_IMPORTED_MODULE_0__core_services__["f" /* YoutubePlayerService */],
            __WEBPACK_IMPORTED_MODULE_9__core_services_user_profile_service__["a" /* UserProfile */]])
    ], NowPlaylistEffects);
    return NowPlaylistEffects;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/effects/player-search.effects.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerSearchEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__store_player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_services_youtube_search__ = __webpack_require__("./resources/assets/src/app/core/services/youtube.search.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var PlayerSearchEffects = (function () {
    function PlayerSearchEffects(actions$, store, playerSearchActions, youtubeSearch, youtubeVideosInfo) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.playerSearchActions = playerSearchActions;
        this.youtubeSearch = youtubeSearch;
        this.youtubeVideosInfo = youtubeVideosInfo;
        this.searchQuery$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].SEARCH_NEW_QUERY)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["n" /* withLatestFrom */])(this.store), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (latest) { return latest[1]; }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["j" /* switchMap */])(function (store) {
            return _this.youtubeSearch
                .resetPageToken()
                .searchFor(store.search.searchType, store.search.query, store.search.queryParams)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (youtubeResponse) {
                return _this.playerSearchActions.searchResultsReturned(youtubeResponse);
            }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(function (err) { return Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["a" /* of */])(_this.playerSearchActions.errorInSearch(err)); }));
        }));
        this.resetVideos$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].SEARCH_NEW_QUERY, __WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].PLAYLISTS_SEARCH_START.action)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function () { return _this.playerSearchActions.resetResults(); }));
        this.searchResultsReturned$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].SEARCH_RESULTS_RETURNED)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["n" /* withLatestFrom */])(this.store.select(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["l" /* getSearchType */])), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (states) {
            if (states[1] === __WEBPACK_IMPORTED_MODULE_6__store_player_search__["b" /* CSearchTypes */].VIDEO) {
                return __WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].ADD_METADATA_TO_VIDEOS.creator(states[0]);
            }
            return __WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].ADD_PLAYLISTS_TO_RESULTS.creator(states[0]);
        }));
        this.addPlaylistsToResults$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].ADD_PLAYLISTS_TO_RESULTS.action)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (result) { return __WEBPACK_IMPORTED_MODULE_6__store_player_search__["a" /* AddResultsAction */].creator(result.items); }));
        this.addMetadataToVideos$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].ADD_METADATA_TO_VIDEOS.action)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (medias) {
            return medias.items.map(function (media) { return media.id.videoId; }).join(',');
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["g" /* mergeMap */])(function (mediaIds) {
            return _this.youtubeVideosInfo
                .fetchVideosData(mediaIds)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (videos) {
                return __WEBPACK_IMPORTED_MODULE_6__store_player_search__["a" /* AddResultsAction */].creator(videos);
            }));
        }));
        this.searchMoreForQuery$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].SEARCH_MORE_FOR_QUERY)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["n" /* withLatestFrom */])(this.store), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (latest) { return latest[1]; }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["e" /* filter */])(function (store) { return !store.search.isSearching; }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["g" /* mergeMap */])(function (store) {
            _this.youtubeSearch.searchMore(store.search.pageToken.next);
            return _this.youtubeSearch
                .searchFor(store.search.searchType, store.search.query, store.search.queryParams)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (youtubeResponse) {
                return _this.playerSearchActions.searchResultsReturned(youtubeResponse);
            }));
        }));
        this.searchMoreSearchStarted$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].SEARCH_MORE_FOR_QUERY)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["n" /* withLatestFrom */])(this.store.select(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["g" /* getIsSearching */])), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["e" /* filter */])(function (states) { return !states[1]; }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function () { return _this.playerSearchActions.searchStarted(); }));
        this.updatePreset$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].UPDATE_QUERY_PARAM)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function () { return new __WEBPACK_IMPORTED_MODULE_6__store_player_search__["d" /* SearchCurrentQuery */](); }));
        this.resetVideosAfterParamUpdate$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].UPDATE_QUERY_PARAM)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function () { return _this.playerSearchActions.resetResults(); }));
        this.resetPageToken$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].RESET_PAGE_TOKEN)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["g" /* mergeMap */])(function () { return Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["a" /* of */])(_this.youtubeSearch.resetPageToken()); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function () { return ({ type: 'PAGE_RESET_DONE' }); }));
        this.searchCurrentQuery$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].SEARCH_CURRENT_QUERY)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_7__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["n" /* withLatestFrom */])(this.store.select(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["j" /* getQuery */])), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (latest) { return latest[1]; }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (query) { return _this.playerSearchActions.searchNewQuery(query); }));
        // Playlists SEARCH EFFECTS
        this.playlistsSearchStart$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */].PLAYLISTS_SEARCH_START.action)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["n" /* withLatestFrom */])(this.store), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (latest) { return latest[1]; }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["j" /* switchMap */])(function (store) {
            return _this.youtubeSearch
                .searchForPlaylist(store.search.query, store.search.queryParams)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (youtubeResponse) {
                return __WEBPACK_IMPORTED_MODULE_6__store_player_search__["a" /* AddResultsAction */].creator(youtubeResponse.items);
            }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(function (err) { return Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["a" /* of */])(_this.playerSearchActions.errorInSearch(err)); }));
        }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "searchQuery$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "resetVideos$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "searchResultsReturned$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "addPlaylistsToResults$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "addMetadataToVideos$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "searchMoreForQuery$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "searchMoreSearchStarted$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "updatePreset$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "resetVideosAfterParamUpdate$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "resetPageToken$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "searchCurrentQuery$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], PlayerSearchEffects.prototype, "playlistsSearchStart$", void 0);
    PlayerSearchEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_6__store_player_search__["c" /* PlayerSearchActions */], __WEBPACK_IMPORTED_MODULE_8__core_services_youtube_search__["a" /* YoutubeSearch */],
            __WEBPACK_IMPORTED_MODULE_0__core_services__["g" /* YoutubeVideosInfo */]])
    ], PlayerSearchEffects);
    return PlayerSearchEffects;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/effects/router.effects.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RouterEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_router_store__ = __webpack_require__("./resources/assets/src/app/core/store/router-store/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RouterEffects = (function () {
    function RouterEffects(actions$, router, location) {
        var _this = this;
        this.actions$ = actions$;
        this.router = router;
        this.location = location;
        this.navigate$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_4__store_router_store__["d" /* GO */])
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["f" /* map */])(function (action) { return action.payload; }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["l" /* tap */])(function (_a) {
            var path = _a.path, queryParams = _a.query, extras = _a.extras;
            return _this.router.navigate(path, __assign({ queryParams: queryParams }, extras));
        }));
        this.navigateBack$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_4__store_router_store__["a" /* BACK */])
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["l" /* tap */])(function () { return _this.location.back(); }));
        this.navigateForward$ = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_4__store_router_store__["c" /* FORWARD */])
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators__["l" /* tap */])(function () { return _this.location.forward(); }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], RouterEffects.prototype, "navigate$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], RouterEffects.prototype, "navigateBack$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], RouterEffects.prototype, "navigateForward$", void 0);
    RouterEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["Location"]])
    ], RouterEffects);
    return RouterEffects;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/effects/user-profile.effects.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProfileEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_effects__ = __webpack_require__("./node_modules/@ngrx/effects/@ngrx/effects.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_user_profile__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_user_profile_service__ = __webpack_require__("./resources/assets/src/app/core/services/user-profile.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_authorization_service__ = __webpack_require__("./resources/assets/src/app/core/services/authorization.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UserProfileEffects = (function () {
    function UserProfileEffects(actions$, userProfileActions, userProfile, auth) {
        var _this = this;
        this.actions$ = actions$;
        this.userProfileActions = userProfileActions;
        this.userProfile = userProfile;
        this.auth = auth;
        this.checkUserAuth$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].CHECK_USER_AUTH), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["j" /* switchMap */])(function () { return _this.auth.loadAuth(); }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (googleUser) {
            return new __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["e" /* UserSigninSuccess */](googleUser);
        }));
        this.updateToken$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].UPDATE_TOKEN), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (token) { return (_this.auth.accessToken = token); }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["j" /* switchMap */])(function (token) {
            return _this.userProfile.getPlaylists(true).pipe(Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["a" /* catchError */])(function (error) {
                console.log("error in fetching user's playlists " + error);
                return Object(__WEBPACK_IMPORTED_MODULE_0_rxjs_observable_of__["a" /* of */])(error);
            }));
        }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (response) { return _this.userProfileActions.updateData(response); }));
        this.addUserPlaylists$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].UPDATE), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (data) { return _this.userProfileActions.addPlaylists(data.items); }));
        this.updateNextPageToken$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].UPDATE), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (_a) {
            var nextPageToken = _a.nextPageToken;
            return nextPageToken
                ? _this.userProfileActions.updatePageToken(nextPageToken)
                : _this.userProfileActions.userProfileCompleted();
        }));
        this.getMorePlaylists$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].UPDATE_NEXT_PAGE_TOKEN), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["l" /* tap */])(function (pageToken) { return _this.userProfile.updatePageToken(pageToken); }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["j" /* switchMap */])(function (pageToken) { return _this.userProfile.getPlaylists(false); }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (response) { return _this.userProfileActions.updateData(response); }));
        this.userProfileRecieved$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_PROFILE_RECIEVED), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (profile) { return _this.userProfile.toUserJson(profile); }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (profile) {
            return _this.userProfileActions.updateUserProfile(profile);
        }));
        // SIGN IN/OUT EFFECTS
        this.userSignin$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_SIGNIN), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function () { return new __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["d" /* UserSigninStart */](); }));
        this.userSigninStart$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_SIGNIN_START), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["j" /* switchMap */])(function () {
            return _this.auth
                .signIn()
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["a" /* catchError */])(function (error) { return _this.auth.handleFailedLogin(error); }));
        }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (response) { return new __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["e" /* UserSigninSuccess */](response); }));
        this.userSigninSuccess$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_SIGNIN_SUCCESS), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["l" /* tap */])(function (response) { return _this.auth.setAuthTimer(response); }));
        this.updateTokenAfterSigninSuccess$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_SIGNIN_SUCCESS), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (googleUser) {
            return _this.userProfileActions.updateToken(_this.auth.extractToken(googleUser));
        }));
        this.updateProfileAfterSigninSuccess$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_SIGNIN_SUCCESS), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(__WEBPACK_IMPORTED_MODULE_5__shared_utils_data_utils__["b" /* toPayload */]), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function (googleUser) {
            return _this.userProfileActions.userProfileRecieved(googleUser.getBasicProfile());
        }));
        this.userSignout$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_SIGNOUT), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["j" /* switchMap */])(function () { return _this.auth.signOut(); }), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["f" /* map */])(function () { return new __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["g" /* UserSignoutSuccess */](); }));
        this.userSignoutSuccess$ = this.actions$.pipe(Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["d" /* ofType */])(__WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */].USER_SIGNOUT_SUCCESS), Object(__WEBPACK_IMPORTED_MODULE_1_rxjs_operators__["l" /* tap */])(function () { return _this.auth.disposeAutoSignIn(); }));
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "checkUserAuth$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "updateToken$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "addUserPlaylists$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "updateNextPageToken$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "getMorePlaylists$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "userProfileRecieved$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "userSignin$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "userSigninStart$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "userSigninSuccess$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "updateTokenAfterSigninSuccess$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "updateProfileAfterSigninSuccess$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "userSignout$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
        __metadata("design:type", Object)
    ], UserProfileEffects.prototype, "userSignoutSuccess$", void 0);
    UserProfileEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_4__store_user_profile__["b" /* UserProfileActions */],
            __WEBPACK_IMPORTED_MODULE_6__core_services_user_profile_service__["a" /* UserProfile */],
            __WEBPACK_IMPORTED_MODULE_7__core_services_authorization_service__["a" /* Authorization */]])
    ], UserProfileEffects);
    return UserProfileEffects;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__store__ = __webpack_require__("./resources/assets/src/app/core/store/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__effects__ = __webpack_require__("./resources/assets/src/app/core/effects/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__resolvers__ = __webpack_require__("./resources/assets/src/app/core/resolvers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__api__ = __webpack_require__("./resources/assets/src/app/core/api/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CoreModule = (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__store__["a" /* CoreStoreModule */], __WEBPACK_IMPORTED_MODULE_2__effects__["a" /* AppEffectsModules */]],
            declarations: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__store__["a" /* CoreStoreModule */]],
            providers: __WEBPACK_IMPORTED_MODULE_3__services__["a" /* APP_SERVICES */].concat(__WEBPACK_IMPORTED_MODULE_4__resolvers__["a" /* APP_RESOLVERS */], __WEBPACK_IMPORTED_MODULE_5__api__["a" /* APP_APIS */])
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/resolvers/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APP_RESOLVERS; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__playlist_videos_resolver__ = __webpack_require__("./resources/assets/src/app/core/resolvers/playlist-videos.resolver.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__playlist_resolver__ = __webpack_require__("./resources/assets/src/app/core/resolvers/playlist.resolver.ts");


var APP_RESOLVERS = [
    __WEBPACK_IMPORTED_MODULE_0__playlist_videos_resolver__["a" /* PlaylistVideosResolver */],
    __WEBPACK_IMPORTED_MODULE_1__playlist_resolver__["a" /* PlaylistResolver */],
];


/***/ }),

/***/ "./resources/assets/src/app/core/resolvers/playlist-videos.resolver.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistVideosResolver; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlaylistVideosResolver = (function () {
    function PlaylistVideosResolver(userProfile) {
        this.userProfile = userProfile;
    }
    PlaylistVideosResolver.prototype.resolve = function (route) {
        var playlistId = route.params['id'];
        return this.userProfile.fetchAllPlaylistItems(playlistId);
    };
    PlaylistVideosResolver = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__core_services__["e" /* UserProfile */]])
    ], PlaylistVideosResolver);
    return PlaylistVideosResolver;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/resolvers/playlist.resolver.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistResolver; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__core_services__ = __webpack_require__("./resources/assets/src/app/core/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlaylistResolver = (function () {
    function PlaylistResolver(userProfile) {
        this.userProfile = userProfile;
    }
    PlaylistResolver.prototype.resolve = function (route) {
        var playlistId = route.params['id'];
        return this.userProfile
            .fetchPlaylist(playlistId)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["f" /* map */])(function (response) { return response.items; }));
        // .pipe(map(response => response.items[0]));
    };
    PlaylistResolver = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__core_services__["e" /* UserProfile */]])
    ], PlaylistResolver);
    return PlaylistResolver;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/analytics.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnalyticsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// https://developers.google.com/analytics/devguides/collection/gtagjs/events#default-event-categories-and-labels
// Google Analytics Events
var Events = {
    Login: {
        NAME: 'login',
        LABEL: 'method'
    },
    Search: {
        NAME: 'search',
        LABEL: 'search_term'
    }
};
var CustomEvents = {
    VIDEO_PLAY: 'video_play'
};
var AnalyticsService = (function () {
    function AnalyticsService() {
        this.projectId = window['GA_PROJECT_ID'];
        this.assignGtag();
    }
    AnalyticsService.prototype.assignGtag = function () {
        var hasGtagLoaded = 'gtag' in window;
        if (!hasGtagLoaded) {
            console.info('GTAG has not been loaded');
        }
        this.gtag = hasGtagLoaded ? gtag : function () { return undefined; };
    };
    AnalyticsService.prototype.trackPage = function (page) {
        this.gtag('config', this.projectId, {
            page_title: page,
            page_location: location.origin,
            page_path: location.hash
        });
    };
    AnalyticsService.prototype.trackSearch = function (searchType) {
        this.gtag('event', Events.Search.NAME, (_a = {},
            _a[Events.Search.LABEL] = searchType,
            _a));
        var _a;
    };
    AnalyticsService.prototype.trackSignin = function () {
        this.gtag('event', Events.Login.NAME, (_a = {}, _a[Events.Login.LABEL] = 'Google', _a));
        var _a;
    };
    AnalyticsService.prototype.trackVideoPlay = function () {
        this.gtag('event', CustomEvents.VIDEO_PLAY);
    };
    AnalyticsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AnalyticsService);
    return AnalyticsService;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/authorization.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Authorization; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_observable_fromPromise__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/fromPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_environments_environment__ = __webpack_require__("./resources/assets/src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__gapi_loader_service__ = __webpack_require__("./resources/assets/src/app/core/services/gapi-loader.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var extractAccessToken = function (_googleAuth) {
    return (_googleAuth && _googleAuth.currentUser.get().getAuthResponse().access_token);
};
var Authorization = (function () {
    function Authorization(zone, gapiLoader) {
        this.zone = zone;
        this.gapiLoader = gapiLoader;
        this._scope = 'profile email https://www.googleapis.com/auth/youtube';
    }
    Object.defineProperty(Authorization.prototype, "accessToken", {
        get: function () {
            var token = {
                fromGoogle: extractAccessToken(this._googleAuth),
                fromApp: this._accessToken,
                equal: true
            };
            return token.fromGoogle;
        },
        set: function (value) {
            this._accessToken = value;
        },
        enumerable: true,
        configurable: true
    });
    Authorization.prototype.loadAuth = function () {
        var _this = this;
        // attempt to SILENT authorize
        return this.gapiLoader.load('auth2').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["j" /* switchMap */])(function () { return _this.authorize(); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["l" /* tap */])(function (googleAuth) {
            _this.saveGoogleAuth(googleAuth);
            _this.listenToGoogleAuthSignIn(googleAuth);
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["e" /* filter */])(function (googleAuth) {
            return _this.isSignIn() && _this.hasAccessToken(googleAuth);
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (googleAuth) { return googleAuth.currentUser.get(); }));
    };
    Authorization.prototype.authorize = function () {
        if (this._googleAuth) {
            console.log('signedIn?', this._googleAuth.isSignedIn.get());
        }
        var authOptions = {
            client_id: __WEBPACK_IMPORTED_MODULE_4_environments_environment__["a" /* environment */].youtube.CLIENT_ID,
            scope: this._scope
        };
        return window['gapi'].auth2.init(authOptions);
    };
    Authorization.prototype.hasAccessToken = function (googleAuth) {
        return (googleAuth &&
            googleAuth.currentUser
                .get()
                .getAuthResponse()
                .hasOwnProperty('access_token'));
    };
    Authorization.prototype.saveGoogleAuth = function (googleAuth) {
        this._googleAuth = googleAuth;
        return googleAuth;
    };
    Authorization.prototype.listenToGoogleAuthSignIn = function (googleAuth) {
        window['gapi']['auth2'].getAuthInstance().isSignedIn.listen(function (authState) {
            console.log('authState changed', authState);
        });
    };
    Authorization.prototype.signIn = function () {
        var signOptions = { scope: this._scope };
        if (this._googleAuth) {
            return Object(__WEBPACK_IMPORTED_MODULE_0_rxjs_observable_fromPromise__["a" /* fromPromise */])(this._googleAuth.signIn(signOptions));
        }
        return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */](function (obs) { return obs.complete(); });
    };
    Authorization.prototype.extractToken = function (googleUser) {
        var authResponse = googleUser.getAuthResponse();
        return authResponse.access_token;
    };
    Authorization.prototype.setAuthTimer = function (googleUser) {
        var MILLISECOND = 1000;
        var expireTime = 60 * 5;
        var expireTimeInMs = expireTime * MILLISECOND;
        this.disposeAutoSignIn();
        this.autoSignInTimer = this.startTimerToNextAuth(expireTimeInMs);
    };
    Authorization.prototype.startTimerToNextAuth = function (timeInMs) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].timer(timeInMs)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["m" /* timeInterval */])(), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["j" /* switchMap */])(function () { return _this.authorize(); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["l" /* tap */])(function (googleAuth) {
            return _this.saveGoogleAuth(googleAuth);
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["f" /* map */])(function (googleAuth) {
            return googleAuth.currentUser.get();
        }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["h" /* retry */])(3), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(function (error) {
            window.location.reload();
            return error;
        }))
            .subscribe(function (googleUser) {
            _this.zone.run(function () { return _this.setAuthTimer(googleUser); });
        });
    };
    Authorization.prototype.handleFailedLogin = function (response) {
        console.error('FAILED TO LOGIN:', response);
        return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */](function (obs) {
            obs.error();
            obs.complete();
        });
    };
    Authorization.prototype.isSignIn = function () {
        return this._googleAuth && this._googleAuth.isSignedIn.get();
    };
    Authorization.prototype.signOut = function () {
        return this._googleAuth.signOut();
    };
    Authorization.prototype.disposeAutoSignIn = function () {
        if (this.autoSignInTimer) {
            this.autoSignInTimer.unsubscribe();
        }
    };
    Authorization = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_5__gapi_loader_service__["a" /* GapiLoader */]])
    ], Authorization);
    return Authorization;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/gapi-loader.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GapiLoader; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GapiLoader = (function () {
    function GapiLoader() {
    }
    GapiLoader.prototype.loadGoogleApi = function (api, api$) {
        var _this = this;
        var gapi = 'https://apis.google.com/js/api.js';
        var script = document.createElement('script');
        script.addEventListener('load', function () { return _this._loadApi(api, api$); });
        script.setAttribute('src', gapi);
        document.body.appendChild(script);
    };
    GapiLoader.prototype.load = function (api) {
        var api$ = this.createApi(api);
        this.loadGoogleApi(api, api$);
        return api$;
    };
    GapiLoader.prototype._loadApi = function (api, api$) {
        var gapi = window['gapi'];
        var gapiAuthLoaded = gapi && gapi.auth2 && gapi.auth2.getAuthInstance();
        if (gapiAuthLoaded && gapiAuthLoaded.currentUser) {
            api$.complete(gapiAuthLoaded);
        }
        else {
            gapi.load(api, function (response) { return api$.next(response); });
        }
    };
    GapiLoader.prototype.createApi = function (api) {
        var api$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["a" /* Subject */]();
        return api$;
    };
    GapiLoader = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], GapiLoader);
    return GapiLoader;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APP_SERVICES; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_profile_service__ = __webpack_require__("./resources/assets/src/app/core/services/user-profile.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__youtube_search__ = __webpack_require__("./resources/assets/src/app/core/services/youtube.search.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__youtube_player_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-player.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__now_playlist_service__ = __webpack_require__("./resources/assets/src/app/core/services/now-playlist.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__youtube_videos_info_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-videos-info.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__gapi_loader_service__ = __webpack_require__("./resources/assets/src/app/core/services/gapi-loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__authorization_service__ = __webpack_require__("./resources/assets/src/app/core/services/authorization.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__youtube_data_api__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-data-api/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__version_checker_service__ = __webpack_require__("./resources/assets/src/app/core/services/version-checker.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__media_parser_service__ = __webpack_require__("./resources/assets/src/app/core/services/media-parser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__analytics_service__ = __webpack_require__("./resources/assets/src/app/core/services/analytics.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_service__["a"]; });
/* unused harmony namespace reexport */
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_2__youtube_player_service__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_3__now_playlist_service__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_4__youtube_videos_info_service__["a"]; });
/* unused harmony namespace reexport */
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_6__authorization_service__["a"]; });
/* unused harmony namespace reexport */
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_9__media_parser_service__["a"]; });




















var APP_SERVICES = [
    __WEBPACK_IMPORTED_MODULE_0__user_profile_service__["a" /* UserProfile */],
    __WEBPACK_IMPORTED_MODULE_1__youtube_search__["a" /* YoutubeSearch */],
    __WEBPACK_IMPORTED_MODULE_2__youtube_player_service__["a" /* YoutubePlayerService */],
    __WEBPACK_IMPORTED_MODULE_3__now_playlist_service__["a" /* NowPlaylistService */],
    __WEBPACK_IMPORTED_MODULE_4__youtube_videos_info_service__["a" /* YoutubeVideosInfo */],
    __WEBPACK_IMPORTED_MODULE_5__gapi_loader_service__["a" /* GapiLoader */],
    __WEBPACK_IMPORTED_MODULE_6__authorization_service__["a" /* Authorization */],
    __WEBPACK_IMPORTED_MODULE_7__youtube_data_api__["b" /* YoutubeDataApi */],
    __WEBPACK_IMPORTED_MODULE_8__version_checker_service__["a" /* VersionCheckerService */],
    __WEBPACK_IMPORTED_MODULE_9__media_parser_service__["a" /* MediaParserService */],
    __WEBPACK_IMPORTED_MODULE_10__analytics_service__["a" /* AnalyticsService */]
];


/***/ }),

/***/ "./resources/assets/src/app/core/services/media-parser.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MediaParserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MediaParserService = (function () {
    function MediaParserService() {
        this.HH_MM_SSre = /(\d{1,2}):\d{2}:?\d{0,2}/;
        this.LINE_WITH_TRACKre = /([a-zA-Z \S\d]){0,}(\d{1,2}:\d{2}:{0,1}\d{0,2})+([a-zA-Z \S]){0,}/;
    }
    MediaParserService.prototype.extractTracks = function (media) {
        // const re = /(([0-9]{0,1}[0-9]):([0-9][0-9]){0,1}:{0,1}([0-9][0-9]){0,1}\s*)([\w\s/]*[^ 0-9:/\n\b])/;
        var LINE_WITH_TRACKre = /([a-zA-Z \S\d]){0,}(\d{1,2}:\d{2}:{0,1}\d{0,2})+([a-zA-Z \S]){0,}/;
        var hasTracksRegexp = new RegExp(LINE_WITH_TRACKre, 'gmi');
        var tracks = media.snippet.description.match(hasTracksRegexp);
        // make sure there's a first track
        if (tracks && tracks.length && !tracks[0].includes('00:0')) {
            tracks.unshift('00:00');
        }
        return tracks;
    };
    MediaParserService.prototype.extractTime = function (track) {
        var HH_MM_SSre = this.HH_MM_SSre;
        var title = track.match(HH_MM_SSre);
        return title;
    };
    MediaParserService.prototype.verifyTracksCue = function (tracks) {
        var _this = this;
        var HH_MM_SSre = this.HH_MM_SSre;
        var isCueValid = tracks
            .map(function (track) { return _this.extractTime(track); })
            .every(function (track, index, arr) {
            var prev = index > 0 ? _this.toNumber(arr[index - 1][0]) : false;
            var current = _this.toNumber(track[0]);
            return prev ? current > prev : true;
        });
        return isCueValid;
    };
    MediaParserService.prototype.parseTracks = function (tracks) {
        if (tracks === void 0) { tracks = []; }
        var _tracks = [];
        var isFormatValid = this.verifyTracksCue(tracks);
        if (isFormatValid && tracks) {
            var re_1 = this.HH_MM_SSre;
            _tracks = tracks
                .filter(function (track) {
                var isTrack = re_1.test(track);
                return isTrack;
            });
        }
        return _tracks;
    };
    /**
     * converts time format of HH:MM:SS to seconds
     * @param time string
     */
    MediaParserService.prototype.toNumber = function (time) {
        var timeUnitRatio = {
            '3': 60 * 60,
            '2': 60,
            '1': 1
        };
        return time.split(':').reverse()
            .map(function (num) { return parseInt(num, 10); })
            .reduce(function (acc, current, index, arr) {
            return acc + (current * +timeUnitRatio[index + 1]);
        }, 0);
    };
    MediaParserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], MediaParserService);
    return MediaParserService;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/now-playlist.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NowPlaylistService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__youtube_videos_info_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-videos-info.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NowPlaylistService = (function () {
    function NowPlaylistService(store, youtubeVideosInfo) {
        this.store = store;
        this.youtubeVideosInfo = youtubeVideosInfo;
        this.playlist$ = this.store.pipe(Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["o" /* select */])(__WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["q" /* getNowPlaylist */]));
    }
    NowPlaylistService.prototype.queueVideo = function (mediaId) {
        return this.youtubeVideosInfo.api
            .list(mediaId)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["f" /* map */])(function (items) { return items[0]; }));
    };
    NowPlaylistService.prototype.queueVideos = function (medias) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["h" /* QueueVideos */](medias));
    };
    NowPlaylistService.prototype.removeVideo = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["j" /* RemoveVideo */](media));
    };
    NowPlaylistService.prototype.selectVideo = function (media) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["n" /* SelectVideo */](media));
    };
    NowPlaylistService.prototype.updateFilter = function (filter) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["b" /* FilterChange */](filter));
    };
    NowPlaylistService.prototype.clearPlaylist = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["i" /* RemoveAll */]());
    };
    NowPlaylistService.prototype.selectNextIndex = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["l" /* SelectNext */]());
    };
    NowPlaylistService.prototype.selectPreviousIndex = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["m" /* SelectPrevious */]());
    };
    NowPlaylistService.prototype.trackEnded = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["e" /* MediaEnded */]());
    };
    NowPlaylistService.prototype.getCurrent = function () {
        var media;
        this.playlist$.pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["k" /* take */])(1)).subscribe(function (playlist) {
            media = playlist.videos.find(function (video) { return video.id === playlist.selectedId; });
        });
        return media;
    };
    NowPlaylistService.prototype.updateIndexByMedia = function (mediaId) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["p" /* UpdateIndexByMedia */](mediaId));
    };
    NowPlaylistService.prototype.isInLastTrack = function () {
        var nowPlaylist;
        this.playlist$
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["k" /* take */])(1))
            .subscribe(function (_nowPlaylist) { return (nowPlaylist = _nowPlaylist); });
        var currentVideoId = nowPlaylist.selectedId;
        var indexOfCurrentVideo = nowPlaylist.videos.findIndex(function (video) { return video.id === currentVideoId; });
        var isCurrentLast = indexOfCurrentVideo + 1 === nowPlaylist.videos.length;
        return isCurrentLast;
    };
    NowPlaylistService.prototype.seekToTrack = function (trackEvent) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__store_now_playlist__["k" /* SeekTo */](trackEvent));
    };
    NowPlaylistService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__youtube_videos_info_service__["a" /* YoutubeVideosInfo */]])
    ], NowPlaylistService);
    return NowPlaylistService;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/user-profile.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProfile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__youtube_api_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__youtube_videos_info_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-videos-info.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__authorization_service__ = __webpack_require__("./resources/assets/src/app/core/services/authorization.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserProfile = (function () {
    function UserProfile(http, zone, youtubeVideosInfo, authorization) {
        this.http = http;
        this.zone = zone;
        this.youtubeVideosInfo = youtubeVideosInfo;
        this.authorization = authorization;
        this.isSearching = false;
        this.playlistInfo = new __WEBPACK_IMPORTED_MODULE_3__youtube_api_service__["a" /* YoutubeApiService */]({
            url: 'https://www.googleapis.com/youtube/v3/playlistItems',
            http: this.http,
            idKey: 'playlistId',
            config: {
                mine: 'true'
            }
        }, authorization);
        // TODO - extract to a Model / Reducer?
        // Reducer - because nextPageToken is changed
        // Model - new _config should be recreated easily with a new nextPageToken
        this.playlists = new __WEBPACK_IMPORTED_MODULE_3__youtube_api_service__["a" /* YoutubeApiService */]({
            url: 'https://www.googleapis.com/youtube/v3/playlists',
            http: this.http,
            config: {
                mine: 'true',
                part: 'snippet,id,contentDetails'
            }
        }, authorization);
        this.playlistApi = new __WEBPACK_IMPORTED_MODULE_3__youtube_api_service__["a" /* YoutubeApiService */]({
            url: 'https://www.googleapis.com/youtube/v3/playlists',
            http: this.http,
            idKey: 'id',
            config: {
                part: 'snippet,id,contentDetails'
            }
        }, authorization);
    }
    UserProfile.prototype.getPlaylists = function (isNewPage) {
        var hasAccessToken = this.playlists.hasToken();
        if (!hasAccessToken) {
            return;
        }
        if (isNewPage) {
            this.playlists.resetPageToken();
        }
        // TODO - extract to a reducer or/and an @Effect - SEARCH_START, SEARCH_COMPLETED
        this.isSearching = true;
        return this.playlists.getList();
    };
    UserProfile.prototype.updatePageToken = function (pageToken) {
        this.playlists.setPageToken(pageToken);
    };
    UserProfile.prototype.resetPageToken = function () {
        this.playlists.resetPageToken();
    };
    UserProfile.prototype.fetchPlaylist = function (playlistId) {
        return this.playlistApi.list(playlistId);
    };
    UserProfile.prototype.fetchPlaylistItems = function (playlistId, pageToken) {
        var _this = this;
        if (pageToken === void 0) { pageToken = ''; }
        // const token = this.playlists.config.get('access_token');
        if ('' === pageToken) {
            this.playlistInfo.deletePageToken();
        }
        else {
            this.playlistInfo.setPageToken(pageToken);
        }
        return this.playlistInfo.list(playlistId).pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["j" /* switchMap */])(function (response) {
            var videoIds = response.items
                .map(function (video) { return video.snippet.resourceId.videoId; })
                .join(',');
            return _this.youtubeVideosInfo.api.list(videoIds);
        }));
    };
    UserProfile.prototype.fetchAllPlaylistItems = function (playlistId) {
        var _this = this;
        var items = [];
        var subscriptions = [];
        var items$ = new __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__["a" /* Subject */]();
        var nextPageToken = '';
        var fetchMetadata = function (response) {
            var videoIds = response.items
                .map(function (video) { return video.snippet.resourceId.videoId; })
                .join(',');
            nextPageToken = response.nextPageToken;
            return _this.youtubeVideosInfo.api.list(videoIds);
        };
        var collectItems = function (videos) {
            items = items.concat(videos.items);
            if (nextPageToken) {
                fetchItems(playlistId, nextPageToken);
            }
            else {
                items$.next(items);
                subscriptions.forEach(function (_s) { return _s.unsubscribe(); });
                items$.complete();
            }
        };
        var fetchItems = function (id, token) {
            _this.playlistInfo.setPageToken(token);
            var sub = _this.playlistInfo
                .list(id)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["j" /* switchMap */])(function (response) { return fetchMetadata(response); }))
                .subscribe(function (response) { return collectItems(response); });
            subscriptions.push(sub);
            return sub;
        };
        fetchItems(playlistId, '');
        return items$.pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["k" /* take */])(1));
    };
    UserProfile.prototype.toUserJson = function (profile) {
        var _profile = {};
        if (profile) {
            _profile.imageUrl = profile.getImageUrl();
            _profile.name = profile.getName();
        }
        return _profile;
    };
    UserProfile.prototype.fetchMetadata = function (items) {
        var videoIds = items.map(function (video) { return video.id; }).join(',');
        return this.youtubeVideosInfo.api.list(videoIds);
    };
    UserProfile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_4__youtube_videos_info_service__["a" /* YoutubeVideosInfo */],
            __WEBPACK_IMPORTED_MODULE_5__authorization_service__["a" /* Authorization */]])
    ], UserProfile);
    return UserProfile;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/version-checker.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VersionCheckerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_app_api__ = __webpack_require__("./resources/assets/src/app/core/api/app.api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_retry__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/retry.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_timer__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/timer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








function verifyPackage(packageJson) {
    return packageJson.hasOwnProperty('version');
}
var VersionCheckerService = (function () {
    function VersionCheckerService(http, zone, appApi) {
        this.http = http;
        this.zone = zone;
        this.appApi = appApi;
        this.interval = 1000 * 60 * 60;
        this.protocol = 'https';
        this.prefix = 'raw.githubusercontent.com';
        this.repo = 'orizens/echoes-player';
        this.repoBranch = 'gh-pages';
        this.pathToFile = 'assets/package.json';
        this.url = this.protocol + "://" + this.prefix + "/" + this.repo + "/" + this.repoBranch + "/" + this.pathToFile;
    }
    VersionCheckerService.prototype.check = function () {
        return this.http.get(this.url);
    };
    VersionCheckerService.prototype.start = function () {
        var _this = this;
        var checkTimer;
        this.zone.runOutsideAngular(function () {
            checkTimer = __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].timer(0, _this.interval)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["j" /* switchMap */])(function () { return _this.check(); }), Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["h" /* retry */])(), Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["e" /* filter */])(verifyPackage))
                .subscribe(function (response) { return _this.appApi.recievedNewVersion(response); });
        });
        return checkTimer;
    };
    VersionCheckerService.prototype.updateVersion = function () {
        if (window) {
            window.location.reload(true);
        }
    };
    VersionCheckerService.prototype.checkForVersion = function () {
        var _this = this;
        return this.check()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["h" /* retry */])(), Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["e" /* filter */])(verifyPackage), Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["k" /* take */])(1))
            .subscribe(function (response) { return _this.appApi.notifyNewVersion(response); });
    };
    VersionCheckerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_3__api_app_api__["a" /* AppApi */]])
    ], VersionCheckerService);
    return VersionCheckerService;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/youtube-api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubeApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("./resources/assets/src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");



var defaultParams = {
    part: 'snippet,contentDetails',
    key: __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].youtube.API_KEY,
    maxResults: '50',
    pageToken: ''
};
// @Injectable()
var YoutubeApiService = (function () {
    function YoutubeApiService(options, authService) {
        this.authService = authService;
        this.authorize = false;
        this.isSearching = false;
        this.resetConfig();
        if (authService) {
            this.authorize = true;
        }
        if (options) {
            this.url = options.url;
            this.http = options.http;
            this.idKey = options.idKey || '';
            if (options.config) {
                this.setConfig(options.config);
            }
        }
    }
    YoutubeApiService.prototype.setConfig = function (options) {
        this.params = Object.keys(options).reduce(function (params, option) {
            return params.set(option, options[option]);
        }, this.params);
    };
    YoutubeApiService.prototype.hasToken = function () {
        return this.authService && this.authService.accessToken.length > 0;
    };
    YoutubeApiService.prototype.resetConfig = function () {
        this.params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpParams */]({ fromObject: defaultParams });
    };
    YoutubeApiService.prototype.getList = function () {
        this.isSearching = true;
        var options = {
            params: this.params,
            headers: this.createHeaders()
        };
        return this.http.get(this.url, options);
    };
    YoutubeApiService.prototype.list = function (id) {
        var _this = this;
        if (this.idKey) {
            this.setConfig((_a = {}, _a[this.idKey] = id, _a));
            // this.params[this.idKey] = id;
        }
        this.isSearching = true;
        var options = {
            params: this.params,
            headers: this.createHeaders()
        };
        return this.http.get(this.url, options).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["f" /* map */])(function (response) {
            _this.nextPageToken = response.nextPageToken;
            _this.isSearching = false;
            return response;
        }));
        var _a;
    };
    YoutubeApiService.prototype.fetchNextPage = function () {
        if (!this.isSearching) {
            // this.params['pageToken'] = this.nextPageToken;
            this.setPageToken(this.nextPageToken);
        }
    };
    YoutubeApiService.prototype.resetPageToken = function () {
        // this.params['pageToken'] = '';
        this.setPageToken('');
    };
    YoutubeApiService.prototype.setPageToken = function (pageToken) {
        this.setConfig({ pageToken: pageToken });
    };
    YoutubeApiService.prototype.deletePageToken = function () {
        this.params = this.params.delete('pageToken');
        console.log('remove pageToken', this.params.toString());
    };
    YoutubeApiService.prototype.createHeaders = function () {
        var accessToken = this.authService && this.authService.accessToken;
        var headers = {};
        if (accessToken && this.authorize) {
            headers['Authorization'] = "Bearer " + accessToken;
        }
        return headers;
    };
    return YoutubeApiService;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/youtube-data-api/faker.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fakeData; });
var fakeData = {
    "kind": "youtube#videoListResponse",
    "etag": "\"YuGNqL8VyJA_7QVzAZ4GTyhUvgs/TP0447AgOL1bk9vYoK9AAobeuf0\"",
    "nextPageToken": "CAUQAA",
    "pageInfo": {
        "totalResults": 29,
        "resultsPerPage": 5
    },
    "items": [
        {
            "kind": "youtube#video",
            "etag": "\"YuGNqL8VyJA_7QVzAZ4GTyhUvgs/xO7OM6bISm7CJELK0o3Uq5br5Bc\"",
            "id": "H81Tdrmz2LA",
            "snippet": {
                "publishedAt": "2017-12-28T14:13:37.000Z",
                "channelId": "UCvjgXvBlbQiydffZU7m1_aw",
                "title": "Coding Challenge #86: Cube Wave by Bees and Bombs",
                "description": "In this coding challenge, I attempt recreate a 3D @beesandbombs GIF with p5.js and the WebGL renderer.\n\nGIF: https://twitter.com/beesandbombs/status/940639806522085376\n\nSupport this channel on Patreon: https://patreon.com/codingtrain\nTo buy Coding Train merchandise: https://codingtrain.storenvy.com\nTo Support the Processing Foundation: https://processingfoundation.org/support\n\nSend me your questions and coding challenges!: https://github.com/CodingTrain/Rainbow-Topics\n\nContact: \nTwitter: https://twitter.com/shiffman\nThe Coding Train website: http://thecodingtrain.com/ \n\nLinks discussed in this video:\np5.js reference: https://p5js.org/reference/\nBees and Bombs: https://beesandbombs.tumblr.com/\n\nVideos discussed in this video:\nSimple Harmonic Motion: https://youtu.be/GvwPwIUSYqE\nWebGL playlist: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bPhi8sS1hHJ77n3zRO9FR_\n\nSource Code for the all Video Lessons: https://github.com/CodingTrain/Rainbow-Code\n\np5.js: https://p5js.org/\nProcessing: https://processing.org\n\nFor an Introduction to Programming: https://www.youtube.com/watch?v=8j0UDiN7my4&index=1&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA\nFor More Coding Challenges: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/H81Tdrmz2LA/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/H81Tdrmz2LA/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/H81Tdrmz2LA/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/H81Tdrmz2LA/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/H81Tdrmz2LA/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "The Coding Train",
                "tags": [
                    "JavaScript (Programming Language)",
                    "live",
                    "programming",
                    "daniel shiffman",
                    "creative coding",
                    "coding challenge",
                    "tutorial",
                    "coding",
                    "challenges",
                    "coding train",
                    "the coding train",
                    "live stream",
                    "itp nyu",
                    "class",
                    "webgl",
                    "threejs",
                    "3d javascript",
                    "3d canvas",
                    "webgl p5js",
                    "p5js 3d",
                    "3d animation",
                    "3d animation javascript",
                    "3d lights",
                    "3d material",
                    "simple harmonic motion",
                    "p5.js",
                    "p5.js tutorial",
                    "3d",
                    "javascript",
                    "programming challenge",
                    "bees and bombs",
                    "harmonic motion",
                    "sine wave",
                    "sine art"
                ],
                "categoryId": "27",
                "liveBroadcastContent": "none",
                "defaultLanguage": "en",
                "localized": {
                    "title": "Coding Challenge #86: Cube Wave by Bees and Bombs",
                    "description": "In this coding challenge, I attempt recreate a 3D @beesandbombs GIF with p5.js and the WebGL renderer.\n\nGIF: https://twitter.com/beesandbombs/status/940639806522085376\n\nSupport this channel on Patreon: https://patreon.com/codingtrain\nTo buy Coding Train merchandise: https://codingtrain.storenvy.com\nTo Support the Processing Foundation: https://processingfoundation.org/support\n\nSend me your questions and coding challenges!: https://github.com/CodingTrain/Rainbow-Topics\n\nContact: \nTwitter: https://twitter.com/shiffman\nThe Coding Train website: http://thecodingtrain.com/ \n\nLinks discussed in this video:\np5.js reference: https://p5js.org/reference/\nBees and Bombs: https://beesandbombs.tumblr.com/\n\nVideos discussed in this video:\nSimple Harmonic Motion: https://youtu.be/GvwPwIUSYqE\nWebGL playlist: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bPhi8sS1hHJ77n3zRO9FR_\n\nSource Code for the all Video Lessons: https://github.com/CodingTrain/Rainbow-Code\n\np5.js: https://p5js.org/\nProcessing: https://processing.org\n\nFor an Introduction to Programming: https://www.youtube.com/watch?v=8j0UDiN7my4&index=1&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA\nFor More Coding Challenges: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH"
                },
                "defaultAudioLanguage": "en"
            }
        },
        {
            "kind": "youtube#video",
            "etag": "\"YuGNqL8VyJA_7QVzAZ4GTyhUvgs/0zNFJSO1uloRI31LnvGzInkiBVk\"",
            "id": "01WZbEL8Ac8",
            "snippet": {
                "publishedAt": "2017-12-19T15:52:40.000Z",
                "channelId": "UCvjgXvBlbQiydffZU7m1_aw",
                "title": "Live Stream Archive - ITP Winter Show 2017",
                "description": "It's the ITP Winter Show 2017! This the archive of a live stream of me awkwardly wandering around and interviewing students about their work.  Stay tuned for an edited highlight reel.\n\nITP is a two-year graduate program located in the Tisch School of the Arts whose mission is to explore the imaginative use of communications technologies — how they might augment, improve, and bring delight and art into people's lives. \n\nhttp://itp.nyu.edu/shows/winter2017/show/\nhttps://tisch.nyu.edu/itp\n\nTimestamps to the different projects featured in this video:\n\n2:15 - \"Glow Box\" by Michael Simpson, Yeseul Song\n4:40 - \"Heart Sync\" by Ellen Nickles\n7:38 - \"Flappy Shadow\" by Kai-Che Hung, Tong Wu\n9:25 - \"Project Wings\" by Chengtao Yi, Meicheng Jia\n11:48 - \"Take Me To\" by Stephanie Koltun\n13:38 - \"Scenescoop\" by Cristobal Valenzuela\n17:25 - \"The Hand\" by Nick Wallace, Tong Wu\n20:25 - \"see . saw\" by Lin Zhang, Mengzhen Xiao\n23:38 - \"Wish Candles\" by Hye Ryeong Shin, Yeonhee Lee\n26:55 - Digital Fabrication Projects\n31:35 - \"Take the Money and Run!\" by Aidan Nelson, Simon Jensen\n33:30 - \"The cosmos in the water\" by yiyao nie\n34:55 - \"MIDI Glove\" by Oren Shoham\n36:07 - \"The Lingo Gizmo\" by Elizabeth Ferguson\n37:57 - \"Waterfall\" by Chengchao Zhu, Joohyun Park\n39:25 - \"Everyday Windows\" by Ilana Pecis Bonder, Nicolás Peña-Escarpentier\n41:03 - \"Interactive Whack-a-Mole\" by Kellee Massey\n42:38 - \"Mycophilia\" by Yue Fan\n44:25 - \"Anger Flanker\" by Krizia Fernando, Ridwan Madon, Vidia Anindhita\n45:58 - \"THE WALL\" by Dongphil Yoo\n47:34 - \"Sama Dance\" by Zohreh Zadbood\n48:54 - \"Sophia\" by Alden Jones\n51:40 - \"Extra\" by Nicolás Peña-Escarpentier, Stephanie Hagemeister \n52:41 - \"8 Beat\" by Ayal Rosenberg \n55:44 - \"The future forest experience\" by Lu Wang, Xiao Ma, Yu-Hao Ko\n57:00 - Panic Pusher by Chen Chen, Yanlin Ma\n1:08:03 - \"Swipe Reich: Anti-Tinder for Anti-Nazis\" by Melissa Parker\n1:13:24 - \"Worry Capsule Tree\" by Hau Yuan, Xiran Yang\n1:14:14 - \"The Laser Harp\" by Oren Shoham, Alan Peng\n1:15:15 - \"Love Testing\" by Danxiaomeng Huang, Eva Yipeng Chen, Ying Tung Ella Chung\n1:17:24 - \"Encounter\" by Joohyun Park\n1:18:26 - \"change-a-bit\" by Sejo Vega-Cebrian\n1:20:06 - \"Enlighten\" by Daniel Sebastian Castano\n1:22:00 - \"The Starting Line\" by Terrick Gutierrez, Lauren Race\n1:26:34 - \"KNOB\" by Anthony Bui, Brandon Newberg, Roland Arnoldt\n1:28:12 - \"Musical Gear Table\" by Itay Niv\n1:30:12 - \"*SIGH*\" by Namsoo Kim, Youjin Chung\n1:31:19 - \"Happy Holidays in NYC\" by Namsoo Kim, Youjin Chung\n1:34:10 - \"Dance!\" by Chengchao Zhu, Yifan Liu\n1:35:11 - \"The Traffic Jam\" by Alejandro Sanin\n1:36:22 - \"Emotions on Line\" by Yihan Chen\n1:40:05 - \"ADMIN\" by Wenqi Li\n1:41:17 - \"Cello Synth Control\" by Ahmad Arshad, Jesse Simpson\n1:43:47 - \"Chat Charades\" by Grau Puche Recarens, Regina Cantu De Alba\n1:47:00 - \"I Took a Pill in China\" by Li-Chung Chen, Yuan Chen\n1:48:55 - \"MagIc:Magnet+Music\" by Hayeon / Hayley Hwang\n1:55:40 - \"Into the Darkness\" by Heng Tang\n1:58:09 - \"Artificial Unintelligence\" by Shreiya Chowdhary\n2:01:00 - \"p5ML PONG\" by Alejandro Matamala\n2:02:17 - \"Presence\" by Dan Oved\n2:04:30 - \"2 Minute Hate\" by Fanni Fazakas\n\n\nHere is the complete list of projects:\nhttps://itp.nyu.edu/shows/winter2017/category/projects/\n\nHelp us caption & translate this video!\n\nhttps://amara.org/v/dZUY/",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/01WZbEL8Ac8/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/01WZbEL8Ac8/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/01WZbEL8Ac8/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/01WZbEL8Ac8/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/01WZbEL8Ac8/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "The Coding Train",
                "tags": [
                    "creative coding",
                    "coding train",
                    "tutorial",
                    "live",
                    "intelligence creative coding",
                    "live stream",
                    "processing",
                    "itp nyu",
                    "itp",
                    "itp spring 2017",
                    "nyu itp",
                    "nyu itp thesis projects"
                ],
                "categoryId": "27",
                "liveBroadcastContent": "none",
                "defaultLanguage": "en",
                "localized": {
                    "title": "Live Stream Archive - ITP Winter Show 2017",
                    "description": "It's the ITP Winter Show 2017! This the archive of a live stream of me awkwardly wandering around and interviewing students about their work.  Stay tuned for an edited highlight reel.\n\nITP is a two-year graduate program located in the Tisch School of the Arts whose mission is to explore the imaginative use of communications technologies — how they might augment, improve, and bring delight and art into people's lives. \n\nhttp://itp.nyu.edu/shows/winter2017/show/\nhttps://tisch.nyu.edu/itp\n\nTimestamps to the different projects featured in this video:\n\n2:15 - \"Glow Box\" by Michael Simpson, Yeseul Song\n4:40 - \"Heart Sync\" by Ellen Nickles\n7:38 - \"Flappy Shadow\" by Kai-Che Hung, Tong Wu\n9:25 - \"Project Wings\" by Chengtao Yi, Meicheng Jia\n11:48 - \"Take Me To\" by Stephanie Koltun\n13:38 - \"Scenescoop\" by Cristobal Valenzuela\n17:25 - \"The Hand\" by Nick Wallace, Tong Wu\n20:25 - \"see . saw\" by Lin Zhang, Mengzhen Xiao\n23:38 - \"Wish Candles\" by Hye Ryeong Shin, Yeonhee Lee\n26:55 - Digital Fabrication Projects\n31:35 - \"Take the Money and Run!\" by Aidan Nelson, Simon Jensen\n33:30 - \"The cosmos in the water\" by yiyao nie\n34:55 - \"MIDI Glove\" by Oren Shoham\n36:07 - \"The Lingo Gizmo\" by Elizabeth Ferguson\n37:57 - \"Waterfall\" by Chengchao Zhu, Joohyun Park\n39:25 - \"Everyday Windows\" by Ilana Pecis Bonder, Nicolás Peña-Escarpentier\n41:03 - \"Interactive Whack-a-Mole\" by Kellee Massey\n42:38 - \"Mycophilia\" by Yue Fan\n44:25 - \"Anger Flanker\" by Krizia Fernando, Ridwan Madon, Vidia Anindhita\n45:58 - \"THE WALL\" by Dongphil Yoo\n47:34 - \"Sama Dance\" by Zohreh Zadbood\n48:54 - \"Sophia\" by Alden Jones\n51:40 - \"Extra\" by Nicolás Peña-Escarpentier, Stephanie Hagemeister \n52:41 - \"8 Beat\" by Ayal Rosenberg \n55:44 - \"The future forest experience\" by Lu Wang, Xiao Ma, Yu-Hao Ko\n57:00 - Panic Pusher by Chen Chen, Yanlin Ma\n1:08:03 - \"Swipe Reich: Anti-Tinder for Anti-Nazis\" by Melissa Parker\n1:13:24 - \"Worry Capsule Tree\" by Hau Yuan, Xiran Yang\n1:14:14 - \"The Laser Harp\" by Oren Shoham, Alan Peng\n1:15:15 - \"Love Testing\" by Danxiaomeng Huang, Eva Yipeng Chen, Ying Tung Ella Chung\n1:17:24 - \"Encounter\" by Joohyun Park\n1:18:26 - \"change-a-bit\" by Sejo Vega-Cebrian\n1:20:06 - \"Enlighten\" by Daniel Sebastian Castano\n1:22:00 - \"The Starting Line\" by Terrick Gutierrez, Lauren Race\n1:26:34 - \"KNOB\" by Anthony Bui, Brandon Newberg, Roland Arnoldt\n1:28:12 - \"Musical Gear Table\" by Itay Niv\n1:30:12 - \"*SIGH*\" by Namsoo Kim, Youjin Chung\n1:31:19 - \"Happy Holidays in NYC\" by Namsoo Kim, Youjin Chung\n1:34:10 - \"Dance!\" by Chengchao Zhu, Yifan Liu\n1:35:11 - \"The Traffic Jam\" by Alejandro Sanin\n1:36:22 - \"Emotions on Line\" by Yihan Chen\n1:40:05 - \"ADMIN\" by Wenqi Li\n1:41:17 - \"Cello Synth Control\" by Ahmad Arshad, Jesse Simpson\n1:43:47 - \"Chat Charades\" by Grau Puche Recarens, Regina Cantu De Alba\n1:47:00 - \"I Took a Pill in China\" by Li-Chung Chen, Yuan Chen\n1:48:55 - \"MagIc:Magnet+Music\" by Hayeon / Hayley Hwang\n1:55:40 - \"Into the Darkness\" by Heng Tang\n1:58:09 - \"Artificial Unintelligence\" by Shreiya Chowdhary\n2:01:00 - \"p5ML PONG\" by Alejandro Matamala\n2:02:17 - \"Presence\" by Dan Oved\n2:04:30 - \"2 Minute Hate\" by Fanni Fazakas\n\n\nHere is the complete list of projects:\nhttps://itp.nyu.edu/shows/winter2017/category/projects/\n\nHelp us caption & translate this video!\n\nhttps://amara.org/v/dZUY/"
                },
                "defaultAudioLanguage": "en"
            }
        },
        {
            "kind": "youtube#video",
            "etag": "\"YuGNqL8VyJA_7QVzAZ4GTyhUvgs/MP0V6TdQhw4TyZQyv8qD_CNqAco\"",
            "id": "DqmXYPNrHcw",
            "snippet": {
                "publishedAt": "2015-02-06T13:00:01.000Z",
                "channelId": "UCj_iGliGCkLcHSZ8eqVNPDQ",
                "title": "Tutoriel : Trello",
                "description": "Dans ce tutoriel je vous propose de découvrir comment bien utiliser trello en utilisant la méthode Kanban\n\nRetrouvez un concentré du web autour du monde du développement web et du graphisme...\nFormez-vous et améliorez vos compétences à travers près de 161 heures de formation vidéo...\n\nSuivez Grafikart sur:\nFacebook: https://www.facebook.com/pages/Grafikart/483920250642\nTwitter: https://twitter.com/grafikart_fr\nPlus de tutoriels : http://www.grafikart.fr",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/DqmXYPNrHcw/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/DqmXYPNrHcw/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/DqmXYPNrHcw/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/DqmXYPNrHcw/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    }
                },
                "channelTitle": "Grafikart.fr",
                "tags": [
                    "Science & Technology",
                    "Grafikart Tv",
                    "Grafikart",
                    "tuto",
                    "tutoriel",
                    "trello",
                    "tuto trello",
                    "tutoriel trello",
                    "grafikart",
                    "grafikart tv",
                    "grafikart.fr",
                    "graphique art",
                    "graphisme",
                    "développement web",
                    "web",
                    "développement"
                ],
                "categoryId": "28",
                "liveBroadcastContent": "none",
                "localized": {
                    "title": "Tutoriel : Trello",
                    "description": "Dans ce tutoriel je vous propose de découvrir comment bien utiliser trello en utilisant la méthode Kanban\n\nRetrouvez un concentré du web autour du monde du développement web et du graphisme...\nFormez-vous et améliorez vos compétences à travers près de 161 heures de formation vidéo...\n\nSuivez Grafikart sur:\nFacebook: https://www.facebook.com/pages/Grafikart/483920250642\nTwitter: https://twitter.com/grafikart_fr\nPlus de tutoriels : http://www.grafikart.fr"
                }
            }
        },
        {
            "kind": "youtube#video",
            "etag": "\"YuGNqL8VyJA_7QVzAZ4GTyhUvgs/td1etfF5BrNa2h1H57mIbHMH_zI\"",
            "id": "3pqkz4Ac_UE",
            "snippet": {
                "publishedAt": "2017-03-26T15:00:01.000Z",
                "channelId": "UCsehVxGYQuCVtqVLnnHihqA",
                "title": "Ionic 2 • Explication et définition d'un web-service (API) • Épisode 5",
                "description": "On se retrouve aujourd'hui dans le cinquième épisode de notre série consacrée au développement d'une application hybride via le framework Ionic 2 pour iOS, Android et Windows Phone.\n\nA partir de cet épisode nous allons nous focaliser sur l'utilisation des web-services (API) au sein d'une application Ionic 2. Durant cette épisode, je vais vous expliquer en détail ce qu'est un web-service mais également pourquoi nous allons en utiliser. Nous verrons dans l'épisode 6 l'intégration de ces derniers.\n\nBien entendu, il est parfaitement possible de suivre cet épisode sous Windows, Mac ou Linux =)\n\n▬▬▬▬▬ Au programme ▬▬▬▬▬\n\n► Qu'est-ce qu'un web-service (API) ?\n► Comment fonctionne un web-service ?\n► Pourquoi faire appel à un web-service ?\n► Démonstration avec l'API gratuite : TheMovieDB.\n► Utilisation du logiciel gratuit PostMan afin de construire nos requêtes HTTP.\n► Qu'est-ce que du JSON ?\n► Choix de l'API à utiliser lors de l'épisode 6.\n\n▬▬▬▬▬▬▬ Liens ▬▬▬▬▬▬▬\n\n► PostMan : http://bit.ly/1K5ZGHG\n► TheMovieDB API : http://bit.ly/2njTx44\n► Liste des APIs gratuites (github) : https://github.com/abhishekbanthia/Public-APIs\n► Liste des APIs gratuites (mashape) : https://market.mashape.com/explore\n\n▬▬▬▬▬▬ Informations ▬▬▬▬▬▬\n\nBon visionnage !\nJ'espère que cette épisode vous plaira !\n\nPS : N'hésitez pas à mettre en HD !\n\nRetrouvez la série via sa playlist officielle : http://bit.ly/ionic2retro\n\nN'hésitez pas à faire des suggestions dans les commentaires !\nSi jamais des points abordés dans cet épisode ne sont pas clairs, dîtes-le moi !\n\n▬▬▬▬▬▬ Musiques ▬▬▬▬▬▬\n\n- Nicolai Heidlas - Sunny Holidays\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Sunday Morning\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Wings\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Happy Harmonies\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - SunnySide\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Sunny Afternoon\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\nMerci pour les + de 5 000 abonnés !!\n@+\n\n► Pour soutenir la chaîne : https://www.tipeee.com/theiphoneretro\n► S'abonner à la chaîne : http://bit.ly/22j9tkU\n► Twitter : https://twitter.com/iphoneretro\n► Facebook : http://bit.ly/20iX9iR\n► Achetez vos jeux moins cher : https://www.instant-gaming.com/igr/gamer-630810/\n► Contribuer aux traductions de vidéos : http://bit.ly/2qN5bFL",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/3pqkz4Ac_UE/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/3pqkz4Ac_UE/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/3pqkz4Ac_UE/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/3pqkz4Ac_UE/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/3pqkz4Ac_UE/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "TheiPhoneRetro",
                "tags": [
                    "Apple",
                    "iPod",
                    "Touch",
                    "Retro",
                    "iPhone",
                    "iPad",
                    "Tutoriels",
                    "Tutorial",
                    "How-to",
                    "Presentation",
                    "Review",
                    "Ionic 2",
                    "Ionic",
                    "Framework",
                    "Angular",
                    "AngularJS",
                    "Google",
                    "Javascript",
                    "Html",
                    "Css",
                    "Typescript",
                    "Français",
                    "Fr",
                    "Tuto",
                    "Android",
                    "Windows Phone",
                    "Développement",
                    "Dev",
                    "Ionic view",
                    "Windows",
                    "Linux",
                    "Free",
                    "Gratuit",
                    "Episode",
                    "Serie",
                    "Installation",
                    "Xcode",
                    "Android Studio",
                    "Java",
                    "Création",
                    "RetroTuto",
                    "Push",
                    "Data",
                    "Object",
                    "Key",
                    "Value",
                    "Clé",
                    "Valeur",
                    "Episode 5",
                    "The Movie Database",
                    "API",
                    "Web-service",
                    "Web",
                    "Service",
                    "REST",
                    "RESTFull",
                    "JSON",
                    "PostMan"
                ],
                "categoryId": "28",
                "liveBroadcastContent": "none",
                "defaultLanguage": "fr",
                "localized": {
                    "title": "Ionic 2 • Explication et définition d'un web-service (API) • Épisode 5",
                    "description": "On se retrouve aujourd'hui dans le cinquième épisode de notre série consacrée au développement d'une application hybride via le framework Ionic 2 pour iOS, Android et Windows Phone.\n\nA partir de cet épisode nous allons nous focaliser sur l'utilisation des web-services (API) au sein d'une application Ionic 2. Durant cette épisode, je vais vous expliquer en détail ce qu'est un web-service mais également pourquoi nous allons en utiliser. Nous verrons dans l'épisode 6 l'intégration de ces derniers.\n\nBien entendu, il est parfaitement possible de suivre cet épisode sous Windows, Mac ou Linux =)\n\n▬▬▬▬▬ Au programme ▬▬▬▬▬\n\n► Qu'est-ce qu'un web-service (API) ?\n► Comment fonctionne un web-service ?\n► Pourquoi faire appel à un web-service ?\n► Démonstration avec l'API gratuite : TheMovieDB.\n► Utilisation du logiciel gratuit PostMan afin de construire nos requêtes HTTP.\n► Qu'est-ce que du JSON ?\n► Choix de l'API à utiliser lors de l'épisode 6.\n\n▬▬▬▬▬▬▬ Liens ▬▬▬▬▬▬▬\n\n► PostMan : http://bit.ly/1K5ZGHG\n► TheMovieDB API : http://bit.ly/2njTx44\n► Liste des APIs gratuites (github) : https://github.com/abhishekbanthia/Public-APIs\n► Liste des APIs gratuites (mashape) : https://market.mashape.com/explore\n\n▬▬▬▬▬▬ Informations ▬▬▬▬▬▬\n\nBon visionnage !\nJ'espère que cette épisode vous plaira !\n\nPS : N'hésitez pas à mettre en HD !\n\nRetrouvez la série via sa playlist officielle : http://bit.ly/ionic2retro\n\nN'hésitez pas à faire des suggestions dans les commentaires !\nSi jamais des points abordés dans cet épisode ne sont pas clairs, dîtes-le moi !\n\n▬▬▬▬▬▬ Musiques ▬▬▬▬▬▬\n\n- Nicolai Heidlas - Sunny Holidays\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Sunday Morning\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Wings\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Happy Harmonies\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - SunnySide\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\n- Nicolai Heidlas - Sunny Afternoon\nhttps://soundcloud.com/nicolai-heidlas\nhttps://www.youtube.com/channel/UC-B3l3dgbIk2lOkUpHkpCDQ\nhttps://www.facebook.com/nicolaiheidlas/?ref=hl\nhttps://twitter.com/NHeidlas\n\nMerci pour les + de 5 000 abonnés !!\n@+\n\n► Pour soutenir la chaîne : https://www.tipeee.com/theiphoneretro\n► S'abonner à la chaîne : http://bit.ly/22j9tkU\n► Twitter : https://twitter.com/iphoneretro\n► Facebook : http://bit.ly/20iX9iR\n► Achetez vos jeux moins cher : https://www.instant-gaming.com/igr/gamer-630810/\n► Contribuer aux traductions de vidéos : http://bit.ly/2qN5bFL"
                },
                "defaultAudioLanguage": "fr"
            }
        },
        {
            "kind": "youtube#video",
            "etag": "\"YuGNqL8VyJA_7QVzAZ4GTyhUvgs/m0DkyjfIRjPOF6dJsqnzmYrjtOY\"",
            "id": "ECuaZxM82fw",
            "snippet": {
                "publishedAt": "2016-05-30T12:12:51.000Z",
                "channelId": "UCj_iGliGCkLcHSZ8eqVNPDQ",
                "title": "JavaScript (1/23) : Apprendre le JavaScript depuis le PHP",
                "description": "Plus d'infos : https://www.grafikart.fr/formations/debuter-javascript/php-to-js\n\nLe problème lorsque l'on cherche à apprendre un nouveau langage est que l'on se retape systématiquement les bases : Les variables, les fonctions, les conditions... Ce qui peut s'avérer pénible lorsque l'on connait déjà un autre langage de programmation. Aussi, je vous propose d'utiliser les connaissances que vous avez déjà en PHP afin d'accélerer votre apprentissage du JavaScript.\n\nRetrouvez tous les tutoriels sur https://www.grafikart.fr",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/ECuaZxM82fw/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/ECuaZxM82fw/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/ECuaZxM82fw/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/ECuaZxM82fw/sddefault.jpg",
                        "width": 640,
                        "height": 480
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/ECuaZxM82fw/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720
                    }
                },
                "channelTitle": "Grafikart.fr",
                "categoryId": "28",
                "liveBroadcastContent": "none",
                "defaultLanguage": "fr",
                "localized": {
                    "title": "JavaScript (1/23) : Apprendre le JavaScript depuis le PHP",
                    "description": "Plus d'infos : https://www.grafikart.fr/formations/debuter-javascript/php-to-js\n\nLe problème lorsque l'on cherche à apprendre un nouveau langage est que l'on se retape systématiquement les bases : Les variables, les fonctions, les conditions... Ce qui peut s'avérer pénible lorsque l'on connait déjà un autre langage de programmation. Aussi, je vous propose d'utiliser les connaissances que vous avez déjà en PHP afin d'accélerer votre apprentissage du JavaScript.\n\nRetrouvez tous les tutoriels sur https://www.grafikart.fr"
                },
                "defaultAudioLanguage": "fr"
            }
        }
    ]
};


/***/ }),

/***/ "./resources/assets/src/app/core/services/youtube-data-api/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataApiProviders; });
/* unused harmony export _defaultUrlParams */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return YoutubeDataApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_environments_environment__ = __webpack_require__("./resources/assets/src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__authorization_service__ = __webpack_require__("./resources/assets/src/app/core/services/authorization.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__faker__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-data-api/faker.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DataApiProviders = {
    SEARCH: 'search',
    PLAYLISTS: 'playlists'
};
var _defaultUrlParams = {
    part: 'snippet,id',
    maxResults: '50',
    key: __WEBPACK_IMPORTED_MODULE_2_environments_environment__["a" /* environment */].youtube.API_KEY
};
/**
 * Performs http requests using HttpClientModule to youtube-api v3.
 *
 * `YoutubeApi` is available as an injectable class, with methods to perform youtube api
 *  methods requests
 *
 * ### Example
 *
 * ```typescript
 * import {YoutubeDataApi} from './services/YoutubeDataApi';
 * @Component({
 *   selector: 'yt-app',
 * })
 * class VideosComponent {
 *   options = {
 *    part: 'snippet,id',
 *    q: 'chillstep'
 *   };
 *   constructor(yt: YoutubeDataApi) {
 *     yt.list('search', options)
 *       // Subscribe to the observable to get the parsed videos
 *       .subscribe(response => this.videos = response.videos);
 *   }
 * }
 * ```
 */
var YoutubeDataApi = (function () {
    function YoutubeDataApi(http, auth) {
        this.http = http;
        this.auth = auth;
        // public _config: HttpParams = new HttpParams();
        this._apiPrefixUrl = 'https://www.googleapis.com/youtube';
        this._apiVersion = 'v3';
    }
    Object.defineProperty(YoutubeDataApi.prototype, "_apiUrl", {
        get: function () {
            return this._apiPrefixUrl + "/" + this._apiVersion;
        },
        enumerable: true,
        configurable: true
    });
    YoutubeDataApi.prototype.list = function (api, options) {
        var params = __assign({}, _defaultUrlParams, options);
        var _options = {
            params: params,
            headers: this.createHeaders(false)
        };
        return this.fakerGet(); //this.http.get(this.getApi(api), _options);
    };
    YoutubeDataApi.prototype.delete = function (api, options) {
        return this._request(api);
    };
    YoutubeDataApi.prototype.insert = function (api, options) {
        return this.http.post(this.getApi(api), {});
    };
    YoutubeDataApi.prototype.update = function (api) {
        return this._request(api);
    };
    YoutubeDataApi.prototype._request = function (api) {
        // const options: RequestOptionsArgs = {
        //   search: this.config,
        //   headers: this.createHeaders()
        // };
        // this.http.
    };
    YoutubeDataApi.prototype.createHeaders = function (addAuth) {
        var accessToken = this.auth.accessToken;
        var headersOptions = {};
        if (accessToken && addAuth) {
            headersOptions['authorization'] = "Bearer " + accessToken;
        }
        return new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpHeaders */](headersOptions);
    };
    YoutubeDataApi.prototype.getApi = function (api) {
        return this._apiUrl + "/" + api;
    };
    YoutubeDataApi.prototype.fakerGet = function () {
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["a" /* Observable */].of(__WEBPACK_IMPORTED_MODULE_5__faker__["a" /* fakeData */]);
    };
    YoutubeDataApi = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__authorization_service__["a" /* Authorization */]])
    ], YoutubeDataApi);
    return YoutubeDataApi;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/youtube-player.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubePlayerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var YoutubePlayerService = (function () {
    function YoutubePlayerService(store, zone) {
        this.store = store;
        this.zone = zone;
    }
    YoutubePlayerService.prototype.setupPlayer = function (player) {
        this.player = player;
    };
    YoutubePlayerService.prototype.play = function () {
        var _this = this;
        this.zone.runOutsideAngular(function () { return _this.player.playVideo(); });
    };
    YoutubePlayerService.prototype.pause = function () {
        var _this = this;
        this.zone.runOutsideAngular(function () { return _this.player.pauseVideo(); });
    };
    YoutubePlayerService.prototype.playVideo = function (media, seconds) {
        var _this = this;
        var id = media.id;
        var isLoaded = this.player.getVideoUrl().includes(id);
        if (!isLoaded) {
            this.zone.runOutsideAngular(function () {
                return _this.player.loadVideoById(id, seconds || undefined);
            });
        }
        this.play();
    };
    YoutubePlayerService.prototype.seekTo = function (seconds) {
        var _this = this;
        this.zone.runOutsideAngular(function () { return _this.player.seekTo(seconds, true); });
    };
    // Not in use
    YoutubePlayerService.prototype.onPlayerStateChange = function (event) {
        var state = event.data;
        // let autoNext = false;
        // play the next song if its not the end of the playlist
        // should add a "repeat" feature
        if (state === YT.PlayerState.ENDED) {
            // this.listeners.ended.forEach(callback => callback(state));
        }
        if (state === YT.PlayerState.PAUSED) {
            // service.playerState = YT.PlayerState.PAUSED;
        }
        if (state === YT.PlayerState.PLAYING) {
            // service.playerState = YT.PlayerState.PLAYING;
        }
    };
    YoutubePlayerService.prototype.setSize = function (height, width) {
        var _this = this;
        this.zone.runOutsideAngular(function () {
            _this.player.setSize(width, height);
        });
    };
    YoutubePlayerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], YoutubePlayerService);
    return YoutubePlayerService;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/youtube-videos-info.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubeVideosInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__youtube_api_service__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__authorization_service__ = __webpack_require__("./resources/assets/src/app/core/services/authorization.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var YoutubeVideosInfo = (function () {
    function YoutubeVideosInfo(http, auth) {
        this.http = http;
        this.api = new __WEBPACK_IMPORTED_MODULE_2__youtube_api_service__["a" /* YoutubeApiService */]({
            url: 'https://www.googleapis.com/youtube/v3/videos',
            http: this.http,
            idKey: 'id',
            config: {
                part: 'snippet,contentDetails,statistics'
            }
        }, auth);
    }
    YoutubeVideosInfo.prototype.fetchVideoData = function (mediaId) {
        return this.api.list(mediaId).pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["f" /* map */])(function (response) { return response.items[0]; }));
    };
    YoutubeVideosInfo.prototype.fetchVideosData = function (mediaIds) {
        return this.api.list(mediaIds).pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["f" /* map */])(function (response) { return response.items; }));
    };
    YoutubeVideosInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__authorization_service__["a" /* Authorization */]])
    ], YoutubeVideosInfo);
    return YoutubeVideosInfo;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/services/youtube.search.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export SearchParams */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubeSearch; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__youtube_data_api__ = __webpack_require__("./resources/assets/src/app/core/services/youtube-data-api/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchTypes = {
    VIDEO: 'video',
    PLAYLIST: 'playlist',
    CHANNEL: 'channel'
};
var SearchParams = {
    Types: (_a = {},
        _a[SearchTypes.VIDEO] = 'video',
        _a[SearchTypes.PLAYLIST] = 'playlist',
        _a[SearchTypes.CHANNEL] = 'channel',
        _a)
};
var YoutubeSearch = (function () {
    function YoutubeSearch(youtubeDataApi) {
        this.youtubeDataApi = youtubeDataApi;
        this._api = __WEBPACK_IMPORTED_MODULE_1__youtube_data_api__["a" /* DataApiProviders */].SEARCH;
        this._apiOptions = {
            part: 'snippet,id',
            q: '',
            type: 'video',
            pageToken: ''
        };
    }
    /**
     * search for video
     * @param query {string}
     * @param params {key/value object}
     */
    YoutubeSearch.prototype.search = function (query, params) {
        if (query || '' === query) {
            var preset = params ? " " + params.preset : '';
            this._apiOptions.q = "" + query + preset;
        }
        return this.youtubeDataApi.list(this._api, this._apiOptions);
    };
    /**
     * resolves which type to search for
     * @param type any type of SearchTypes
     * @param query any string
     * @param params params of api
     */
    YoutubeSearch.prototype.searchFor = function (type, query, params) {
        switch (type) {
            case SearchTypes.VIDEO: {
                return this.searchVideo(query, params);
            }
            case SearchTypes.PLAYLIST: {
                return this.searchForPlaylist(query, params);
            }
        }
    };
    /**
     * search for video
     * @param query {string}
     * @param params {key/value object}
     */
    YoutubeSearch.prototype.searchVideo = function (query, params) {
        this._apiOptions.type = SearchParams.Types[SearchTypes.VIDEO];
        return this.search(query, params);
    };
    /**
     * search for playlist
     * @param query {string}
     * @param params {key/value object}
     */
    YoutubeSearch.prototype.searchForPlaylist = function (query, params) {
        var _this = this;
        this._apiOptions.type = SearchParams.Types[SearchTypes.PLAYLIST];
        return this.search(query, params).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["j" /* switchMap */])(function (response) {
            var options = {
                part: 'snippet,id,contentDetails',
                id: response.items.map(function (pl) { return pl.id.playlistId; }).join(',')
            };
            return _this.youtubeDataApi.list(__WEBPACK_IMPORTED_MODULE_1__youtube_data_api__["a" /* DataApiProviders */].PLAYLISTS, options);
        }));
    };
    YoutubeSearch.prototype.searchMore = function (nextPageToken) {
        this._apiOptions.pageToken = nextPageToken;
        return this;
    };
    YoutubeSearch.prototype.resetPageToken = function () {
        this._apiOptions.pageToken = '';
        return this;
    };
    YoutubeSearch = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__youtube_data_api__["b" /* YoutubeDataApi */]])
    ], YoutubeSearch);
    return YoutubeSearch;
}());

var _a;


/***/ }),

/***/ "./resources/assets/src/app/core/store/app-layout/app-layout.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return RecievedAppVersion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return UpdateAppVersion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CheckVersion; });
/* unused harmony export ExpandSidebar */
/* unused harmony export CollapseSidebar */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return ToggleSidebar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return ThemeChange; });
var ActionTypes = (function () {
    function ActionTypes() {
    }
    ActionTypes.SIDEBAR_EXPAND = '[APP LAYOUT] SIDEBAR_EXPAND';
    ActionTypes.SIDEBAR_COLLAPSE = '[APP LAYOUT] SIDEBAR_COLLAPSE';
    ActionTypes.SIDEBAR_TOGGLE = '[APP LAYOUT] SIDEBAR_TOGGLE';
    ActionTypes.APP_VERSION_RECIEVED = '[APP] APP_VERSION_RECIEVED';
    ActionTypes.APP_UPDATE_VERSION = '[APP] APP_UPDATE_VERSION';
    ActionTypes.APP_CHECK_VERSION = '[APP] APP_CHECK_VERSION';
    ActionTypes.APP_THEME_CHANGE = '[App Theme] APP_THEME_CHANGE';
    return ActionTypes;
}());

var RecievedAppVersion = (function () {
    function RecievedAppVersion(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_VERSION_RECIEVED;
    }
    return RecievedAppVersion;
}());

var UpdateAppVersion = (function () {
    function UpdateAppVersion() {
        this.type = ActionTypes.APP_UPDATE_VERSION;
        this.payload = '';
    }
    return UpdateAppVersion;
}());

var CheckVersion = (function () {
    function CheckVersion() {
        this.type = ActionTypes.APP_CHECK_VERSION;
        this.payload = '';
    }
    return CheckVersion;
}());

var ExpandSidebar = (function () {
    function ExpandSidebar() {
        this.type = ActionTypes.SIDEBAR_EXPAND;
        this.payload = true;
    }
    return ExpandSidebar;
}());

var CollapseSidebar = (function () {
    function CollapseSidebar() {
        this.type = ActionTypes.SIDEBAR_COLLAPSE;
        this.payload = false;
    }
    return CollapseSidebar;
}());

var ToggleSidebar = (function () {
    function ToggleSidebar() {
        this.type = ActionTypes.SIDEBAR_TOGGLE;
        this.payload = '';
    }
    return ToggleSidebar;
}());

var ThemeChange = (function () {
    function ThemeChange(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_THEME_CHANGE;
    }
    return ThemeChange;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/app-layout/app-layout.reducer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = appLayout;
/* unused harmony export getSidebarExpanded */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_layout_actions__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/app-layout.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_themes__ = __webpack_require__("./resources/assets/src/app/app.themes.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var initialState = {
    sidebarExpanded: true,
    requestInProcess: false,
    version: {
        semver: '',
        isNewAvailable: false,
        checkingForVersion: false
    },
    theme: __WEBPACK_IMPORTED_MODULE_1__app_themes__["a" /* DEFAULT_THEME */],
    themes: __WEBPACK_IMPORTED_MODULE_1__app_themes__["b" /* Themes */].sort()
};
function appLayout(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__app_layout_actions__["a" /* ActionTypes */].SIDEBAR_EXPAND:
            return __assign({}, state, { sidebarExpanded: true });
        case __WEBPACK_IMPORTED_MODULE_0__app_layout_actions__["a" /* ActionTypes */].SIDEBAR_COLLAPSE:
            return __assign({}, state, { sidebarExpanded: false });
        case __WEBPACK_IMPORTED_MODULE_0__app_layout_actions__["a" /* ActionTypes */].SIDEBAR_TOGGLE:
            return __assign({}, state, { sidebarExpanded: !state.sidebarExpanded });
        case __WEBPACK_IMPORTED_MODULE_0__app_layout_actions__["a" /* ActionTypes */].APP_VERSION_RECIEVED: {
            var version = getVersion(state, action.payload);
            return __assign({}, state, { version: version });
        }
        case __WEBPACK_IMPORTED_MODULE_0__app_layout_actions__["a" /* ActionTypes */].APP_CHECK_VERSION: {
            var version = __assign({}, state.version, { checkingForVersion: true });
            return __assign({}, state, { version: version });
        }
        case __WEBPACK_IMPORTED_MODULE_0__app_layout_actions__["a" /* ActionTypes */].APP_THEME_CHANGE: {
            return __assign({}, state, { theme: action.payload });
        }
        default:
            return __assign({}, initialState, state, { themes: __WEBPACK_IMPORTED_MODULE_1__app_themes__["b" /* Themes */].sort() });
    }
}
function getSidebarExpanded($state) {
    return $state.select(function (state) { return state.sidebarExpanded; });
}
function getVersion(state, packageJson) {
    var currentVersion = state.version.semver;
    var remoteVersion = packageJson.version;
    var version = {
        semver: '',
        isNewAvailable: state.version.isNewAvailable,
        checkingForVersion: false
    };
    var isCurrentVersionEmpty = '' === currentVersion;
    var isCurrentDifferentFromRemote = !isCurrentVersionEmpty && currentVersion !== remoteVersion;
    if (isCurrentVersionEmpty) {
        version.semver = remoteVersion;
    }
    if (isCurrentDifferentFromRemote && !version.isNewAvailable) {
        version.semver = currentVersion;
        version.isNewAvailable = true;
    }
    else {
        // upgrade is completed
        version.semver = remoteVersion;
        version.isNewAvailable = false;
    }
    return version;
}


/***/ }),

/***/ "./resources/assets/src/app/core/store/app-layout/app-layout.selectors.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export getAppSettings */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAppTheme; });
/* unused harmony export getAllAppThemes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getAppThemes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getAppVersion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getSidebarCollapsed; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");

var getAppSettings = function (state) { return state.appLayout; };
var getAppTheme = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getAppSettings, function (state) { return state.theme; });
var getAllAppThemes = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getAppSettings, function (state) { return state.themes; });
var getAppThemes = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getAppSettings, getAppTheme, getAllAppThemes, function (appLayout, theme, themes) { return ({
    selected: theme,
    themes: themes.map(function (_theme) { return ({ label: _theme, value: _theme }); })
}); });
var getAppVersion = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getAppSettings, function (state) { return state.version; });
var getSidebarCollapsed = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getAppSettings, function (state) { return !state.sidebarExpanded; });


/***/ }),

/***/ "./resources/assets/src/app/core/store/app-layout/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_layout_reducer__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/app-layout.reducer.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_0__app_layout_reducer__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_layout_actions__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/app-layout.actions.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__app_layout_actions__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__app_layout_actions__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__app_layout_actions__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__app_layout_actions__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_1__app_layout_actions__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_1__app_layout_actions__["f"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_layout_selectors__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/app-layout.selectors.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_2__app_layout_selectors__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_2__app_layout_selectors__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_2__app_layout_selectors__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_2__app_layout_selectors__["d"]; });





/***/ }),

/***/ "./resources/assets/src/app/core/store/app-player/app-player.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return PlayVideo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return PauseVideo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return TogglePlayer; });
/* unused harmony export LoadNextTrack */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return LoadAndPlay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return PlayStarted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return UpdateState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FullScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return ResetFullScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return Reset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return SetupPlayer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return PlayerStateChange; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ActionTypes = (function () {
    function ActionTypes() {
    }
    ActionTypes.PLAY = '[Player] PLAY';
    ActionTypes.PAUSE = '[Player] PAUSE';
    ActionTypes.SETUP_PLAYER = '[Player] SETUP_PLAYER';
    ActionTypes.LOAD_AND_PLAY = '[Player] LOAD_AND_PLAY';
    ActionTypes.QUEUE = '[Player] REMOVE';
    ActionTypes.PLAY_STARTED = '[Player] PLAY_STARTED';
    ActionTypes.TOGGLE_PLAYER = '[Player] TOGGLE_PLAYER';
    ActionTypes.UPDATE_STATE = '[Player] STATE_CHANGE';
    ActionTypes.PLAYER_STATE_CHANGE = '[Player] PLAYER_STATE_CHANGE';
    ActionTypes.FULLSCREEN = '[Player] FULLSCREEN';
    ActionTypes.RESET = '[Player] RESET';
    ActionTypes.LOAD_NEXT_TRACK = '[Player] LOAD_NEXT_TRACK';
    ActionTypes.RESET_FULLSCREEN = '[Player] RESET_FULLSCREEN';
    ActionTypes = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], ActionTypes);
    return ActionTypes;
}());

var PlayVideo = (function () {
    function PlayVideo(payload) {
        this.payload = payload;
        this.type = ActionTypes.PLAY;
    }
    return PlayVideo;
}());

var PauseVideo = (function () {
    function PauseVideo(payload) {
        if (payload === void 0) { payload = ''; }
        this.payload = payload;
        this.type = ActionTypes.PAUSE;
    }
    return PauseVideo;
}());

var TogglePlayer = (function () {
    function TogglePlayer(payload) {
        if (payload === void 0) { payload = true; }
        this.payload = payload;
        this.type = ActionTypes.TOGGLE_PLAYER;
    }
    return TogglePlayer;
}());

var LoadNextTrack = (function () {
    function LoadNextTrack() {
        this.type = ActionTypes.LOAD_NEXT_TRACK;
        this.payload = '';
    }
    return LoadNextTrack;
}());

var LoadAndPlay = (function () {
    function LoadAndPlay(payload) {
        this.payload = payload;
        this.type = ActionTypes.LOAD_AND_PLAY;
    }
    return LoadAndPlay;
}());

var PlayStarted = (function () {
    function PlayStarted(payload) {
        this.payload = payload;
        this.type = ActionTypes.PLAY_STARTED;
    }
    return PlayStarted;
}());

var UpdateState = (function () {
    function UpdateState(payload) {
        this.payload = payload;
        this.type = ActionTypes.UPDATE_STATE;
    }
    return UpdateState;
}());

var FullScreen = (function () {
    function FullScreen() {
        this.type = ActionTypes.FULLSCREEN;
        this.payload = '';
    }
    return FullScreen;
}());

var ResetFullScreen = (function () {
    function ResetFullScreen() {
        this.type = ActionTypes.RESET_FULLSCREEN;
        this.payload = '';
    }
    return ResetFullScreen;
}());

var Reset = (function () {
    function Reset() {
        this.type = ActionTypes.RESET;
        this.payload = '';
    }
    return Reset;
}());

var SetupPlayer = (function () {
    function SetupPlayer(payload) {
        this.payload = payload;
        this.type = ActionTypes.SETUP_PLAYER;
    }
    return SetupPlayer;
}());

var PlayerStateChange = (function () {
    function PlayerStateChange(payload) {
        this.payload = payload;
        this.type = ActionTypes.PLAYER_STATE_CHANGE;
    }
    return PlayerStateChange;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/app-player/app-player.reducer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["m"] = player;
/* unused harmony export playVideo */
/* unused harmony export toggleVisibility */
/* unused harmony export changePlayerState */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_player_actions__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/app-player.actions.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["g"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["h"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["i"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["j"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["k"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["l"]; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var initialPlayerState = {
    mediaId: { videoId: 'NONE' },
    index: 0,
    media: {
        snippet: { title: 'No Media Yet' }
    },
    showPlayer: true,
    playerState: 0,
    fullscreen: {
        on: false,
        height: 270,
        width: 367
    },
    isFullscreen: false
};
function player(state, action) {
    if (state === void 0) { state = initialPlayerState; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a" /* ActionTypes */].PLAY:
            return playVideo(state, action.payload);
        case __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a" /* ActionTypes */].QUEUE:
            return state;
        case __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a" /* ActionTypes */].TOGGLE_PLAYER:
            return toggleVisibility(state);
        case __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a" /* ActionTypes */].UPDATE_STATE:
            return changePlayerState(state, action.payload);
        case __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a" /* ActionTypes */].FULLSCREEN: {
            var on = !state.fullscreen.on;
            var _a = initialPlayerState.fullscreen, height = _a.height, width = _a.width;
            if (on) {
                height = window.innerHeight;
                width = window.innerWidth;
            }
            var fullscreen = { on: on, height: height, width: width };
            return __assign({}, state, { fullscreen: fullscreen });
        }
        case __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a" /* ActionTypes */].RESET:
            return __assign({}, state, { isFullscreen: false, playerState: 0 });
        case __WEBPACK_IMPORTED_MODULE_0__app_player_actions__["a" /* ActionTypes */].RESET_FULLSCREEN: {
            var fullscreen = initialPlayerState.fullscreen;
            return __assign({}, initialPlayerState, state, { fullscreen: fullscreen });
        }
        default:
            return __assign({}, initialPlayerState, state);
    }
}
function playVideo(state, media) {
    return __assign({}, state, { mediaId: media.id, media: media });
}
function toggleVisibility(state) {
    return __assign({}, state, { showPlayer: !state.showPlayer });
}
function changePlayerState(state, playerState) {
    return __assign({}, state, { playerState: playerState });
}


/***/ }),

/***/ "./resources/assets/src/app/core/store/app-player/app-player.selectors.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getPlayer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getCurrentMedia; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getIsPlayerPlaying; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return getShowPlayer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getPlayerFullscreen; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");

var getPlayer = function (state) { return state.player; };
var getCurrentMedia = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayer, function (player) { return player.media; });
var getIsPlayerPlaying = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayer, function (player) { return player.playerState === 1; });
var getShowPlayer = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayer, function (player) { return player.showPlayer; });
var getPlayerFullscreen = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayer, function (player) { return player.fullscreen; });


/***/ }),

/***/ "./resources/assets/src/app/core/store/app-player/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/app-player.reducer.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["g"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["h"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["i"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["j"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["k"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["l"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "r", function() { return __WEBPACK_IMPORTED_MODULE_0__app_player_reducer__["m"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_player_actions__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/app-player.actions.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_player_selectors__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/app-player.selectors.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_2__app_player_selectors__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_2__app_player_selectors__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "o", function() { return __WEBPACK_IMPORTED_MODULE_2__app_player_selectors__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "p", function() { return __WEBPACK_IMPORTED_MODULE_2__app_player_selectors__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "q", function() { return __WEBPACK_IMPORTED_MODULE_2__app_player_selectors__["e"]; });





/***/ }),

/***/ "./resources/assets/src/app/core/store/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export localStorageSyncReducer */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreStoreModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store_devtools__ = __webpack_require__("./node_modules/@ngrx/store-devtools/@ngrx/store-devtools.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngrx_store_localstorage__ = __webpack_require__("./node_modules/ngrx-store-localstorage/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngrx_store_localstorage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngrx_store_localstorage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./resources/assets/src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reducers__ = __webpack_require__("./resources/assets/src/app/core/store/reducers.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';


function localStorageSyncReducer(reducer) {
    return Object(__WEBPACK_IMPORTED_MODULE_3_ngrx_store_localstorage__["localStorageSync"])({
        keys: Object.keys(__WEBPACK_IMPORTED_MODULE_5__reducers__["b" /* EchoesReducers */]),
        rehydrate: true
    })(reducer);
}
var metaReducers = [localStorageSyncReducer];
var optionalImports = [];
if (!__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].production) {
    // Note that you must instrument after importing StoreModule
    optionalImports.push(__WEBPACK_IMPORTED_MODULE_2__ngrx_store_devtools__["a" /* StoreDevtoolsModule */].instrument({ maxAge: 25 }));
}
var CoreStoreModule = (function () {
    function CoreStoreModule() {
    }
    CoreStoreModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["j" /* StoreModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__reducers__["b" /* EchoesReducers */], { metaReducers: metaReducers })
            ].concat(optionalImports),
            declarations: [],
            exports: [],
            providers: __WEBPACK_IMPORTED_MODULE_5__reducers__["a" /* EchoesActions */].slice()
        })
    ], CoreStoreModule);
    return CoreStoreModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/now-playlist/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__now_playlist_reducer__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/now-playlist.reducer.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "t", function() { return __WEBPACK_IMPORTED_MODULE_0__now_playlist_reducer__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/now-playlist.actions.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["g"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["h"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["i"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["j"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["k"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["l"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["m"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["n"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "o", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["o"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "p", function() { return __WEBPACK_IMPORTED_MODULE_1__now_playlist_actions__["p"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__now_playlist_selectors__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/now-playlist.selectors.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "q", function() { return __WEBPACK_IMPORTED_MODULE_2__now_playlist_selectors__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "r", function() { return __WEBPACK_IMPORTED_MODULE_2__now_playlist_selectors__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "s", function() { return __WEBPACK_IMPORTED_MODULE_2__now_playlist_selectors__["c"]; });





/***/ }),

/***/ "./resources/assets/src/app/core/store/now-playlist/now-playlist.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return SeekTo; });
/* unused harmony export QueueLoadVideo */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "p", function() { return UpdateIndexByMedia; });
/* unused harmony export QueueFailed */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return QueueVideo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return QueueVideos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return RemoveVideo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FilterChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return SelectVideo; });
/* unused harmony export PlayPlaylistAction */
/* unused harmony export PlayPlaylistStartAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return LoadPlaylistAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return LoadPlaylistEndAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return MediaEnded; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return SelectNext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return SelectPrevious; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return RemoveAll; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return ToggleRepeat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return PlayerStateChange; });
var ActionTypes;
(function (ActionTypes) {
    ActionTypes["QUEUE_LOAD_VIDEO"] = "[NowPlaylist] QUEUE_LOAD_VIDEO";
    ActionTypes["QUEUE"] = "[NowPlaylist] QUEUE";
    ActionTypes["QUEUE_LOAD_VIDEO_SUCCESS"] = "[NowPlaylist] QUEUE_LOAD_VIDEO_SUCCESS";
    ActionTypes["SELECT"] = "[NowPlaylist] SELECT";
    ActionTypes["REMOVE"] = "[NowPlaylist] REMOVE";
    ActionTypes["UPDATE_INDEX"] = "[NowPlaylist] UPDATE_INDEX";
    ActionTypes["QUEUE_FAILED"] = "[NowPlaylist] QUEUE_FAILED";
    ActionTypes["FILTER_CHANGE"] = "[NowPlaylist] FILTER_CHANGE";
    ActionTypes["REMOVE_ALL"] = "[NowPlaylist] REMOVE_ALL";
    ActionTypes["SELECT_NEXT"] = "[NowPlaylist] SELECT_NEXT";
    ActionTypes["SELECT_PREVIOUS"] = "[NowPlaylist] SELECT_PREVIOUS";
    ActionTypes["QUEUE_VIDEOS"] = "[NowPlaylist] QUEUE_VIDEOS";
    ActionTypes["MEDIA_ENDED"] = "[NowPlaylist] MEDIA_ENDED";
    ActionTypes["TOGGLE_REPEAT"] = "[NowPlaylist] TOGGLE_REPEAT";
    ActionTypes["SELECT_AND_SEEK_TO_TIME"] = "[NowPlaylist] SELECT_AND_SEEK_TO_TIME";
    ActionTypes["LOAD_PLAYLIST_START"] = "[NowPlaylist] LOAD_PLAYLIST_START";
    ActionTypes["LOAD_PLAYLIST_END"] = "[NowPlaylist] LOAD_PLAYLIST_END";
    ActionTypes["PLAY_PLAYLIST"] = "[NowPlaylist] PLAY_PLAYLIST";
    ActionTypes["PLAY_PLAYLIST_START"] = "[NowPlaylist] PLAY_PLAYLIST_START";
    ActionTypes["PLAYER_STATE_CHANGE"] = "[NowPlaylist] PLAYER_STATE_CHANGE";
})(ActionTypes || (ActionTypes = {}));
var SeekTo = (function () {
    function SeekTo(payload) {
        this.payload = payload;
        this.type = ActionTypes.SELECT_AND_SEEK_TO_TIME;
    }
    return SeekTo;
}());

var QueueLoadVideo = (function () {
    function QueueLoadVideo(payload) {
        this.payload = payload;
        this.type = ActionTypes.QUEUE_LOAD_VIDEO;
    }
    return QueueLoadVideo;
}());

var UpdateIndexByMedia = (function () {
    function UpdateIndexByMedia(payload) {
        this.payload = payload;
        this.type = ActionTypes.UPDATE_INDEX;
    }
    return UpdateIndexByMedia;
}());

var QueueFailed = (function () {
    function QueueFailed(payload) {
        this.payload = payload;
        this.type = ActionTypes.QUEUE_FAILED;
    }
    return QueueFailed;
}());

var QueueVideo = (function () {
    function QueueVideo(payload) {
        this.payload = payload;
        this.type = ActionTypes.QUEUE;
    }
    return QueueVideo;
}());

var QueueVideos = (function () {
    function QueueVideos(payload) {
        this.payload = payload;
        this.type = ActionTypes.QUEUE_VIDEOS;
    }
    return QueueVideos;
}());

var RemoveVideo = (function () {
    function RemoveVideo(payload) {
        this.payload = payload;
        this.type = ActionTypes.REMOVE;
    }
    return RemoveVideo;
}());

var FilterChange = (function () {
    function FilterChange(payload) {
        this.payload = payload;
        this.type = ActionTypes.FILTER_CHANGE;
    }
    return FilterChange;
}());

var SelectVideo = (function () {
    function SelectVideo(payload) {
        this.payload = payload;
        this.type = ActionTypes.SELECT;
    }
    return SelectVideo;
}());

var PlayPlaylistAction = (function () {
    function PlayPlaylistAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.PLAY_PLAYLIST;
    }
    return PlayPlaylistAction;
}());

var PlayPlaylistStartAction = (function () {
    function PlayPlaylistStartAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.PLAY_PLAYLIST_START;
    }
    return PlayPlaylistStartAction;
}());

var LoadPlaylistAction = (function () {
    function LoadPlaylistAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.LOAD_PLAYLIST_START;
    }
    return LoadPlaylistAction;
}());

var LoadPlaylistEndAction = (function () {
    function LoadPlaylistEndAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.LOAD_PLAYLIST_END;
    }
    return LoadPlaylistEndAction;
}());

var MediaEnded = (function () {
    function MediaEnded(payload) {
        this.payload = payload;
        this.type = ActionTypes.MEDIA_ENDED;
    }
    return MediaEnded;
}());

var SelectNext = (function () {
    function SelectNext(payload) {
        this.payload = payload;
        this.type = ActionTypes.SELECT_NEXT;
    }
    return SelectNext;
}());

var SelectPrevious = (function () {
    function SelectPrevious(payload) {
        this.payload = payload;
        this.type = ActionTypes.SELECT_PREVIOUS;
    }
    return SelectPrevious;
}());

var RemoveAll = (function () {
    function RemoveAll(payload) {
        this.payload = payload;
        this.type = ActionTypes.REMOVE_ALL;
    }
    return RemoveAll;
}());

var ToggleRepeat = (function () {
    function ToggleRepeat(payload) {
        if (payload === void 0) { payload = ''; }
        this.payload = payload;
        this.type = ActionTypes.TOGGLE_REPEAT;
    }
    return ToggleRepeat;
}());

var PlayerStateChange = (function () {
    function PlayerStateChange(payload) {
        this.payload = payload;
        this.type = ActionTypes.PLAYER_STATE_CHANGE;
    }
    return PlayerStateChange;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/now-playlist/now-playlist.reducer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = nowPlaylist;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/now-playlist.actions.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var initialState = {
    videos: [],
    selectedId: '',
    filter: '',
    repeat: false
};
function nowPlaylist(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].SELECT:
            return __assign({}, state, { selectedId: action.payload.id });
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].QUEUE:
            return __assign({}, state, { videos: addMedia(state.videos, action.payload) });
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].QUEUE_VIDEOS:
            return __assign({}, state, { videos: addMedias(state.videos, action.payload) });
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].REMOVE:
            return __assign({}, state, { videos: removeMedia(state.videos, action.payload) });
        // updates index by media
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].UPDATE_INDEX:
            return __assign({}, state, { selectedId: action.payload });
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].FILTER_CHANGE:
            return __assign({}, state, { filter: action.payload });
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].REMOVE_ALL:
            return __assign({}, state, { videos: [], filter: '', selectedId: '' });
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].SELECT_NEXT: {
            return __assign({}, state, { selectedId: selectNextIndex(state.videos, state.selectedId, state.filter, state.repeat) });
        }
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].SELECT_PREVIOUS:
            return __assign({}, state, { selectedId: selectPreviousIndex(state.videos, state.selectedId, state.filter) });
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].MEDIA_ENDED:
            return selectNextOrPreviousTrack(state, state.filter);
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].TOGGLE_REPEAT: {
            return __assign({}, state, { repeat: !state.repeat });
        }
        case __WEBPACK_IMPORTED_MODULE_0__now_playlist_actions__["a" /* ActionTypes */].LOAD_PLAYLIST_END: {
            return __assign({}, state);
        }
        default:
            return state;
    }
}
function addMedia(videos, media) {
    var newMedia = videos.findIndex(function (video) { return video.id === media.id; });
    var newMedias = [];
    if (newMedia === -1) {
        newMedias.push(media);
    }
    return videos.concat(newMedias);
}
function addMedias(videos, medias) {
    var allVideoIds = videos.map(function (video) { return video.id; });
    var newVideos = medias.filter(function (media) { return !allVideoIds.includes(media.id); });
    return videos.concat(newVideos);
}
function filterVideos(videos, filter) {
    var sanitizedFilter = filter.toLowerCase();
    return videos.filter(function (video) {
        return JSON.stringify(video)
            .toLowerCase()
            .includes(sanitizedFilter);
    });
}
function getSelectedInFilteredVideos(videos, filter, selectedId) {
    var filteredVideos = filterVideos(videos, filter);
    var currentIndex = filteredVideos.findIndex(function (video) { return video.id === selectedId; });
    return {
        filteredVideos: filteredVideos,
        currentIndex: currentIndex
    };
}
function selectNextIndex(videos, selectedId, filter, isRepeat) {
    var _a = getSelectedInFilteredVideos(videos, filter, selectedId), filteredVideos = _a.filteredVideos, currentIndex = _a.currentIndex;
    var nextIndex = currentIndex + 1;
    if (!filteredVideos.length) {
        nextIndex = 0;
    }
    if (filteredVideos.length === nextIndex) {
        nextIndex = isRepeat ? 0 : currentIndex;
    }
    return filteredVideos[nextIndex].id || '';
}
function selectPreviousIndex(videos, selectedId, filter) {
    var _a = getSelectedInFilteredVideos(videos, filter, selectedId), filteredVideos = _a.filteredVideos, currentIndex = _a.currentIndex;
    var previousIndex = currentIndex - 1;
    if (!filteredVideos.length || previousIndex < 0) {
        previousIndex = 0;
    }
    return filteredVideos[previousIndex].id || '';
}
function selectNextOrPreviousTrack(state, filter) {
    var videos = state.videos, selectedId = state.selectedId, repeat = state.repeat;
    var _a = getSelectedInFilteredVideos(videos, filter, selectedId), filteredVideos = _a.filteredVideos, currentIndex = _a.currentIndex;
    var isCurrentLast = currentIndex + 1 === filteredVideos.length;
    var nextId = isCurrentLast
        ? getNextIdForPlaylist(filteredVideos, repeat, selectedId)
        : selectNextIndex(filteredVideos, selectedId, filter, repeat);
    return __assign({}, state, { selectedId: nextId });
}
function getNextIdForPlaylist(videos, repeat, currentId) {
    if (currentId === void 0) { currentId = ''; }
    var id = '';
    if (videos.length && repeat) {
        id = videos[0].id;
    }
    return id;
}
function removeMedia(videos, media) {
    return videos.filter(function (_media) { return _media.id !== media.id; });
}


/***/ }),

/***/ "./resources/assets/src/app/core/store/now-playlist/now-playlist.selectors.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getNowPlaylist; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return isPlayerInRepeat; });
/* unused harmony export getPlaylistVideos */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getPlaylistMediaIds; });
/* unused harmony export getSelectedMediaId */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getSelectedMedia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");

var getNowPlaylist = function (state) { return state.nowPlaylist; };
var isPlayerInRepeat = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getNowPlaylist, function (nowPlaylist) { return nowPlaylist.repeat; });
var getPlaylistVideos = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getNowPlaylist, function (nowPlaylist) { return nowPlaylist.videos; });
var getPlaylistMediaIds = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlaylistVideos, function (playlist) { return playlist.map(function (media) { return media.id; }); });
var getSelectedMediaId = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getNowPlaylist, function (nowPlaylist) { return nowPlaylist.selectedId; });
var getSelectedMedia = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getNowPlaylist, getSelectedMediaId, function (nowPlaylist, selectedId) {
    var mediaIds = nowPlaylist.videos.map(function (video) { return video.id; });
    var selectedMediaIndex = mediaIds.indexOf(selectedId);
    return nowPlaylist.videos[selectedMediaIndex];
});


/***/ }),

/***/ "./resources/assets/src/app/core/store/player-search/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__player_search_reducer__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/player-search.reducer.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__player_search_reducer__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_0__player_search_reducer__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__player_search_actions__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/player-search.actions.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__player_search_actions__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__player_search_actions__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__player_search_actions__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_1__player_search_actions__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_1__player_search_actions__["e"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__player_search_selectors__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/player-search.selectors.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_2__player_search_selectors__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_2__player_search_selectors__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_2__player_search_selectors__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_2__player_search_selectors__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_2__player_search_selectors__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_2__player_search_selectors__["f"]; });





/***/ }),

/***/ "./resources/assets/src/app/core/store/player-search/player-search.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PlayerSearchActions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return UpdateQueryAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return UpdateSearchType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SearchCurrentQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddResultsAction; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__ = __webpack_require__("./node_modules/ngrx-action-creator-factory/ngrx-action-creator-factory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PlayerSearchActions = (function () {
    function PlayerSearchActions() {
        this.getSuggestions = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.GET_SUGGESTIONS);
        this.searchNewQuery = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.SEARCH_NEW_QUERY);
        this.searchMoreForQuery = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.SEARCH_MORE_FOR_QUERY);
        this.updateFilter = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.UPDATE_FILTER);
        this.updateQueryParam = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.UPDATE_QUERY_PARAM);
        this.resetPageToken = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.RESET_PAGE_TOKEN);
        this.searchResultsReturned = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.SEARCH_RESULTS_RETURNED);
        this.searchStarted = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.SEARCH_STARTED);
        // addResults = ActionCreatorFactory.create(PlayerSearchActions.ADD_RESULTS);
        this.resetResults = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.RESET_RESULTS);
        this.errorInSearch = __WEBPACK_IMPORTED_MODULE_1_ngrx_action_creator_factory__["ActionCreatorFactory"].create(PlayerSearchActions_1.ERROR_RESULTS);
    }
    PlayerSearchActions_1 = PlayerSearchActions;
    // @ActionCreator({
    //   type: 'UPDATE_FILTER',
    //   payload: string
    // })
    // @ActionCreator<string>(PlayerSearchActions.UPDATE_FILTER)
    // update;
    PlayerSearchActions.UPDATE_FILTER = '[PlayerSearch] UPDATE_FILTER';
    PlayerSearchActions.UPDATE_QUERY_PARAM = '[PlayerSearch] UPDATE_QUERY_PARAM';
    PlayerSearchActions.UPDATE_QUERY = '[PlayerSearch] UPDATE_QUERY';
    PlayerSearchActions.SEARCH_NEW_QUERY = '[PlayerSearch] SEARCH_NEW_QUERY';
    PlayerSearchActions.SEARCH_MORE_FOR_QUERY = '[PlayerSearch] SEARCH_MORE_FOR_QUERY';
    PlayerSearchActions.GET_SUGGESTIONS = '[PlayerSearch] GET_SUGGESTIONS';
    PlayerSearchActions.RESET_PAGE_TOKEN = '[PlayerSearch] RESET_PAGE_TOKEN';
    PlayerSearchActions.SEARCH_RESULTS_RETURNED = '[PlayerSearch] SERACH_RESULTS_RETURNED';
    PlayerSearchActions.SEARCH_CURRENT_QUERY = '[PlayerSearch] SEARCH_CURRENT_QUERY';
    PlayerSearchActions.SEARCH_STARTED = '[PlayerSearch] SEARCH_STARTED';
    PlayerSearchActions.SEARCH_TYPE_UPDATE = '[PlayerSearch] SEARCH_TYPE_UPDATE';
    PlayerSearchActions.ADD_PLAYLISTS_TO_RESULTS = {
        action: '[PlayerSearch] ADD_PLAYLISTS_TO_RESULTS',
        creator: function (payload) { return ({
            payload: payload,
            type: PlayerSearchActions_1.ADD_PLAYLISTS_TO_RESULTS.action
        }); }
    };
    PlayerSearchActions.ADD_METADATA_TO_VIDEOS = {
        action: '[PlayerSearch] ADD_METADATA_TO_VIDEOS',
        creator: function (payload) { return ({
            payload: payload,
            type: PlayerSearchActions_1.ADD_METADATA_TO_VIDEOS.action
        }); }
    };
    PlayerSearchActions.PLAYLISTS_SEARCH_START = {
        action: '[PlayerSearch] PLAYLISTS_SEARCH_START',
        creator: function () { return ({ type: PlayerSearchActions_1.PLAYLISTS_SEARCH_START.action }); }
    };
    // Results Actions
    PlayerSearchActions.ADD_RESULTS = '[PlayerSearch] ADD_RESULTS';
    PlayerSearchActions.RESET_RESULTS = '[PlayerSearch] RESET_RESULTS';
    PlayerSearchActions.ERROR_RESULTS = '[PlayerSearch] ERROR_RESULTS';
    PlayerSearchActions = PlayerSearchActions_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], PlayerSearchActions);
    return PlayerSearchActions;
    var PlayerSearchActions_1;
}());

var UpdateQueryAction = (function () {
    function UpdateQueryAction(payload) {
        this.payload = payload;
        this.type = PlayerSearchActions.UPDATE_QUERY;
    }
    return UpdateQueryAction;
}());

var UpdateSearchType = (function () {
    function UpdateSearchType(payload) {
        this.payload = payload;
        this.type = PlayerSearchActions.SEARCH_TYPE_UPDATE;
    }
    return UpdateSearchType;
}());

var SearchCurrentQuery = (function () {
    function SearchCurrentQuery() {
        this.type = PlayerSearchActions.SEARCH_CURRENT_QUERY;
    }
    return SearchCurrentQuery;
}());

// export class AddResults implements Action {
//   readonly type = PlayerSearchActions.ADD_RESULTS;
//   constructor(public payload: any[]) { }
// }
var AddResultsAction = {
    type: PlayerSearchActions.ADD_RESULTS,
    creator: function (payload) {
        return { payload: payload, type: this.type };
    },
    handler: function (state, payload) {
        return __assign({}, state, { results: state.results.concat(payload), isSearching: false });
    }
};


/***/ }),

/***/ "./resources/assets/src/app/core/store/player-search/player-search.interfaces.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CSearchTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CPresetTypes; });
var CSearchTypes = (function () {
    function CSearchTypes() {
    }
    CSearchTypes.VIDEO = 'video';
    CSearchTypes.PLAYLIST = 'playlist';
    return CSearchTypes;
}());

var CPresetTypes = (function () {
    function CPresetTypes() {
    }
    CPresetTypes.FULL_ALBUMS = 'full albums';
    CPresetTypes.LIVE = 'live';
    return CPresetTypes;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/player-search/player-search.reducer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["b"] = search;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__player_search_actions__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/player-search.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__player_search_interfaces__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/player-search.interfaces.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__player_search_interfaces__["b"]; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



var initialState = {
    query: '',
    filter: '',
    searchType: __WEBPACK_IMPORTED_MODULE_1__player_search_interfaces__["b" /* CSearchTypes */].VIDEO,
    queryParams: {
        preset: '',
        duration: -1
    },
    presets: [
        { label: 'Any', value: '' },
        { label: 'Albums', value: __WEBPACK_IMPORTED_MODULE_1__player_search_interfaces__["a" /* CPresetTypes */].FULL_ALBUMS },
        { label: 'Live', value: __WEBPACK_IMPORTED_MODULE_1__player_search_interfaces__["a" /* CPresetTypes */].LIVE }
    ],
    pageToken: {
        next: '',
        prev: ''
    },
    isSearching: false,
    results: []
};
function search(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].UPDATE_QUERY: {
            return __assign({}, state, { query: action.payload });
        }
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].SEARCH_NEW_QUERY:
            return __assign({}, state, { query: action.payload, isSearching: true });
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].UPDATE_QUERY_PARAM:
            var queryParams = __assign({}, state.queryParams, action.payload);
            return __assign({}, state, { queryParams: queryParams });
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].SEARCH_RESULTS_RETURNED:
            var _a = action.payload, nextPageToken = _a.nextPageToken, prevPageToken = _a.prevPageToken;
            var statePageToken = state.pageToken;
            var pageToken = {
                next: nextPageToken || statePageToken.next,
                prev: prevPageToken || statePageToken.prev
            };
            return __assign({}, state, { pageToken: pageToken });
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].SEARCH_STARTED:
            return __assign({}, state, { isSearching: true });
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["a" /* AddResultsAction */].type:
            return __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["a" /* AddResultsAction */].handler(state, action.payload);
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].RESET_RESULTS:
            return __assign({}, state, { results: [] });
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].SEARCH_TYPE_UPDATE: {
            return __assign({}, state, { searchType: action.payload });
        }
        case __WEBPACK_IMPORTED_MODULE_0__player_search_actions__["b" /* PlayerSearchActions */].PLAYLISTS_SEARCH_START.action: {
            return __assign({}, state, { isSearching: true });
        }
        default:
            // upgrade policy - for when the initialState has changed
            return __assign({}, initialState, state);
    }
}


/***/ }),

/***/ "./resources/assets/src/app/core/store/player-search/player-search.selectors.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export getPlayerSearch */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getPlayerSearchResults; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getQuery; });
/* unused harmony export getQueryParams */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return getQueryParamPreset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return getSearchType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getIsSearching; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getPresets; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");

var getPlayerSearch = function (state) { return state.search; };
var getPlayerSearchResults = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayerSearch, function (search) { return search.results; });
var getQuery = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayerSearch, function (search) { return search.query; });
var getQueryParams = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayerSearch, function (search) { return search.queryParams; });
var getQueryParamPreset = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayerSearch, getQueryParams, function (search, queryParams) { return queryParams.preset; });
var getSearchType = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayerSearch, function (search) { return search.searchType; });
var getIsSearching = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayerSearch, function (search) { return search.isSearching; });
var getPresets = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getPlayerSearch, function (search) { return search.presets; });


/***/ }),

/***/ "./resources/assets/src/app/core/store/reducers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return EchoesReducers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EchoesActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_player__ = __webpack_require__("./resources/assets/src/app/core/store/app-player/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__now_playlist__ = __webpack_require__("./resources/assets/src/app/core/store/now-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_profile__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__player_search__ = __webpack_require__("./resources/assets/src/app/core/store/player-search/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_layout__ = __webpack_require__("./resources/assets/src/app/core/store/app-layout/index.ts");
// import { routerReducer, RouterReducerState } from '@ngrx/router-store';
// reducers





var EchoesReducers = {
    player: __WEBPACK_IMPORTED_MODULE_0__app_player__["r" /* player */],
    nowPlaylist: __WEBPACK_IMPORTED_MODULE_1__now_playlist__["t" /* nowPlaylist */],
    user: __WEBPACK_IMPORTED_MODULE_2__user_profile__["j" /* user */],
    search: __WEBPACK_IMPORTED_MODULE_3__player_search__["m" /* search */],
    appLayout: __WEBPACK_IMPORTED_MODULE_4__app_layout__["g" /* appLayout */]
    // routerReducer
};
var EchoesActions = [
    __WEBPACK_IMPORTED_MODULE_0__app_player__["a" /* ActionTypes */],
    __WEBPACK_IMPORTED_MODULE_2__user_profile__["b" /* UserProfileActions */],
    __WEBPACK_IMPORTED_MODULE_3__player_search__["c" /* PlayerSearchActions */]
];


/***/ }),

/***/ "./resources/assets/src/app/core/store/router-store/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__router_store_actions__ = __webpack_require__("./resources/assets/src/app/core/store/router-store/router-store.actions.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__router_store_actions__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__router_store_actions__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__router_store_actions__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__router_store_actions__["d"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__router_store_reducer__ = __webpack_require__("./resources/assets/src/app/core/store/router-store/router-store.reducer.ts");
/* unused harmony namespace reexport */




/***/ }),

/***/ "./resources/assets/src/app/core/store/router-store/router-store.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return GO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BACK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return FORWARD; });
/* unused harmony export Go */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Back; });
/* unused harmony export Forward */
var GO = '[Router] Go';
var BACK = '[Router] Back';
var FORWARD = '[Router] Forward';
var Go = (function () {
    function Go(payload) {
        this.payload = payload;
        this.type = GO;
    }
    return Go;
}());

var Back = (function () {
    function Back() {
        this.type = BACK;
    }
    return Back;
}());

var Forward = (function () {
    function Forward() {
        this.type = FORWARD;
    }
    return Forward;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/router-store/router-store.reducer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export NavigationSerializer */
var NavigationSerializer = (function () {
    function NavigationSerializer() {
    }
    NavigationSerializer.prototype.serialize = function (routerState) {
        // console.log('ROUTE', routerState);
        var url = routerState.url;
        var queryParams = routerState.root.queryParams;
        return { url: url, queryParams: queryParams };
    };
    return NavigationSerializer;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/user-profile/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/user-profile.reducer.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["g"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_reducer__["h"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_profile_actions__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/user-profile.actions.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_profile_selectors__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/user-profile.selectors.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_2__user_profile_selectors__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_2__user_profile_selectors__["d"]; });





/***/ }),

/***/ "./resources/assets/src/app/core/store/user-profile/user-profile.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UserProfileActions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UserSignin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return UserSigninStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return UserSigninSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return UserSignout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return UserSignoutSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckUserAuth; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ngrx_action_creator_factory__ = __webpack_require__("./node_modules/ngrx-action-creator-factory/ngrx-action-creator-factory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ngrx_action_creator_factory___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ngrx_action_creator_factory__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var UserProfileActions = (function () {
    function UserProfileActions() {
        this.setViewPlaylist = __WEBPACK_IMPORTED_MODULE_0_ngrx_action_creator_factory__["ActionCreatorFactory"].create(UserProfileActions_1.VIEWED_PLAYLIST);
        this.updateData = function (data) { return ({ type: UserProfileActions_1.UPDATE, payload: data }); };
        this.updateToken = function (payload) { return ({ type: UserProfileActions_1.UPDATE_TOKEN, payload: payload }); };
    }
    UserProfileActions_1 = UserProfileActions;
    UserProfileActions.prototype.addPlaylists = function (playlists) {
        return {
            type: UserProfileActions_1.ADD_PLAYLISTS,
            payload: playlists
        };
    };
    UserProfileActions.prototype.updatePageToken = function (token) {
        return {
            type: UserProfileActions_1.UPDATE_NEXT_PAGE_TOKEN,
            payload: token
        };
    };
    UserProfileActions.prototype.userProfileCompleted = function () {
        return {
            type: UserProfileActions_1.USER_PROFILE_COMPLETED
        };
    };
    UserProfileActions.prototype.userProfileRecieved = function (profile) {
        return {
            type: UserProfileActions_1.USER_PROFILE_RECIEVED,
            payload: profile
        };
    };
    UserProfileActions.prototype.updateUserProfile = function (profile) {
        return {
            type: UserProfileActions_1.UPDATE_USER_PROFILE,
            payload: profile
        };
    };
    UserProfileActions.UPDATE = '[UserProfile] UPDATE';
    UserProfileActions.ADD_PLAYLISTS = '[UserProfile] ADD_PLAYLISTS';
    UserProfileActions.UPDATE_TOKEN = '[UserProfile] UPDATE_TOKEN';
    UserProfileActions.UPDATE_NEXT_PAGE_TOKEN = '[UserProfile] UPDATE_NEXT_PAGE_TOKEN';
    UserProfileActions.USER_PROFILE_COMPLETED = '[UserProfile] USER_PROFILE_COMPLETED';
    UserProfileActions.UPDATE_USER_PROFILE = '[UserProfile] UPDATE_USER_PROFILE';
    UserProfileActions.USER_PROFILE_RECIEVED = '[UserProfile] USER_PROFILE_RECIEVED';
    UserProfileActions.VIEWED_PLAYLIST = '[UserProfile] VIEWED_PLAYLIST';
    UserProfileActions.CHECK_USER_AUTH = '[UserProfile] CHECK_USER_AUTH';
    UserProfileActions.USER_SIGNIN = '[UserProfile] USER_SIGNIN';
    UserProfileActions.USER_SIGNIN_START = '[UserProfile] USER_SIGNIN_START';
    UserProfileActions.USER_SIGNIN_SUCCESS = '[UserProfile] USER_SIGNIN_SUCCESS';
    UserProfileActions.USER_SIGNOUT = '[UserProfile] USER_SIGNOUT';
    UserProfileActions.USER_SIGNOUT_SUCCESS = '[UserProfile] USER_SIGNOUT_SUCCESS';
    UserProfileActions = UserProfileActions_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])()
    ], UserProfileActions);
    return UserProfileActions;
    var UserProfileActions_1;
}());

var UserSignin = (function () {
    function UserSignin() {
        this.type = UserProfileActions.USER_SIGNIN;
    }
    return UserSignin;
}());

var UserSigninStart = (function () {
    function UserSigninStart() {
        this.type = UserProfileActions.USER_SIGNIN_START;
    }
    return UserSigninStart;
}());

var UserSigninSuccess = (function () {
    function UserSigninSuccess(payload) {
        this.payload = payload;
        this.type = UserProfileActions.USER_SIGNIN_SUCCESS;
    }
    return UserSigninSuccess;
}());

var UserSignout = (function () {
    function UserSignout() {
        this.type = UserProfileActions.USER_SIGNOUT;
    }
    return UserSignout;
}());

var UserSignoutSuccess = (function () {
    function UserSignoutSuccess() {
        this.type = UserProfileActions.USER_SIGNOUT_SUCCESS;
    }
    return UserSignoutSuccess;
}());

var CheckUserAuth = (function () {
    function CheckUserAuth() {
        this.type = UserProfileActions.CHECK_USER_AUTH;
    }
    return CheckUserAuth;
}());



/***/ }),

/***/ "./resources/assets/src/app/core/store/user-profile/user-profile.reducer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["h"] = user;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__ = __webpack_require__("./resources/assets/src/app/core/store/user-profile/user-profile.actions.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["g"]; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};


var initialUserState = {
    access_token: '',
    playlists: [],
    data: {},
    nextPageToken: '',
    profile: {},
    viewedPlaylist: ''
};
function user(state, action) {
    if (state === void 0) { state = initialUserState; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b" /* UserProfileActions */].ADD_PLAYLISTS:
            return __assign({}, state, { playlists: state.playlists.concat(action.payload) });
        case __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b" /* UserProfileActions */].UPDATE_TOKEN:
            return __assign({}, state, { access_token: action.payload, playlists: [] });
        case __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b" /* UserProfileActions */].USER_SIGNOUT_SUCCESS:
            return __assign({}, initialUserState);
        case __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b" /* UserProfileActions */].UPDATE:
            return __assign({}, state, { data: action.payload });
        case __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b" /* UserProfileActions */].UPDATE_NEXT_PAGE_TOKEN:
            return __assign({}, state, { nextPageToken: action.payload });
        case __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b" /* UserProfileActions */].UPDATE_USER_PROFILE:
            return __assign({}, state, { profile: action.payload });
        case __WEBPACK_IMPORTED_MODULE_0__user_profile_actions__["b" /* UserProfileActions */].VIEWED_PLAYLIST:
            return __assign({}, state, { viewedPlaylist: action.payload });
        default:
            return state;
    }
}


/***/ }),

/***/ "./resources/assets/src/app/core/store/user-profile/user-profile.selectors.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getUserPlaylists; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getUserViewPlaylist; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getIsUserSignedIn; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");

var getUser = function (state) { return state.user; };
var getUserPlaylists = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getUser, function (user) { return user.playlists; });
var getUserViewPlaylist = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getUser, function (user) { return user.viewedPlaylist; });
var getIsUserSignedIn = Object(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["n" /* createSelector */])(getUser, function (user) { return user.access_token !== ''; });


/***/ }),

/***/ "./resources/assets/src/app/shared/animations/fade-in.animation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fadeInAnimation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return flyOut; });
/* unused harmony export flyInOut */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return expandFadeInAnimation; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_animations__ = __webpack_require__("./node_modules/@angular/animations/esm5/animations.js");

var fadeInAnimation = Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["j" /* trigger */])('fadeIn', [
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["g" /* state */])('void', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({ opacity: 0, transform: 'translateY(-2rem)' })),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('void => *', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('300ms ease-in', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({
            opacity: 1,
            transform: 'translateY(0rem)'
        }))
    ]),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('* => void', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('300ms ease-out', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({
            opacity: 0,
            transform: 'translateY(-2rem)'
        }))
    ])
]);
var flyOut = Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["j" /* trigger */])('flyOut', [
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["g" /* state */])('void', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({ opacity: 0, transform: 'translateY(-30%)' })),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('void => *', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('300ms ease-out', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({
            opacity: 1,
            transform: 'translateY(0%)'
        }))
    ]),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('* => void', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('300ms ease-out', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({
            opacity: 0,
            transform: 'translateX(-80%)'
        }))
    ])
]);
var flyInOut = Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["j" /* trigger */])('flyInOut', [
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["g" /* state */])('in', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({ transform: 'translateX(0)' })),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('void => *', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({ transform: 'translateX(-100%)' }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])(100)
    ]),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('* => void', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])(100, Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({ transform: 'translateX(100%)' }))
    ])
]);
var expandFadeInAnimation = Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["j" /* trigger */])('expandFadeIn', [
    // state('void', style({ top: '-35rem' })),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["g" /* state */])('show', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({
        opacity: 1,
        transform: 'scale(1)'
    })),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["g" /* state */])('hide', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* style */])({
        opacity: 0,
        transform: 'scale(0.4)',
        top: -2
    })),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('show => hide', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('300ms ease-in')),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* transition */])('hide => show', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('300ms ease-in'))
]);


/***/ }),

/***/ "./resources/assets/src/app/shared/components/btn/btn.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_shared_utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ButtonDirective = (function () {
    function ButtonDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
        this.btn = '';
        this.mainStyle = 'btn';
        this.stylePrefix = 'btn-';
    }
    ButtonDirective.prototype.ngOnInit = function () {
        this.addClass(this.mainStyle);
    };
    ButtonDirective.prototype.ngOnChanges = function (_a) {
        var btn = _a.btn;
        if (btn && Object(__WEBPACK_IMPORTED_MODULE_1_app_shared_utils_data_utils__["a" /* isNewChange */])(btn)) {
            this.applyStyles();
        }
    };
    ButtonDirective.prototype.addClass = function (className) {
        this.renderer.addClass(this.element.nativeElement, className);
    };
    ButtonDirective.prototype.applyStyles = function () {
        var _this = this;
        var prefix = this.stylePrefix;
        var styles = this.btn.split(' ').map(function (style) { return "" + prefix + style; })
            .forEach(function (className) { return _this.addClass(className); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ButtonDirective.prototype, "btn", void 0);
    ButtonDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[btn]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"]])
    ], ButtonDirective);
    return ButtonDirective;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/btn/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__btn_directive__ = __webpack_require__("./resources/assets/src/app/shared/components/btn/btn.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__btn_directive__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/components/button-group/button-group.component.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  display: inline-block; }\n  :host .btn {\n    border: 0; }\n  :host .btn.btn-default.active {\n      background-color: var(--brand-primary);\n      color: var(--brand-inverse-text); }\n  :host .btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {\n    border-top-left-radius: 3px;\n    border-bottom-left-radius: 3px; }\n  :host .btn-group > .btn:last-child:not(:first-child) {\n    border-top-right-radius: 3px;\n    border-bottom-right-radius: 3px; }\n"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/button-group/button-group.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonGroupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ButtonGroupComponent = (function () {
    function ButtonGroupComponent() {
        this.buttonClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ButtonGroupComponent.prototype.ngOnInit = function () { };
    ButtonGroupComponent.prototype.isSelectedButton = function (buttonValue) {
        return buttonValue === this.selectedButton;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], ButtonGroupComponent.prototype, "buttons", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], ButtonGroupComponent.prototype, "selectedButton", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], ButtonGroupComponent.prototype, "buttonClick", void 0);
    ButtonGroupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'button-group',
            styles: [__webpack_require__("./resources/assets/src/app/shared/components/button-group/button-group.component.scss")],
            template: "\n    <div class=\"btn-group btn-group-sm navbar-btn\">\n      <button class=\"btn btn-default\"\n        *ngFor=\"let button of buttons\"\n        [class.active]=\"isSelectedButton(button.value)\"\n        (click)=\"buttonClick.next(button)\">\n        {{ button.label }}\n      </button>\n    </div>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        })
    ], ButtonGroupComponent);
    return ButtonGroupComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/button-group/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__button_group_component__ = __webpack_require__("./resources/assets/src/app/shared/components/button-group/button-group.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__button_group_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/components/button-icon/button-icon.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonIconComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ButtonIconComponent = (function () {
    function ButtonIconComponent() {
    }
    ButtonIconComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], ButtonIconComponent.prototype, "icon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], ButtonIconComponent.prototype, "types", void 0);
    ButtonIconComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'button-icon',
            // styleUrls: ['./button-icon.scss'],
            template: "\n    <button [ngClass]=\"types\">\n      <icon [name]=\"icon\"></icon> <ng-content></ng-content>\n    </button>\n  ",
        }),
        __metadata("design:paramtypes", [])
    ], ButtonIconComponent);
    return ButtonIconComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/button-icon/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__button_icon_component__ = __webpack_require__("./resources/assets/src/app/shared/components/button-icon/button-icon.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__button_icon_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/components/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CORE_COMPONENTS; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__youtube_list__ = __webpack_require__("./resources/assets/src/app/shared/components/youtube-list/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__youtube_media__ = __webpack_require__("./resources/assets/src/app/shared/components/youtube-media/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__youtube_playlist__ = __webpack_require__("./resources/assets/src/app/shared/components/youtube-playlist/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__button_group__ = __webpack_require__("./resources/assets/src/app/shared/components/button-group/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__playlist_viewer__ = __webpack_require__("./resources/assets/src/app/shared/components/playlist-viewer/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_indicator__ = __webpack_require__("./resources/assets/src/app/shared/components/loading-indicator/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__btn__ = __webpack_require__("./resources/assets/src/app/shared/components/btn/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__button_icon__ = __webpack_require__("./resources/assets/src/app/shared/components/button-icon/index.ts");








var CORE_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_0__youtube_list__["a" /* YoutubeListComponent */],
    __WEBPACK_IMPORTED_MODULE_1__youtube_media__["a" /* YoutubeMediaComponent */],
    __WEBPACK_IMPORTED_MODULE_2__youtube_playlist__["a" /* YoutubePlaylistComponent */],
    __WEBPACK_IMPORTED_MODULE_3__button_group__["a" /* ButtonGroupComponent */],
    __WEBPACK_IMPORTED_MODULE_4__playlist_viewer__["b" /* PlaylistViewerComponent */], __WEBPACK_IMPORTED_MODULE_4__playlist_viewer__["a" /* PlaylistCoverComponent */],
    __WEBPACK_IMPORTED_MODULE_5__loading_indicator__["a" /* LoadingIndicatorComponent */],
    __WEBPACK_IMPORTED_MODULE_6__btn__["a" /* ButtonDirective */],
    __WEBPACK_IMPORTED_MODULE_7__button_icon__["a" /* ButtonIconComponent */]
];


/***/ }),

/***/ "./resources/assets/src/app/shared/components/loading-indicator/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__loading_indicator_component__ = __webpack_require__("./resources/assets/src/app/shared/components/loading-indicator/loading-indicator.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__loading_indicator_component__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/components/loading-indicator/loading-indicator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingIndicatorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadingIndicatorComponent = (function () {
    function LoadingIndicatorComponent() {
        this.message = '';
        this.loading = false;
    }
    Object.defineProperty(LoadingIndicatorComponent.prototype, "show", {
        get: function () {
            return this.loading;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], LoadingIndicatorComponent.prototype, "message", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], LoadingIndicatorComponent.prototype, "loading", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.show-loader'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], LoadingIndicatorComponent.prototype, "show", null);
    LoadingIndicatorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'loader',
            styles: [__webpack_require__("./resources/assets/src/app/shared/components/loading-indicator/loading-indicator.scss")],
            template: "\n  <div class=\"alert alert-info\">\n    <icon name=\"circle-o-notch spin\"></icon> {{ message }}\n  </div>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        })
    ], LoadingIndicatorComponent);
    return LoadingIndicatorComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/loading-indicator/loading-indicator.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block;\n  margin: 2rem;\n  position: fixed;\n  z-index: 1000;\n  min-width: 40%;\n  -webkit-transition: -webkit-transform 0.5s cubic-bezier(0.68, -0.55, 0.27, 1.55);\n  transition: -webkit-transform 0.5s cubic-bezier(0.68, -0.55, 0.27, 1.55);\n  transition: transform 0.5s cubic-bezier(0.68, -0.55, 0.27, 1.55);\n  transition: transform 0.5s cubic-bezier(0.68, -0.55, 0.27, 1.55), -webkit-transform 0.5s cubic-bezier(0.68, -0.55, 0.27, 1.55);\n  -webkit-transform: translatey(-20rem);\n          transform: translatey(-20rem); }\n  :host.show-loader {\n    -webkit-transform: translatey(0rem);\n            transform: translatey(0rem); }\n  :host .alert {\n    background-color: var(--sidebar-bg);\n    -webkit-box-shadow: 0 0 20px -3px #000;\n            box-shadow: 0 0 20px -3px #000;\n    padding: 2rem; }\n  :host .alert.alert-info {\n      color: var(--brand-primary); }\n"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/playlist-viewer/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__playlist_viewer_component__ = __webpack_require__("./resources/assets/src/app/shared/components/playlist-viewer/playlist-viewer.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__playlist_viewer_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__playlist_cover_component__ = __webpack_require__("./resources/assets/src/app/shared/components/playlist-viewer/playlist-cover.component.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__playlist_cover_component__["a"]; });




/***/ }),

/***/ "./resources/assets/src/app/shared/components/playlist-viewer/playlist-cover.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistCoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlaylistCoverComponent = (function () {
    function PlaylistCoverComponent() {
        this.play = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.queue = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    PlaylistCoverComponent.prototype.ngOnInit = function () { };
    Object.defineProperty(PlaylistCoverComponent.prototype, "title", {
        get: function () {
            return this.playlist && this.playlist.snippet
                ? this.playlist.snippet.title
                : '...';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PlaylistCoverComponent.prototype, "total", {
        get: function () {
            return this.playlist && this.playlist.contentDetails
                ? this.playlist.contentDetails.itemCount
                : '...';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PlaylistCoverComponent.prototype, "thumbUrl", {
        get: function () {
            var thumbnails = this.playlist && this.playlist.snippet.thumbnails;
            var sizes = ['default', 'medium'];
            return sizes.reduce(function (acc, size) { return thumbnails.hasOwnProperty(size) && thumbnails[size].url; }, '');
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PlaylistCoverComponent.prototype, "playlist", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlaylistCoverComponent.prototype, "play", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlaylistCoverComponent.prototype, "queue", void 0);
    PlaylistCoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'playlist-cover',
            styles: [__webpack_require__("./resources/assets/src/app/shared/components/playlist-viewer/playlist-cover.scss")],
            template: "\n  <div class=\"playlist-cover clearfix\">\n    <div class=\"cover-bg\" [ngStyle]=\"{ 'background-image': 'url(' + thumbUrl + ')' }\"></div>\n    <div class=\"btn btn-transparent playlist-thumbnail\">\n      <img [src]=\"thumbUrl\">\n    </div>\n    <div class=\"actions\">\n      <button class=\"btn btn-lg ux-maker play-media bg-primary\"\n        (click)=\"play.emit(playlist)\" title=\"play playlist\">\n        <icon name=\"play\"></icon>\n      </button>\n      <button class=\"btn btn-lg ux-maker play-media bg-primary\"\n        (click)=\"queue.emit(playlist)\" title=\"queue playlist\">\n        <icon name=\"share\"></icon>\n      </button>\n    </div>\n  </div>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], PlaylistCoverComponent);
    return PlaylistCoverComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/playlist-viewer/playlist-cover.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block; }\n  :host .playlist-cover {\n    position: relative;\n    padding: 9rem 0;\n    margin-bottom: 1rem;\n    overflow: hidden; }\n  :host .playlist-cover .cover-bg {\n      background-size: cover;\n      background-repeat: no-repeat;\n      background-position: center;\n      -webkit-filter: blur(5px);\n              filter: blur(5px);\n      -webkit-transform: scale(1.2 1.2);\n              transform: scale(1.2 1.2);\n      height: 24rem;\n      position: absolute;\n      top: 0;\n      left: 0;\n      width: 100%; }\n  :host .playlist-cover .playlist-thumbnail {\n      position: absolute;\n      top: 56px; }\n  :host .playlist-cover .playlist-thumbnail img {\n        border: 1px solid rgba(255, 255, 255, 0.5); }\n  :host .playlist-cover h4 {\n      margin: 0;\n      line-height: 5rem; }\n  :host .playlist-cover h4 span {\n        font-weight: normal;\n        position: relative;\n        background-color: rgba(255, 255, 255, 0.5);\n        padding: 1rem; }\n  :host .playlist-cover .actions {\n      -webkit-transform: translateX(11rem) translateY(9rem);\n              transform: translateX(11rem) translateY(9rem); }\n  :host .playlist-cover .actions .btn {\n        color: white; }\n  :host .playlist-cover .actions .btn:hover {\n          color: black; }\n"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/playlist-viewer/playlist-viewer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaylistViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlaylistViewerComponent = (function () {
    function PlaylistViewerComponent() {
        this.videos = [];
        this.queuedPlaylist = [];
        this.queuePlaylist = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.playPlaylist = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.queueVideo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.playVideo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.unqueueVideo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    PlaylistViewerComponent.prototype.ngOnInit = function () {
    };
    PlaylistViewerComponent.prototype.onPlayPlaylist = function (playlist) {
        this.playPlaylist.emit(playlist);
    };
    PlaylistViewerComponent.prototype.onQueueVideo = function (media) {
        this.queueVideo.emit(media);
    };
    PlaylistViewerComponent.prototype.onPlayVideo = function (media) {
        this.playVideo.emit(media);
    };
    PlaylistViewerComponent.prototype.onQueuePlaylist = function (playlist) {
        this.queuePlaylist.emit(playlist);
    };
    PlaylistViewerComponent.prototype.onRemove = function (media) {
        this.unqueueVideo.emit(media);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], PlaylistViewerComponent.prototype, "videos", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PlaylistViewerComponent.prototype, "playlist", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], PlaylistViewerComponent.prototype, "queuedPlaylist", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlaylistViewerComponent.prototype, "queuePlaylist", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlaylistViewerComponent.prototype, "playPlaylist", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlaylistViewerComponent.prototype, "queueVideo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlaylistViewerComponent.prototype, "playVideo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PlaylistViewerComponent.prototype, "unqueueVideo", void 0);
    PlaylistViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'playlist-viewer',
            styles: [__webpack_require__("./resources/assets/src/app/shared/components/playlist-viewer/playlist-viewer.scss")],
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush,
            template: "\n  <playlist-cover\n    [playlist]=\"playlist\"\n    (play)=\"onPlayPlaylist($event)\"\n    (queue)=\"onQueuePlaylist($event)\">\n  </playlist-cover>\n  <section>\n    <youtube-list\n      [list]=\"videos\"\n      [queued]=\"queuedPlaylist\"\n      (play)=\"onPlayVideo($event)\"\n      (queue)=\"onQueueVideo($event)\"\n      (unqueue)=\"onRemove($event)\"\n    ></youtube-list>\n  </section>\n  "
        }),
        __metadata("design:paramtypes", [])
    ], PlaylistViewerComponent);
    return PlaylistViewerComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/playlist-viewer/playlist-viewer.scss":
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block;\n  padding-bottom: 6rem; }\n"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-list/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__youtube_list__ = __webpack_require__("./resources/assets/src/app/shared/components/youtube-list/youtube-list.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__youtube_list__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-list/youtube-list.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host ul {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -ms-flex-wrap: wrap;\n        flex-wrap: wrap;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center; }\n  :host .youtube-list-item {\n    width: 100%;\n    max-width: 100%; }\n    :host .youtube-list-item:hover {\n      -webkit-box-shadow: none;\n              box-shadow: none; } }\n\n@media (min-width: 480px) {\n  :host .youtube-list-item {\n    width: 33%; } }\n\n@media (min-width: 767px) {\n  :host .youtube-list-item {\n    max-width: 28rem;\n    width: 24.5%; } }\n\n@media (min-width: 1440px) {\n  :host .youtube-list-item {\n    width: 28rem; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-list/youtube-list.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubeListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_shared_animations_fade_in_animation__ = __webpack_require__("./resources/assets/src/app/shared/animations/fade-in.animation.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


function createIdMap(list) {
    return list.reduce(function (acc, cur) {
        acc[cur.id] = true;
        return acc;
    }, {});
}
var YoutubeListComponent = (function () {
    function YoutubeListComponent() {
        this.list = [];
        this.queued = [];
        this.play = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.queue = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.add = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.unqueue = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.queuedMediaIdMap = {};
    }
    YoutubeListComponent.prototype.ngOnChanges = function (_a) {
        var queued = _a.queued;
        // if (queued && queued.currentValue) {
        //   console.log('YoutubeListComponent.createIdMap()');
        //   this.queuedMediaIdMap = createIdMap(queued.currentValue);
        // }
    };
    YoutubeListComponent.prototype.playSelectedVideo = function (media) {
        this.play.emit(media);
    };
    YoutubeListComponent.prototype.queueSelectedVideo = function (media) {
        this.queue.emit(media);
    };
    YoutubeListComponent.prototype.addVideo = function (media) {
        this.add.emit(media);
    };
    YoutubeListComponent.prototype.unqueueSelectedVideo = function (media) {
        this.unqueue.emit(media);
    };
    YoutubeListComponent.prototype.getMediaStatus = function (media) {
        console.log('getMediaStatus()');
        return {
            queued: this.queuedMediaIdMap[media.id]
        };
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], YoutubeListComponent.prototype, "list", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], YoutubeListComponent.prototype, "queued", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeListComponent.prototype, "play", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeListComponent.prototype, "queue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeListComponent.prototype, "add", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeListComponent.prototype, "unqueue", void 0);
    YoutubeListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'youtube-list',
            styles: [__webpack_require__("./resources/assets/src/app/shared/components/youtube-list/youtube-list.scss")],
            animations: [__WEBPACK_IMPORTED_MODULE_1_app_shared_animations_fade_in_animation__["b" /* fadeInAnimation */]],
            template: "\n  <ul class=\"list-unstyled clearfix\">\n    <li class=\"youtube-list-item\" [@fadeIn] *ngFor=\"let media of list\">\n      <youtube-media\n        [media]=\"media\"\n        [queued]=\"media | isInQueue:queued\"\n        (play)=\"playSelectedVideo(media)\"\n        (queue)=\"queueSelectedVideo(media)\"\n        (unqueue)=\"unqueueSelectedVideo(media)\"\n        (add)=\"addVideo(media)\">\n      </youtube-media>\n    </li>\n  </ul>\n  ",
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], YoutubeListComponent);
    return YoutubeListComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-media/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__youtube_media__ = __webpack_require__("./resources/assets/src/app/shared/components/youtube-media/youtube-media.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__youtube_media__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-media/youtube-media.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"youtube-item card ux-maker\" [class.show-description]=\"showDesc\">\n\t<section class=\"media-card\">\n\n\t\t<div class=\"front face is-rounded-bottom\">\n\t\t\t<div class=\"indicators clearfix\">\n\n\t\t\t\t<span class=\"pull-left item-is-playing\">\n\t\t\t\t\t<icon name=\"play\"></icon>Now Playing\n\t\t\t\t</span>\n\n\t\t\t</div>\n\n\t\t\t<div rel=\"tooltip\" class=\"media-thumb is-rounded-top\" [tooltip]=\"media.snippet.title\" (click)=\"playVideo(media)\">\n\n\t\t\t\t<div class=\"thumbnail\">\n\t\t\t\t\t<img name class=\"thumb-image\" src=\"{{ media | videoToThumb }}\">\n\t\t\t\t</div>\n\n\t\t\t\t<section class=\"stats is-absolute-bottom\">\n\t\t\t\t\t<span class=\"item-duration item-action pull-right\">\n\t\t\t\t\t\t<icon name=\"clock-o\"></icon> {{ media.contentDetails?.duration | toFriendlyDuration }}\n\t\t\t\t\t</span>\n\n\t\t\t\t\t<span class=\"item-likes item-action\" rel=\"tooltip\" title=\"Likes\">\n\t\t\t\t\t\t<icon name=\"thumbs-up\"></icon> {{ media.statistics?.likeCount | number:'2.0'}}\n\t\t\t\t\t</span>\n\n\t\t\t\t\t<span class=\"item-views item-action\" rel=\"tooltip\" title=\"Views\">\n\t\t\t\t\t\t<icon name=\"eye\"></icon> {{ media.statistics?.viewCount | number:'2.0'}}\n\t\t\t\t\t</span>\n\t\t\t\t</section>\n\n\t\t\t\t<button class=\"btn btn-default btn-lg ux-maker play-media\">\n\t\t\t\t\t<icon name=\"play\"></icon>\n\t\t\t\t</button>\n\t\t\t</div>\n\n\t\t\t<section class=\"item-actions main-actions is-rounded-bottom\">\n\t\t\t\t<h4 class=\"title\">\n\t\t\t\t\t<a href='#/video/' rel=\"tooltip\" class=\"media-thumb ellipsis\" [tooltip]=\"media.snippet.title\">\n\t\t\t\t\t\t{{ media.snippet.title }}\n\t\t\t\t\t</a>\n\t\t\t\t</h4>\n\t\t\t\t<section class=\"media-actions is-flex-row is-flex-valign\">\n\t\t\t\t\t<button *ngIf=\"!queued\" class=\"btn btn-link first-action\" tooltip=\"Queue this video to now playlist\" (click)=\"queueVideo(media)\">\n\t\t\t\t\t\t<icon name=\"share\"></icon> Queue\n\t\t\t\t\t</button>\n\t\t\t\t\t<button *ngIf=\"queued\" class=\" btn btn-danger\" tooltip=\"Queue this video to now playlist\" (click)=\"removeVideoFromQueue(media)\">\n\t\t\t\t\t\t<icon name=\"trash\"></icon> Remove From Queue\n\t\t\t\t\t</button>\n\t\t\t\t\t<button class=\"btn btn-link add-to-playlist\" disabled tooltip=\"Add this video to a playlist\" (click)=\"addVideo(media)\">\n\t\t\t\t\t\t<icon name=\"plus\"></icon> Add\n\t\t\t\t\t</button>\n\t\t\t\t\t<span class=\"is-strechable\"></span>\n\t\t\t\t\t<button class=\"btn btn-transparent no-padding\">\n\t\t\t\t\t\t<icon name=\"info-circle\" class=\"text-info text-md is-media-valign\" (click)=\"toggle(showDesc)\" tooltip=\"more info about this video\"></icon>\n\t\t\t\t\t</button>\n\t\t\t\t</section>\n\t\t\t</section>\n\t\t</div>\n\n\t\t<div class=\"description back face is-rounded is-absolute-top is-absolute-left\">\n\t\t\t<h4>\n\t\t\t\t<a href='https://youtube.com/watch?v={{ media.id }}' target=\"_blank\" tooltip=\"Open in YouTube\" class=\"media-thumb\">\n\t\t\t\t\t<icon name=\"youtube\"></icon> {{ media.snippet.title}}</a>\n\t\t\t</h4>\n\t\t\t<div *ngIf=\"showDesc\">{{ media.snippet.description }}</div>\n\t\t</div>\n\n\t\t<section *ngIf=\"showDesc\" class=\"close-desc is-absolute-bottom is-absolute-right\">\n\t\t\t<button (click)=\"toggle(showDesc)\" class=\"btn-transparent btn-xs text-md text-success\" tooltip=\"flip back...\">\n\t\t\t\t<icon name=\"times-circle\"></icon>\n\t\t\t</button>\n\t\t</section>\n\n\t</section>\n\n</section>"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-media/youtube-media.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host {\n    --media-primary-color: var(--brand-primary);\n    --media-bg-primary: var(--brand-bg-lite-transparent);\n    --media-dark-transparent: var(--brand-dark-bg-color-transparent);\n    --text-color: var(--brand-primary-text-color);\n    --text-primary-color-inverse: var(--brand-inverse-text); }\n    :host .youtube-item.show-description .item-actions {\n      visibility: none; }\n    :host .youtube-item .item-actions {\n      visibility: visible; }\n    :host .youtube-item .tooltip {\n      white-space: normal; }\n    :host .youtube-item {\n      width: 100%;\n      max-width: 100%;\n      margin: 0;\n      position: relative;\n      padding: 1rem;\n      border-radius: 0;\n      background-color: rgba(0, 0, 0, 0);\n      -webkit-box-shadow: none;\n              box-shadow: none;\n      border: none; }\n      :host .youtube-item .media-card {\n        color: gray;\n        text-shadow: none;\n        display: block;\n        position: relative;\n        text-decoration: none; }\n      :host .youtube-item .media-actions .first-action {\n        padding-left: 0; }\n      :host .youtube-item .front {\n        padding: 0px;\n        -webkit-box-shadow: 0 1px 30px -5px var(--media-dark-transparent);\n                box-shadow: 0 1px 30px -5px var(--media-dark-transparent);\n        position: relative; }\n      :host .youtube-item .title {\n        height: 27px;\n        margin: 0;\n        color: var(--text-color); }\n      :host .youtube-item .media-thumb {\n        display: block;\n        position: relative;\n        cursor: pointer;\n        font-weight: normal;\n        overflow: hidden; }\n        :host .youtube-item .media-thumb .stats {\n          background: var(--brand-dark-bg-color-transparent);\n          width: 100%;\n          padding: 1rem;\n          color: white; }\n          :host .youtube-item .media-thumb .stats .item-action.item-likes {\n            margin-right: 0.5rem; }\n          :host .youtube-item .media-thumb .stats .item-action .fa {\n            color: rgba(255, 255, 255, 0.9); }\n        :host .youtube-item .media-thumb:hover img {\n          -webkit-transform: scale(1.4) translate(0, 0);\n                  transform: scale(1.4) translate(0, 0); }\n        :host .youtube-item .media-thumb:hover .play-media {\n          opacity: 1; }\n      :host .youtube-item .play-media {\n        opacity: 0;\n        position: absolute;\n        top: 50%;\n        left: 50%;\n        background: var(--media-primary-color);\n        color: var(--text-primary-color-inverse);\n        border: none; }\n      :host .youtube-item .item-is-playing {\n        display: none;\n        color: var(--media-primary-color);\n        text-shadow: 0 0px 1px var(--media-primary-color); }\n        :host .youtube-item .item-is-playing i {\n          margin: 2px 4px 0 0; }\n        :host .youtube-item .item-is-playing.playing-true {\n          display: initial; }\n      :host .youtube-item .thumbnail {\n        overflow: hidden;\n        border-radius: 0;\n        clear: both;\n        height: 149px;\n        border: none;\n        background-color: var(--text-primary-color-inverse);\n        padding: 0;\n        -webkit-box-shadow: none;\n                box-shadow: none;\n        margin-bottom: 0; }\n        :host .youtube-item .thumbnail .thumb-image {\n          height: 100%;\n          -webkit-transform: scale(1.7) translatey(0);\n                  transform: scale(1.7) translatey(0);\n          -webkit-transition: -webkit-transform 0.3s ease-out;\n          transition: -webkit-transform 0.3s ease-out;\n          transition: transform 0.3s ease-out;\n          transition: transform 0.3s ease-out, -webkit-transform 0.3s ease-out;\n          display: block;\n          max-width: 100%;\n          margin-left: auto;\n          margin-right: auto;\n          border: 0; }\n      :host .youtube-item .description {\n        height: 219px;\n        overflow: hidden;\n        background-color: var(--media-bg-primary);\n        margin: 0;\n        padding: 8px 8px 36px 8px;\n        -webkit-box-sizing: border-box;\n                box-sizing: border-box;\n        word-wrap: break-word;\n        font-size: 16px;\n        color: var(--text-color);\n        width: 100%; }\n      :host .youtube-item.show-description .description {\n        overflow: auto; }\n      :host .youtube-item.show-description .stats {\n        display: none; }\n      :host .youtube-item.show-description .main-actions.item-actions {\n        visibility: hidden; }\n      :host .youtube-item .indicators {\n        height: 22px;\n        position: absolute;\n        z-index: 1000; }\n      :host .youtube-item .item-actions {\n        background: var(--media-bg-primary);\n        padding: 1rem;\n        margin: 0;\n        -webkit-box-shadow: none;\n                box-shadow: none;\n        bottom: -1px; }\n        :host .youtube-item .item-actions a:hover {\n          text-decoration: none; }\n        :host .youtube-item .item-actions span {\n          vertical-align: middle;\n          font-size: 1.2rem; }\n      :host .youtube-item .item-views {\n        display: inline-block; }\n      :host .youtube-item .share {\n        position: absolute;\n        right: 0px;\n        z-index: 10;\n        text-align: center; }\n        :host .youtube-item .share .dropdown-toggle {\n          padding: 0px 10px; }\n        :host .youtube-item .share .dropdown-menu {\n          right: 0px;\n          left: auto; } }\n\n@media (min-width: 500px) and (max-width: 767px) {\n  :host .youtube-item .item-views {\n    display: block; } }\n\n@media (min-width: 767px) {\n  :host .youtube-item .item-views {\n    width: auto;\n    max-width: 33%; } }\n\n@media (min-width: 768px) {\n  :host .youtube-item .play-media {\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%); } }\n\n@media (max-width: 767px) {\n  :host .youtube-item .play-media {\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%); } }\n"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-media/youtube-media.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubeMediaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var YoutubeMediaComponent = (function () {
    function YoutubeMediaComponent() {
        this.queued = false;
        this.play = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.queue = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.add = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.unqueue = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.showDesc = false;
        this.isPlaying = false;
    }
    YoutubeMediaComponent.prototype.playVideo = function (media) {
        this.play.emit(media);
    };
    YoutubeMediaComponent.prototype.queueVideo = function (media) {
        this.queue.emit(media);
    };
    YoutubeMediaComponent.prototype.addVideo = function (media) {
        this.add.emit(media);
    };
    YoutubeMediaComponent.prototype.toggle = function (showDesc) {
        this.showDesc = !showDesc;
    };
    YoutubeMediaComponent.prototype.removeVideoFromQueue = function (media) {
        this.unqueue.emit(media);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], YoutubeMediaComponent.prototype, "media", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], YoutubeMediaComponent.prototype, "queued", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeMediaComponent.prototype, "play", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeMediaComponent.prototype, "queue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeMediaComponent.prototype, "add", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubeMediaComponent.prototype, "unqueue", void 0);
    YoutubeMediaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'youtube-media',
            styles: [__webpack_require__("./resources/assets/src/app/shared/components/youtube-media/youtube-media.scss")],
            template: __webpack_require__("./resources/assets/src/app/shared/components/youtube-media/youtube-media.html"),
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], YoutubeMediaComponent);
    return YoutubeMediaComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-playlist/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__youtube_playlist__ = __webpack_require__("./resources/assets/src/app/shared/components/youtube-playlist/youtube-playlist.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__youtube_playlist__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-playlist/youtube-playlist.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"youtube-playlist-item ux-maker card\">\n  <section class=\"media-title front\">\n    <div class=\"media-thumb\" [tooltip]=\"media.snippet.title\">\n      <div class=\"thumbnail is-rounded\">\n        <img src=\"{{ media | videoToThumb }}\" class=\"thumb-image\">\n      </div>\n      <button class=\"btn btn-default btn-lg play-media\" (click)=\"playPlaylist(media)\">\n        <icon name=\"play\"></icon>\n      </button>\n    </div>\n\n    <section class=\"item-actions is-absolute is-rounded-bottom\">\n      <a class=\"playlist-link\" [routerLink]=\"[ link + '/playlist', media.id ]\" [tooltip]=\"media.snippet.title\" (click)=\"onNavigateToPlaylist()\">\n        <h4 class=\"title ellipsis\">\n          <icon name=\"th-large\"></icon> ({{ media.contentDetails.itemCount }}) {{ media.snippet.title }}\n        </h4>\n        <icon name=\"refresh 2x spin\" *ngIf=\"loading\" class=\"loader\"></icon>\n      </a>\n    </section>\n  </section>\n</div>"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-playlist/youtube-playlist.scss":
/***/ (function(module, exports) {

module.exports = "@media (min-width: 320px) {\n  :host {\n    --playlist-primary: var(--brand-primary);\n    --playlist-bg-primary: var(--brand-bg-lite-transparent);\n    --playlist-dark-transparent: var(--brand-dark-bg-color-transparent);\n    --playlist-inverse-color: var(--brand-inverse-text);\n    --playlist-link-color: var(--link-primary-color); }\n    :host .youtube-playlist-item {\n      margin: 0;\n      padding: 1rem;\n      border-radius: 0;\n      background-color: rgba(0, 0, 0, 0);\n      -webkit-box-shadow: none;\n              box-shadow: none;\n      border: none; }\n      :host .youtube-playlist-item .playlist-link .title {\n        color: var(--playlist-link-color); }\n    :host .media-title {\n      text-shadow: none;\n      display: block;\n      position: relative;\n      font-family: 'Open Sans Condensed', sans-serif;\n      text-decoration: none; }\n    :host .front {\n      padding: 0px;\n      background: var(--playlist-bg-primary);\n      -webkit-box-shadow: 0 1px 30px -5px var(--playlist-dark-transparent);\n              box-shadow: 0 1px 30px -5px var(--playlist-dark-transparent);\n      position: relative; }\n    :host .media-thumb {\n      display: block;\n      position: relative;\n      cursor: pointer;\n      font-weight: normal; }\n      :host .media-thumb .thumb-image {\n        -webkit-transition: -webkit-transform 0.3s ease-out;\n        transition: -webkit-transform 0.3s ease-out;\n        transition: transform 0.3s ease-out;\n        transition: transform 0.3s ease-out, -webkit-transform 0.3s ease-out;\n        height: 100%;\n        -webkit-transform: scale(1.7) translatey(0);\n                transform: scale(1.7) translatey(0);\n        display: block;\n        max-width: 100%;\n        margin-left: auto;\n        margin-right: auto;\n        border: 0; }\n      :host .media-thumb:hover .thumb-image {\n        -webkit-transform: scale(1.4) translate(0, 0);\n                transform: scale(1.4) translate(0, 0); }\n      :host .media-thumb:hover .play-media {\n        opacity: 1;\n        -webkit-transform: translate(-50%, -50%);\n                transform: translate(-50%, -50%); }\n      :host .media-thumb .play-media {\n        opacity: 0;\n        position: absolute;\n        top: 50%;\n        left: 50%;\n        background: var(--playlist-primary);\n        color: var(--playlist-inverse-color);\n        border: none; }\n      :host .media-thumb .thumbnail {\n        overflow: hidden;\n        clear: both;\n        height: 190px;\n        border: none;\n        background-color: var(--playlist-inverse-color);\n        padding: 0;\n        -webkit-box-shadow: none;\n                box-shadow: none;\n        margin-bottom: 0; }\n    :host .item-actions {\n      padding: 1rem;\n      margin: 0;\n      bottom: 0px;\n      width: 100%;\n      background-color: var(--playlist-dark-transparent); }\n      :host .item-actions .title {\n        margin: 0; }\n      :host .item-actions .playlist-link:hover {\n        text-decoration: none; }\n      :host .item-actions .loader {\n        position: absolute;\n        right: 10px;\n        top: 8px;\n        color: var(--brand-primary); } }\n\n@media (min-width: 768px) {\n  :host {\n    width: 24.5%;\n    max-width: 28rem; }\n    :host .item-actions .playlist-link {\n      display: block; } }\n\n@media (min-width: 767px) {\n  :host .youtube-list-item {\n    max-width: 320px; } }\n\n@media (min-width: 1440px) {\n  :host {\n    width: 280px; } }\n"

/***/ }),

/***/ "./resources/assets/src/app/shared/components/youtube-playlist/youtube-playlist.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubePlaylistComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var YoutubePlaylistComponent = (function () {
    function YoutubePlaylistComponent() {
        this.link = './';
        this.play = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.queue = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.isPlaying = false;
        this.loading = false;
    }
    YoutubePlaylistComponent.prototype.playPlaylist = function (media) {
        this.play.next(media);
    };
    YoutubePlaylistComponent.prototype.queuePlaylist = function (media) {
        this.queue.next(media);
    };
    YoutubePlaylistComponent.prototype.onNavigateToPlaylist = function () {
        this.loading = true;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], YoutubePlaylistComponent.prototype, "media", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], YoutubePlaylistComponent.prototype, "link", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubePlaylistComponent.prototype, "play", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], YoutubePlaylistComponent.prototype, "queue", void 0);
    YoutubePlaylistComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'youtube-playlist',
            styles: [__webpack_require__("./resources/assets/src/app/shared/components/youtube-playlist/youtube-playlist.scss")],
            template: __webpack_require__("./resources/assets/src/app/shared/components/youtube-playlist/youtube-playlist.html"),
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        })
    ], YoutubePlaylistComponent);
    return YoutubePlaylistComponent;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/directives/icon/icon.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IconDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_data_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/data.utils.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ICON_BASE_CLASSNAME = 'fa';
var ICON_LIB_PREFFIX = 'fa';
var IconDirective = (function () {
    function IconDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.name = '';
        this.icons = {
            'fa': true
        };
    }
    IconDirective.prototype.ngOnInit = function () {
        var name = this.name;
        var classes = [ICON_BASE_CLASSNAME];
        if (name) {
            classes = classes.concat(this.createIconStyles(name));
        }
        this.setClasses(classes);
    };
    IconDirective.prototype.ngOnChanges = function (_a) {
        var name = _a.name;
        if (name && Object(__WEBPACK_IMPORTED_MODULE_1__utils_data_utils__["a" /* isNewChange */])(name)) {
            this.createIconStyles(name.currentValue);
        }
    };
    IconDirective.prototype.createIconStyles = function (names) {
        return names.split(' ')
            .map(function (name) { return ICON_LIB_PREFFIX + "-" + name; });
    };
    IconDirective.prototype.setClasses = function (names) {
        var _this = this;
        names.forEach(function (name) {
            return _this.renderer.addClass(_this.el.nativeElement, name);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], IconDirective.prototype, "name", void 0);
    IconDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: 'icon, [appIcon]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer2"]])
    ], IconDirective);
    return IconDirective;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/directives/icon/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__icon_directive__ = __webpack_require__("./resources/assets/src/app/shared/directives/icon/icon.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__icon_directive__["a"]; });



/***/ }),

/***/ "./resources/assets/src/app/shared/directives/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CORE_DIRECTIVES; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__icon__ = __webpack_require__("./resources/assets/src/app/shared/directives/icon/index.ts");

var CORE_DIRECTIVES = [
    __WEBPACK_IMPORTED_MODULE_0__icon__["a" /* IconDirective */]
];


/***/ }),

/***/ "./resources/assets/src/app/shared/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_infinite_scroll__ = __webpack_require__("./node_modules/ngx-infinite-scroll/modules/ngx-infinite-scroll.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_typeahead__ = __webpack_require__("./node_modules/ngx-typeahead/esm5/ngx-typeahead.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_youtube_player__ = __webpack_require__("./node_modules/ngx-youtube-player/esm5/ngx-youtube-player.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components__ = __webpack_require__("./resources/assets/src/app/shared/components/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__directives__ = __webpack_require__("./resources/assets/src/app/shared/directives/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pipes__ = __webpack_require__("./resources/assets/src/app/shared/pipes/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_tooltip__ = __webpack_require__("./node_modules/ngx-tooltip/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_tooltip___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_ngx_tooltip__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var SharedModule = (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_6_ngx_youtube_player__["a" /* YoutubePlayerModule */],
                __WEBPACK_IMPORTED_MODULE_4_ngx_infinite_scroll__["a" /* InfiniteScrollModule */],
                __WEBPACK_IMPORTED_MODULE_5_ngx_typeahead__["a" /* NgxTypeaheadModule */],
                __WEBPACK_IMPORTED_MODULE_10_ngx_tooltip__["TooltipModule"]
            ],
            declarations: __WEBPACK_IMPORTED_MODULE_7__components__["a" /* CORE_COMPONENTS */].concat(__WEBPACK_IMPORTED_MODULE_8__directives__["a" /* CORE_DIRECTIVES */], __WEBPACK_IMPORTED_MODULE_9__pipes__["a" /* PIPES */]),
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormsModule */]
            ].concat(__WEBPACK_IMPORTED_MODULE_7__components__["a" /* CORE_COMPONENTS */], __WEBPACK_IMPORTED_MODULE_8__directives__["a" /* CORE_DIRECTIVES */], __WEBPACK_IMPORTED_MODULE_9__pipes__["a" /* PIPES */], [
                __WEBPACK_IMPORTED_MODULE_4_ngx_infinite_scroll__["a" /* InfiniteScrollModule */],
                __WEBPACK_IMPORTED_MODULE_6_ngx_youtube_player__["a" /* YoutubePlayerModule */],
                __WEBPACK_IMPORTED_MODULE_5_ngx_typeahead__["a" /* NgxTypeaheadModule */],
                __WEBPACK_IMPORTED_MODULE_10_ngx_tooltip__["TooltipModule"]
            ]),
            providers: []
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/pipes/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PIPES; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__search_pipe__ = __webpack_require__("./resources/assets/src/app/shared/pipes/search.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__toFriendlyDuration_pipe__ = __webpack_require__("./resources/assets/src/app/shared/pipes/toFriendlyDuration.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__videoToThumb_pipe__ = __webpack_require__("./resources/assets/src/app/shared/pipes/videoToThumb.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__parseTracks_pipe__ = __webpack_require__("./resources/assets/src/app/shared/pipes/parseTracks.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__isInQueue_pipe__ = __webpack_require__("./resources/assets/src/app/shared/pipes/isInQueue.pipe.ts");





var PIPES = [
    __WEBPACK_IMPORTED_MODULE_0__search_pipe__["a" /* SearchPipe */],
    __WEBPACK_IMPORTED_MODULE_1__toFriendlyDuration_pipe__["a" /* ToFriendlyDurationPipe */],
    __WEBPACK_IMPORTED_MODULE_2__videoToThumb_pipe__["a" /* VideoToThumbPipe */],
    __WEBPACK_IMPORTED_MODULE_3__parseTracks_pipe__["a" /* ParseTracksPipe */],
    __WEBPACK_IMPORTED_MODULE_4__isInQueue_pipe__["a" /* IsInQueuePipe */]
];


/***/ }),

/***/ "./resources/assets/src/app/shared/pipes/isInQueue.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsInQueuePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var IsInQueuePipe = (function () {
    function IsInQueuePipe() {
    }
    IsInQueuePipe.prototype.transform = function (media, medias) {
        return medias[0].includes(media.id);
    };
    IsInQueuePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'isInQueue' })
    ], IsInQueuePipe);
    return IsInQueuePipe;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/pipes/parseTracks.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParseTracksPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_media_parser_service__ = __webpack_require__("./resources/assets/src/app/core/services/media-parser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_memo_decorator__ = __webpack_require__("./node_modules/memo-decorator/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_memo_decorator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_memo_decorator__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ParseTracksPipe = (function () {
    function ParseTracksPipe(mediaParser) {
        this.mediaParser = mediaParser;
    }
    ParseTracksPipe.prototype.transform = function (value) {
        return this.mediaParser.parseTracks(value);
    };
    __decorate([
        __WEBPACK_IMPORTED_MODULE_2_memo_decorator___default()(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Object)
    ], ParseTracksPipe.prototype, "transform", null);
    ParseTracksPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'parseTracks' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__core_services_media_parser_service__["a" /* MediaParserService */]])
    ], ParseTracksPipe);
    return ParseTracksPipe;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/pipes/search.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SearchPipe = (function () {
    function SearchPipe() {
    }
    SearchPipe.prototype.transform = function (values, args) {
        var term = args.length ? args.toLowerCase() : '';
        var matchString = function (key) {
            if (typeof key === 'string') {
                return key.toLowerCase().indexOf(term) > -1;
            }
            return Object.keys(key).some(function (prop) { return matchString(key[prop]); });
        };
        return values.filter(matchString);
    };
    SearchPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'search',
        })
    ], SearchPipe);
    return SearchPipe;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/pipes/toFriendlyDuration.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToFriendlyDurationPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_memo_decorator__ = __webpack_require__("./node_modules/memo-decorator/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_memo_decorator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_memo_decorator__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToFriendlyDurationPipe = (function () {
    function ToFriendlyDurationPipe() {
    }
    ToFriendlyDurationPipe.prototype.transform = function (value, args) {
        var time = value;
        if (!time) {
            return '...';
        }
        return ['PT', 'H', 'M', 'S']
            .reduce(function (prev, cur, i, arr) {
            var now = prev.rest.split(cur);
            if (cur !== 'PT' && cur !== 'H' && !prev.rest.match(cur)) {
                prev.new.push('00');
            }
            if (now.length === 1) {
                return prev;
            }
            prev.new.push(now[0]);
            return {
                rest: now[1].replace(cur, ''),
                new: prev.new
            };
        }, { rest: time, new: [] })
            .new.filter(function (_time) { return _time !== ''; })
            .map(function (_time) { return (_time.length === 1 ? "0" + _time : _time); })
            .join(':');
    };
    __decorate([
        __WEBPACK_IMPORTED_MODULE_1_memo_decorator___default()(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Array]),
        __metadata("design:returntype", Object)
    ], ToFriendlyDurationPipe.prototype, "transform", null);
    ToFriendlyDurationPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'toFriendlyDuration'
        })
    ], ToFriendlyDurationPipe);
    return ToFriendlyDurationPipe;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/pipes/videoToThumb.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoToThumbPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_media_utils__ = __webpack_require__("./resources/assets/src/app/shared/utils/media.utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_memo_decorator__ = __webpack_require__("./node_modules/memo-decorator/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_memo_decorator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_memo_decorator__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VideoToThumbPipe = (function () {
    function VideoToThumbPipe() {
    }
    VideoToThumbPipe.prototype.transform = function (value) {
        var thumb = Object(__WEBPACK_IMPORTED_MODULE_1__utils_media_utils__["a" /* extractThumbUrl */])(value);
        return thumb || '';
    };
    __decorate([
        __WEBPACK_IMPORTED_MODULE_2_memo_decorator___default()(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Object)
    ], VideoToThumbPipe.prototype, "transform", null);
    VideoToThumbPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'videoToThumb' })
    ], VideoToThumbPipe);
    return VideoToThumbPipe;
}());



/***/ }),

/***/ "./resources/assets/src/app/shared/utils/data.utils.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return isNewChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return toPayload; });
var isNewChange = function (prop) {
    return prop.currentValue !== prop.previousValue;
};
var toPayload = function (action) { return action.payload; };


/***/ }),

/***/ "./resources/assets/src/app/shared/utils/media.utils.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export getSnippet */
/* unused harmony export extractThumbnail */
/* harmony export (immutable) */ __webpack_exports__["a"] = extractThumbUrl;
function getSnippet(media) {
    return media && media.hasOwnProperty('snippet') && media.snippet;
}
function extractThumbnail(snippet) {
    var thumbUrl = '';
    if (snippet) {
        var thumbs_1 = snippet.thumbnails;
        var sizes = ['high', 'standard', 'default'];
        var thumb = sizes.reduce(function (acc, size) {
            acc.result = !acc.result.length && thumbs_1[size] ? thumbs_1[size].url : acc.result;
            return acc;
        }, { result: '' });
        thumbUrl = thumb.result;
    }
    return thumbUrl;
}
function extractThumbUrl(media) {
    return extractThumbnail(getSnippet(media));
}


/***/ }),

/***/ "./resources/assets/src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false,
    youtube: {
        API_KEY: 'AIzaSyDCGg6FG6s_zxACqel09vQUKBc-x26pKFA',
        CLIENT_ID: '971861197531-hm7solf3slsdjc4omsfti4jbcbe625hs'
    }
};


/***/ }),

/***/ "./resources/assets/src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./resources/assets/src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("./resources/assets/src/app/app.module.ts");




if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map